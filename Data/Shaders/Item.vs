attribute vec3 aPosition;

uniform mat4 uMatTransform;
uniform mat4 CustomMatrix0;		//Matrix World * View

varying vec3 v_posV;
varying vec3 vPosition;

void main() {
	
	//Pass over local position
	vPosition = aPosition;
	
	//Compute and pass view position
	v_posV    = (CustomMatrix0 * vec4(aPosition, 1.0)).xyz;
	
	//Compute final position
	vec4 pos = uMatTransform * vec4(aPosition, 1.0);
	gl_Position = pos;
}