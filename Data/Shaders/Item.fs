precision mediump float;

uniform samplerCube CubeTexture;

uniform vec3 CustomVector0;		// x: fog start, y: fog length, z: fog alpha
uniform vec3 CustomVector1;		// fog color rgb

varying vec3 vPosition;
varying vec3 v_posV;

void main() 
{
	//----
	// Fog
	//----
	//Compute fog factor
	float eyeDistance = sqrt((v_posV.x * v_posV.x) + (v_posV.y * v_posV.y) + (v_posV.z * v_posV.z));
	float factor = (eyeDistance - CustomVector0.x) / CustomVector0.y;
	float lerpValue = clamp(factor, 0.0, 1.0);
	
	vec4 color = textureCube(CubeTexture, vPosition);
	
	// Final color
	gl_FragColor = mix(color, vec4(CustomVector1, CustomVector0.z), lerpValue);
	
	if (gl_FragColor.a < 0.3) discard;
}
