precision mediump float;

varying vec3 vPosW;
varying vec3 vNormW;

uniform vec3 CustomVector0;
uniform samplerCube CubeTexture;

void main() 
{   
	vec3 fromEye = vPosW - CustomVector0;
	vec3 reflectDir = reflect( normalize(fromEye), normalize(vNormW) );
	gl_FragColor = textureCube(CubeTexture, reflectDir);
}