attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec3 aBinormal;
attribute vec3 aTangent;
attribute vec2 aTexCoord0;
attribute vec4 aSkinMatrixIndices;
attribute vec4 aSkinMatrixWeights;

uniform mat4 uMatTransform;
uniform mat4 CustomMatrix0;		//Matrix World * View
uniform mat4 CustomMatrix1;		//Matrix World
uniform mat4 uMatBones[60];

varying vec3 v_posW;
varying vec3 v_normW;
varying vec3 v_binormW;
varying vec3 v_tangW;
varying vec2 vTexCoord0;
varying vec3 v_posV;

void main()
{
	vTexCoord0 = aTexCoord0;

	vec4 weights = aSkinMatrixWeights;
	weights.w = 1.0 - weights.x - weights.y - weights.z;
	
	mat4 boneMatrix = uMatBones[int(aSkinMatrixIndices.x)] * weights.x;
	boneMatrix += uMatBones[int(aSkinMatrixIndices.y)] * weights.y;
	boneMatrix += uMatBones[int(aSkinMatrixIndices.z)] * weights.z;
	boneMatrix += uMatBones[int(aSkinMatrixIndices.w)] * weights.w;
	
	vec4 localPos = boneMatrix * vec4( aPosition, 1.0);
	gl_Position = uMatTransform * localPos;
	
	//Compute and pass view position
	v_posV    = (CustomMatrix0 * boneMatrix * vec4(aPosition, 1.0)).xyz;
	
	//Compute world attributes
	v_posW    = (CustomMatrix1 * vec4(aPosition, 1.0)).xyz;
	v_normW   = (CustomMatrix1 * vec4(aNormal, 0.0)).xyz;
	v_binormW = (CustomMatrix1 * vec4(aBinormal, 0.0)).xyz;
	v_tangW   = (CustomMatrix1 * vec4(aTangent, 0.0)).xyz;
}