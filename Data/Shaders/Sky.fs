precision mediump float;

varying vec3 vPosition;

uniform samplerCube CubeTexture;

void main() 
{
	gl_FragColor = textureCube(CubeTexture, vPosition);
}