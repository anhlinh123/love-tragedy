attribute vec3 aPosition;
attribute vec3 aNormal;

uniform mat4 uMatTransform;
uniform mat4 CustomMatrix0;

varying vec3 vPosW;
varying vec3 vNormW;

void main() {
	
	//Pass over local position
	vPosW = (CustomMatrix0 * vec4(aPosition,1.0)).xyz;
	vNormW = (CustomMatrix0 *vec4(aNormal,0.0)).xyz;
	
	//Compute final position
	vec4 pos = uMatTransform * vec4(aPosition, 1.0);
	gl_Position = pos;
}