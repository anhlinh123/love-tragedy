precision mediump float;

uniform sampler2D DiffuseTexture;
uniform sampler2D CustomTexture0;
uniform sampler2D CustomTexture1;
uniform vec3 CustomVector0;

varying vec2 vTexCoord0;

void main()
{
	vec2 disp    = texture2D(CustomTexture1, vec2(vTexCoord0.x, vTexCoord0.y + CustomVector0.x)).rg;
	vec2 offset  = (2.0 * disp - 1.0) * 0.15;
	vec2 uv      = vTexCoord0 + offset;
	
	vec4 color   = texture2D(DiffuseTexture, uv);
	
	vec4 mask    = texture2D(CustomTexture0, vTexCoord0);
	
	gl_FragColor = color * vec4(1.0, 1.0, 1.0, mask.r);
	
	if (mask.r < 0.3)
	{
		discard;
	}
}