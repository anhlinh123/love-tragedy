precision mediump float;

uniform sampler2D DiffuseTexture;

uniform vec3 CustomVector0;		// x: fog start, y: fog length, z: fog alpha
uniform vec3 CustomVector1;		// fog color rgb
uniform vec3 CustomVector2;		// eye position
uniform vec3 CustomVector3;		// light position

varying vec2 vTexCoord0;
varying vec3 v_posW;
varying vec3 v_normW;
varying vec3 v_tangW;
varying vec3 v_binormW;
varying vec3 v_posV;

// hard-coded uniform
vec3 u_lightColor = vec3(2.0, 2.0, 2.0);
vec3 u_ambColor = vec3(1.0, 1.0, 1.0);
float u_weight = 0.5;
float u_specPower = 0.0;

void main() 
{
	//----
	// Fog
	//----
	//Compute fog factor
	float eyeDistance = sqrt((v_posV.x * v_posV.x) + (v_posV.y * v_posV.y) + (v_posV.z * v_posV.z));
	float factor = (eyeDistance - CustomVector0.x) / CustomVector0.y;
	float lerpValue = clamp(factor, 0.0, 1.0);
	
	vec4 texColor = texture2D(DiffuseTexture, vec2(vTexCoord0.x, 1.0 - vTexCoord0.y));
	if (mix(texColor.a, CustomVector0.z, lerpValue)	< 0.3) discard;
	
	//----------------------
	// Compute vector normal
	//----------------------
	//mat3 TBN    = mat3(normalize(v_tangW), normalize(v_binormW), normalize(v_normW));
	vec3 normW  = v_normW;//TBN * vec3(1.0, 1.0, 1.0);
	
	//--------------
	//Compute lights
	//--------------
	vec3 u_eyePos = CustomVector2;
	vec3 u_lightPos = CustomVector3;
	
	float d = length(u_lightPos);
	vec3 l  = normalize(u_lightPos);
	vec3 n  = normalize(normW);
	vec3 r  = normalize(reflect(l, n));
	vec3 e  = normalize(u_eyePos - v_posW);
	
	// Diffuse
	float cosDiff = max(dot(n, -l), 0.0);
	vec3 diffuse = u_lightColor * cosDiff;
	
	// Specular
	float cosSpec = max(dot(r, e), 0.0);
	vec3 specular = u_lightColor * pow(cosSpec, u_specPower); 
	
	vec4 color = vec4(mix(diffuse, u_ambColor, u_weight) * texColor.rgb, texColor.a); 
	
	// Final color
	gl_FragColor = mix(color, vec4(CustomVector1, CustomVector0.z), lerpValue);
}