attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec3 aBinormal;
attribute vec3 aTangent;
attribute vec2 aTexCoord0;

uniform mat4 uMatTransform;
uniform mat4 CustomMatrix0;		//Matrix World * View
uniform mat4 CustomMatrix1;		//Matrix World

varying vec3 v_posW;
varying vec3 v_normW;
varying vec3 v_binormW;
varying vec3 v_tangW;
varying vec2 vTexCoord0;
varying vec3 v_posV;

void main() 
{	
	//Pass over local uv
	vTexCoord0 = aTexCoord0;
	
	//Compute and pass view position
	v_posV    = (CustomMatrix0 * vec4(aPosition, 1.0)).xyz;
	
	//Compute world attributes
	v_posW    = (CustomMatrix1 * vec4(aPosition, 1.0)).xyz;
	v_normW   = (CustomMatrix1 * vec4(aNormal, 0.0)).xyz;
	v_binormW = (CustomMatrix1 * vec4(aBinormal, 0.0)).xyz;
	v_tangW   = (CustomMatrix1 * vec4(aTangent, 0.0)).xyz;
	
	gl_Position = uMatTransform * vec4(aPosition, 1.0);
}