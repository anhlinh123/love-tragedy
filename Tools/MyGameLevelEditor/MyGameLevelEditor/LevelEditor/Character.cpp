#include "stdafx.h"
#include <math.h>
#include "CoreEngine\Controller\Scene.h"
#include "CoreEngine\Object\Model.h"
#include "Character.h"


// temporary place here
Vector3 GetRotationVector(Vector3& from, Vector3& to, Vector3& up)
{
	// assume that everything is normalize
	// in fact I just test it with kinds of vector (0, 0, 1) or (1, 0, 0) or so so...
	float sign = - to.Cross(from).Dot(up);
	sign = ((int)sign == 0)? 1.0f:sign;
	float angle = sign * acos(to.Dot(from));
	return (up * angle);
}

Vector3 RotatePoint(Vector3& point, Vector3& rot)
{
	Matrix m;
	Vector4 p = Vector4(point.x, point.y, point.z, 1.0f);

	p = p * m.SetRotationZ(rot.z);
	p = p * m.SetRotationX(rot.x);
	p = p * m.SetRotationY(rot.y);
	return Vector3(p.x, p.y, p.z);
}

void Character::Load(Json::Value& data)
{
	m_pos = Vector3(data["pos"][0].asFloat(), data["pos"][1].asFloat(), data["pos"][2].asFloat());
	m_up = Vector3(data["up"][0].asFloat(), data["up"][1].asFloat(), data["up"][2].asFloat());
	m_face = Vector3(data["face"][0].asFloat(), data["face"][1].asFloat(), data["face"][2].asFloat());

	Vector3 original_up = Vector3(data["original_up"][0].asFloat(), data["original_up"][1].asFloat(), data["original_up"][2].asFloat());
	Vector3 rotate = Vector3(data["rotate"][0].asFloat(), data["rotate"][1].asFloat(), data["rotate"][2].asFloat());

	Object* o = new Object(CreateModel(data["name"].asString().c_str()));
	o->SetGeometry( m_pos + m_up * 0.5f,			// position is at center of the cube, so this hack help character stand on top of the cube
					rotate, 
					Vector3(0.5f, 0.5f, 0.5f));
	SCENE->AddObject(o);
	m_obj = o;
}