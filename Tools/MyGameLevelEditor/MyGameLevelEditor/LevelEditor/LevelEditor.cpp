#include "stdafx.h"
#include <math.h>
#include "LevelEditor.h"
#include "CoreEngine\Controller\Scene.h"
#include "CoreEngine\Object\Model.h"
#include "Utilities\utilities.h"
#include "Jsoncpp\json\json.h"


Vec3i::Vec3i(Vector3& vec3f)
{
	x = (int)floor(vec3f.x + 0.5f);
	y = (int)floor(vec3f.y + 0.5f);
	z = (int)floor(vec3f.z + 0.5f);
}

Vec3i::Vec3i(float a, float b, float c)
{
	x = (int)floor(a + 0.5f);
	y = (int)floor(b + 0.5f);
	z = (int)floor(c + 0.5f);
}


//struct callback
//{
//	static void DoMovePivot(Command* cmd)
//	{
//		Vector3 deltaMove = VECTOR3_VAL(cmd->m_arguments["move"]) * cmd->m_deltaTime;
//		cmd->m_obj->Move(deltaMove);
//		SCENE->GetCamera()->MovePosAndTarget(deltaMove);
//	}
//
//	static void MovePivotCompleted(Command* cmd)
//	{
//		LevelEditor* level = cmd->m_levelRef;
//		level->SyncPivotPosition(level->GetPivot().m_pos + VECTOR3_VAL(cmd->m_arguments["move"]) );
//	}
//
//	static void DoRotatePivot(Command* cmd)
//	{
//		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
//		cmd->m_obj->Rotate(deltaRot, VECTOR3_VAL(cmd->m_arguments["offset"]));
//	}
//
//	static void RotatePivotCompleted(Command* cmd)
//	{
//		LevelEditor* level = cmd->m_levelRef;
//		level->SyncPivotRotation( VECTOR3_VAL(cmd->m_arguments["rotate"]), 
//								VECTOR3_VAL(cmd->m_arguments["move"]), 
//								VECTOR3_VAL(cmd->m_arguments["up"]) );
//	}
//
//	static void DoJumpPivot(Command* cmd)
//	{
//		float radius = FLOAT_VAL(cmd->m_arguments["R"]);
//		float angle = 3.1416f * cmd->m_progress;
//		float oldAngle = 3.1416f * cmd->m_oldProgress;
//
//		Vector3 deltaMoveUp = VECTOR3_VAL(cmd->m_arguments["up"]) * radius * (sin(angle) - sin(oldAngle));
//		Vector3 deltaMoveFace = VECTOR3_VAL(cmd->m_arguments["face"]) * radius * (cos(oldAngle) - cos(angle));
//
//		cmd->m_obj->Move(deltaMoveUp + deltaMoveFace);
//		SCENE->GetCamera()->MovePosAndTarget(deltaMoveFace);
//	}
//
//	static void JumpPivotCompleted(Command* cmd)
//	{
//		LevelEditor* level = cmd->m_levelRef;
//		level->SyncPivotPosition(VECTOR3_VAL(cmd->m_arguments["dest"]));
//	}
//
//	static void DoRotateCamera(Command* cmd)
//	{
//		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
//		// Camera lock to behind character
//		SCENE->GetCamera()->RotateAroundTarget(deltaRot);
//	}
//
//	static void DoRotateCameraWithPoint(Command* cmd)
//	{
//		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
//		// Camera lock to behind character
//		SCENE->GetCamera()->RotatePosAndTarget( deltaRot, VECTOR3_VAL(cmd->m_arguments["point"]) );
//	}
//
//	static void DoMoveCube(Command* cmd)
//	{
//		Vector3 deltaMove = VECTOR3_VAL(cmd->m_arguments["move"]) * cmd->m_deltaTime;
//		cmd->m_obj->Move(deltaMove);
//	}
//
//	static void MoveCubeCompleted(Command* cmd)
//	{
//		LevelEditor* level = cmd->m_levelRef;
//		level->SyncCubeMove(VEC3i_VAL(cmd->m_arguments["switch"]),
//							VEC3i_VAL(cmd->m_arguments["from"]), 
//							VEC3i_VAL(cmd->m_arguments["to"]) );
//	}
//};


LevelEditor::LevelEditor()
{
}

LevelEditor::~LevelEditor()
{
	for (int i = 0; i < MAX_LEVEL_SIZE; i++)
	{
		for (int j = 0; j < MAX_LEVEL_SIZE; j++)
		{
			for (int k = 0; k < MAX_LEVEL_SIZE; k++)
			{
				if (m_levelMap[i][j][k])
					delete m_levelMap[i][j][k];
			}
		}
	}
}

void LevelEditor::CreatePivot(Json::Value& root)
{
	Json::Value data = root["pivot"];
	m_pivot.Load(data);

	data = root["preview_box"];
	m_previewBox.Load(data);
}

void LevelEditor::CreateNewLevel()
{
	// create a pivot at position (0, 0, 0)
	std::ifstream in(LEVEL_EDITOR_CONFIG, std::ios::in | std::ios::binary);
	std::stringstream sstr;
	sstr << in.rdbuf();

	Json::Value root;
	Json::Reader reader;

	bool parsingSuccessful = reader.parse( sstr.str(), root );
	if ( !parsingSuccessful )
		return;

	// reset the level
	memset(m_levelMap, NULL, sizeof(Cube*) * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE);
	m_offset = Vector3(1.0f, 1.0f, 1.0f);

	CreatePivot(root);
}

void LevelEditor::ProcessCommands(float deltaTime)
{
	m_bHasCommandRunning = false;
	for (CommandList::iterator it = m_commands.begin(); it != m_commands.end(); )
	{
		Command* cmd = (*it);
		if (!cmd->m_bIsRunning)
		{
			m_commands.erase(it++);
			delete cmd;
		}
		else
		{
			m_bHasCommandRunning = true;
			cmd->Execute(deltaTime);
			++it;
		}
	}
}

//bool LevelEditor::IsMovable(Vector3& move)
//{
//	Vec3i np(m_pivot.m_pos + m_pivot.m_up + move + m_offset);
//	Vec3i ng(m_pivot.m_pos + move + m_offset);
//
//	Cube* checkForBlock  = m_levelMap[np.x][np.y][np.z];
//	Cube* checkForGround = m_levelMap[ng.x][ng.y][ng.z];
//
//	return (!checkForBlock && checkForGround);
//}
//
//bool LevelEditor::IsRotable(Vector3& move)
//{
//	bool bRet = false;
//
//	Vec3i pos(m_pivot.m_pos + m_offset);
//	Vec3i newPos(m_pivot.m_pos + m_pivot.m_up + move + m_offset);
//	Vec3i newGround(m_pivot.m_pos + move + m_offset);
//
//	Cube* checkForBlock  = m_levelMap[newPos.x][newPos.y][newPos.z];
//	Cube* checkForGround = m_levelMap[newGround.x][newGround.y][newGround.z];
//	Cube* checkForRotable = m_levelMap[pos.x][pos.y][pos.z];
//
//	bRet = (!checkForBlock && !checkForGround && checkForRotable && checkForRotable->HasType(Cube::ROTABLE) );
//
//	if (bRet)
//	{
//		Ability& abi = *(checkForRotable->m_abilities[Cube::ROTABLE]);
//		bRet = bRet && 	
//				( ((abi.from.Dot(m_pivot.m_up) > 0.98f && abi.to.Dot(move) > 0.98f)) ||
//				  ((abi.to.Dot(m_pivot.m_up) > 0.98f && abi.from.Dot(move) > 0.98f)) ); // rotate should be two ways
//	}
//
//	return bRet;
//}
//
//bool LevelEditor::IsJumpable(Cube* cube)
//{
//	
//	return (cube->HasType(Cube::JUMP_ENTRY) && (cube->m_abilities[Cube::JUMP_ENTRY])->up.Dot(m_pivot.m_up) > 0.98f); 
//}
//
//bool LevelEditor::IsElevatorSwitch(Cube* cube)
//{
//	return (cube->HasType(Cube::ELEVATOR_SWITCH) && (cube->m_abilities[Cube::ELEVATOR_SWITCH])->up.Dot(m_pivot.m_up) > 0.98f); 
//}

void LevelEditor::MovePivot(Vector3& move)
{
	// rotate the pivot face to the moving direction
	Vector3 rotate = GetRotationVector(m_pivot.m_face, move, m_pivot.m_up);

	// make command to rotate face
	m_pivot.m_obj->Rotate(rotate, VECTOR3_ZERO);
	m_pivot.m_face = RotatePoint(m_pivot.m_face, rotate);

	// move both pivot and preview box to new position
	m_pivot.m_obj->Move(move);
	m_pivot.m_pos += move;
	SCENE->GetCamera()->MovePosAndTarget(move);

	m_previewBox.m_obj->Move(move);
	Vec3i curpos(m_pivot.m_pos + m_offset);
	m_previewBox.m_obj->SetVisible( (bool)(m_levelMap[curpos.x][curpos.y][curpos.z] == NULL) );
}

//void LevelEditor::SyncPivotPosition(Vector3& newPos)
//{
//	m_pivot.m_pos = newPos;
//
//	Vec3i pos(m_pivot.m_pos + m_offset);
//	OnEnterCube(m_levelMap[pos.x][pos.y][pos.z]);
//}
//
//void LevelEditor::SyncPivotRotation(Vector3& rot, Vector3& move, Vector3& up)
//{
//	//printf("sync rotation m_bCheckForRotable=%d\n", m_bCheckForRotable);
//	if (m_bCheckForRotable)
//	{
//		m_bCheckForRotable = false;
//		// rotate the object around the cube axis
//		Vector3 rotate = GetRotationVector(m_pivot.m_up, move, move.Cross(m_pivot.m_up));
//		
//		{
//			//printf("rotate plane\n");
//			ArgumentBundle args;
//			args.m_arguments["move"] = new Vector3(move);
//			args.m_arguments["rotate"] = new Vector3(rotate);
//			args.m_arguments["up"] = new Vector3(move);
//			args.m_arguments["offset"] = new Vector3(m_pivot.m_up * 0.5f);
//
//			Command* cmd = new Command(Command::ROTATE_OBJECT, m_pivot.m_obj, args, this, callback::DoRotatePivot, callback::RotatePivotCompleted, 2.0f); //with faster speed than move
//			m_commands.push_back(cmd);
//		}
//
//		if (SCENE->GetCamera()->m_mode == Camera::CAMERA_LOCK_BEHIND ||
//			SCENE->GetCamera()->m_mode == Camera::CAMERA_LOOK_AT_CHARACTER)
//		{
//			ArgumentBundle args;
//			args.m_arguments["rotate"] = new Vector3(rotate);
//			args.m_arguments["point"] = new Vector3(m_pivot.m_pos);
//
//			Command* cmd = new Command(Command::ROTATE_CAMERA, NULL, args, this, callback::DoRotateCameraWithPoint, NULL, 1.2f);
//			m_commands.push_back(cmd);
//		}
//	}
//	
//	m_pivot.m_up = up;
//	m_pivot.m_face = RotatePoint(m_pivot.m_face, rot);
//}
//
//void LevelEditor::OnEnterCube(Cube* cube)
//{
//	//Check for open view grid
//	if ( cube->HasType(Cube::OPEN_VIEW) )
//	{
//		SCENE->GetCamera()->OpenView(1000.0f);
//	}
//	//end
//
//	if (IsJumpable(cube))
//	{
//		Ability& abi = *(cube->m_abilities[Cube::JUMP_ENTRY]);
//		float radius = (abi.to - m_pivot.m_pos).Length() / 2.0f;
//
//		// command to jump the character
//		{
//			ArgumentBundle args;
//			args.m_arguments["R"] = new float(radius);
//			args.m_arguments["up"] = new Vector3(m_pivot.m_up);
//			args.m_arguments["face"] = new Vector3(m_pivot.m_face);
//			args.m_arguments["dest"] = new Vector3(abi.to);
//
//			Command* cmd = new Command(Command::MOVE_OBJECT, m_pivot.m_obj, args, this, callback::DoJumpPivot, callback::JumpPivotCompleted, 1.5f);
//			m_commands.push_back(cmd);
//		}
//	}
//	else if (IsElevatorSwitch(cube))
//	{
//		Ability& abi = *(cube->m_abilities[Cube::ELEVATOR_SWITCH]);
//		Vec3i pos(abi.from + m_offset);
//		Object* objToMove = (m_levelMap[pos.x][pos.y][pos.z])->m_obj;
//
//		// command to move the 'from' grid to 'to' position
//		{
//			ArgumentBundle args;
//			args.m_arguments["move"] = new Vector3(abi.to - abi.from);
//			args.m_arguments["switch"] = new Vec3i(m_pivot.m_pos + m_offset);
//			args.m_arguments["from"] = new Vec3i(abi.from + m_offset);
//			args.m_arguments["to"] = new Vec3i(abi.to + m_offset);
//
//			Command* cmd = new Command(Command::MOVE_OBJECT, objToMove, args, this, callback::DoMoveCube, callback::MoveCubeCompleted, 1.5f);
//			m_commands.push_back(cmd);
//		}
//	}
//}
//
//void LevelEditor::SyncCubeMove(Vec3i& switchPos, Vec3i& from, Vec3i& to)
//{
//	m_levelMap[to.x][to.y][to.z] = m_levelMap[from.x][from.y][from.z];
//	m_levelMap[from.x][from.y][from.z] = NULL;
//	
//	// switch 'from' and 'to' so it is a two way elevator
//	Ability* abi = (m_levelMap[switchPos.x][switchPos.y][switchPos.z])->m_abilities[Cube::ELEVATOR_SWITCH];
//	Vector3 temp = abi->from;
//	abi->from = abi->to;
//	abi->to = temp;
//}

void LevelEditor::PlaceNewBlock()
{
	Vec3i curpos(m_pivot.m_pos + m_offset);

	Cube* newNormal = Cube_AddCube(
		&m_levelMap[curpos.x][curpos.y][curpos.z],
		Cube::NORMAL, VECTOR3_ZERO, VECTOR3_ZERO, VECTOR3_ZERO);
		
	// add new object
	if (!newNormal->m_obj)
	{
		Object* o = new Object(CreateModel("Box.json"));
		o->SetGeometry(	m_pivot.m_pos, 
						Vector3(0.0f, 0.0f, 0.0f), 
						Vector3(0.5f, 0.5f, 0.5f));
		SCENE->AddObject(o);
		newNormal->m_obj = o;

		m_previewBox.m_obj->SetVisible(false);
		SyncOffset();
	}
}

void LevelEditor::RemoveBlock()
{
	Vec3i curpos(m_pivot.m_pos + m_offset);
	Cube* cube = m_levelMap[curpos.x][curpos.y][curpos.z];

	if (cube)
	{
		cube->m_obj->SetVisible(false); // temporary solution, should delete this object from SCENE
		delete cube;
		m_levelMap[curpos.x][curpos.y][curpos.z] = NULL;
	}
}

void LevelEditor::SyncOffset()
{
	// update the offset
	Vec3i old(m_offset);

	Vector3 curpos_ = m_pivot.m_pos;
	if (curpos_.x < 0.0f && (m_offset.x < 1.0f - curpos_.x) )
		m_offset.x = 1.0f - curpos_.x;
	if (curpos_.y < 0.0f && (m_offset.y < 1.0f - curpos_.y) )
		m_offset.y = 1.0f - curpos_.y;
	if (curpos_.z < 0.0f && (m_offset.z < 1.0f - curpos_.z) )
		m_offset.z = 1.0f - curpos_.z;

	Vec3i now(m_offset);

	// move the WHOLE MAP according to new offset... so bad :(
	struct LevelData
	{
		Cube* data[MAX_LEVEL_SIZE][MAX_LEVEL_SIZE][MAX_LEVEL_SIZE];
	};

	LevelData *pmap = new LevelData;
	memset(&pmap->data, NULL, sizeof(Cube*) * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE);
	for (int i = 0; i < MAX_LEVEL_SIZE; i++)
	{
		for (int j = 0; j < MAX_LEVEL_SIZE; j++)
		{
			for (int k = 0; k < MAX_LEVEL_SIZE; k++)
			{
				if (m_levelMap[i][j][k] != NULL)
					(pmap->data)[i - old.x + now.x][j - old.y + now.y][k - old.z + now.z] = m_levelMap[i][j][k];
			}
		}
	}
	memcpy(&m_levelMap, &pmap->data, sizeof(Cube*) * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE);
	delete pmap;
}

#define VECTOR3_TO_JSONARRAY(vector3, json) (json)[0] = (vector3).x;\
											(json)[1] = (vector3).y;\
											(json)[2] = (vector3).z;

void LevelEditor::SaveLevel(const char* level)
{
	Json::Value root = Json::objectValue;

	Json::Value& jsOffset = root["offset"] = Json::arrayValue;
	Json::Value& jsNormal = root["normal"] = Json::arrayValue;

	VECTOR3_TO_JSONARRAY(m_offset, jsOffset)

	for (int i = 0; i < MAX_LEVEL_SIZE; i++)
	{
		for (int j = 0; j < MAX_LEVEL_SIZE; j++)
		{
			for (int k = 0; k < MAX_LEVEL_SIZE; k++)
			{
				if (m_levelMap[i][j][k])
				{
					Json::Value jsPos = Json::objectValue;
					Vector3 pos = Vector3(i - m_offset.x, j - m_offset.y, k - m_offset.z);
					VECTOR3_TO_JSONARRAY(pos, jsPos["pos"])
					jsNormal.append(jsPos);
				}
			}
		}
	}

	std::ofstream out(level, std::ios::out | std::ios::binary);
	out << root;
}

void LevelEditor::LoadLevel(const char* level)
{
	std::ifstream in(level, std::ios::in | std::ios::binary);
	std::stringstream sstr;
	sstr << in.rdbuf();

	Json::Value root;
	Json::Reader reader;

	bool parsingSuccessful = reader.parse( sstr.str(), root );
	if ( !parsingSuccessful )
		return;

	// reset the level
	memset(m_levelMap, NULL, sizeof(Cube*) * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE * MAX_LEVEL_SIZE);

	Json::Value data = root["offset"];
	m_offset = Vector3(data[0].asFloat(), data[1].asFloat(), data[2].asFloat());
	Vec3i offset(m_offset);

	// load the level
	data = root["normal"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];

		Cube* newNormal = Cube_AddCube(
			&m_levelMap[datai["pos"][0].asInt() + offset.x][datai["pos"][1].asInt() + offset.y][datai["pos"][2].asInt() + offset.z],
			Cube::NORMAL, Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f));
		
		if (!newNormal->m_obj)
		{
			Object* o = new Object(CreateModel("Box.json"));
			o->SetGeometry(	Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()), 
							Vector3(0.0f, 0.0f, 0.0f), 
							Vector3(0.5f, 0.5f, 0.5f));
			SCENE->AddObject(o);
			newNormal->m_obj = o;
		}
	}

	// load the special grids
	data = root["rotable"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];

		Cube* newRotable = 	Cube_AddCube(
			&m_levelMap[datai["pos"][0].asInt() + offset.x][datai["pos"][1].asInt() + offset.y][datai["pos"][2].asInt() + offset.z],
			Cube::ROTABLE, Vector3(0.0f, 0.0f, 0.0f),
			Vector3(datai["from"][0].asFloat(), datai["from"][1].asFloat(), datai["from"][2].asFloat()), 
			Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()));
			
		if (!newRotable->m_obj)
		{
			Object* o = new Object(CreateModel("Box.json"));
			o->SetGeometry(	Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()), 
							Vector3(0.0f, 0.0f, 0.0f), 
							Vector3(0.5f, 0.5f, 0.5f));

			SCENE->AddObject(o);
			newRotable->m_obj = o;
		}
	}

	data = root["jumpable"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];

		// add jump entry grid
		Cube* newJumpable = Cube_AddCube(
			&m_levelMap[datai["pos"][0].asInt() + offset.x][datai["pos"][1].asInt() + offset.y][datai["pos"][2].asInt() + offset.z],
			Cube::JUMP_ENTRY, 
			Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()), 
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()));

		if (!newJumpable->m_obj)
		{
			Object* o = new Object(CreateModel("Box.json"));
			o->SetGeometry(	Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()), 
							Vector3(0.0f, 0.0f, 0.0f), 
							Vector3(0.5f, 0.5f, 0.5f));

			SCENE->AddObject(o);
			newJumpable->m_obj = o;
		}

		// add jump destination grid
		Cube* newNormal = Cube_AddCube(
			&m_levelMap[datai["to"][0].asInt() + offset.x][datai["to"][1].asInt() + offset.y][datai["to"][2].asInt() + offset.z],
			Cube::NORMAL, Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f));

		if (!newNormal->m_obj)
		{
			Object* o = new Object(CreateModel("Box.json"));
			o->SetGeometry(	Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()), 
							Vector3(0.0f, 0.0f, 0.0f), 
							Vector3(0.5f, 0.5f, 0.5f));

			SCENE->AddObject(o);
			newNormal->m_obj = o;
		}
	}

	data = root["elevator_switch"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];

		// add elevator switch cube
		Cube* newElevator = Cube_AddCube(
			&m_levelMap[datai["pos"][0].asInt() + offset.x][datai["pos"][1].asInt() + offset.y][datai["pos"][2].asInt() + offset.z],
			Cube::ELEVATOR_SWITCH, 
			Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()), 
			Vector3(datai["from"][0].asFloat(), datai["from"][1].asFloat(), datai["from"][2].asFloat()), 
			Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()));

		if (!newElevator->m_obj)
		{
			Object* o = new Object(CreateModel("Box.json"));
			o->SetGeometry(	Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()), 
							Vector3(0.0f, 0.0f, 0.0f), 
							Vector3(0.5f, 0.5f, 0.5f));

			SCENE->AddObject(o);
			newElevator->m_obj = o;
		}
	}

	data = root["open_view"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];

		Cube* newNormal = Cube_AddCube(
			&m_levelMap[datai["pos"][0].asInt() + offset.x][datai["pos"][1].asInt() + offset.y][datai["pos"][2].asInt() + offset.z],
			Cube::OPEN_VIEW, Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f));
			
		if (!newNormal->m_obj)
		{
			Object* o = new Object(CreateModel("Box.json"));
			o->SetGeometry(	Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()), 
							Vector3(0.0f, 0.0f, 0.0f), 
							Vector3(0.5f, 0.5f, 0.5f));

			SCENE->AddObject(o);
		}
	}

	data = root["boy"];
	m_boy.Load(data);
}