#include "stdafx.h"
#include "Command.h"

ArgumentBundle::ArgumentBundle(ArgumentBundle& args)
{
	m_arguments = args.m_arguments;
	m_bFreeMemory = !args.m_bFreeMemory;
}

ArgumentBundle::~ArgumentBundle()
{
	if (m_bFreeMemory)
	{
		for (std::map<std::string, void*>::iterator it = m_arguments.begin(); it != m_arguments.end(); it++)
		{
			delete it->second;
		}
	}
}

Command::Command(CommandType type, Object* obj, ArgumentBundle& argument, LevelEditor* levelRef, 
		CommandCallback execCb, CommandCallback finCb, float speed) 
	: m_type(type)
	, m_obj(obj)
	, m_arguments(argument)
	, m_levelRef(levelRef)
	, m_executeCallback(execCb)
	, m_finishCallback(finCb)
	, m_speed(speed)
	, m_deltaTime(0.0f)
	, m_progress(0.0f)
	, m_oldProgress(0.0f)
	, m_bIsRunning(true)
{
}

Command::~Command()
{ 
}

void Command::Execute(float deltaTime)
{
	m_deltaTime = deltaTime * m_speed;
	m_oldProgress = m_progress;
	m_progress += m_deltaTime;
	bool bFinish = m_progress > 1.0f;
	if (bFinish) //calibrate so that the progress exactly match 1.0f
	{
		m_deltaTime -= (m_progress - 1.0f);
		m_progress = 1.0f; 
	}

	if (m_bIsRunning && m_executeCallback)
	{
		m_executeCallback(this);
		if (bFinish)
			Finish();
	}
}

void Command::Finish()
{
	m_bIsRunning = false;
	if (m_finishCallback)
		m_finishCallback(this);
}