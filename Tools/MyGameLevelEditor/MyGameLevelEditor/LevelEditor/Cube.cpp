#include "stdafx.h"
#include "Cube.h"


Cube* Cube_AddCube(Cube** existing, Cube::CubeType type, Vector3& up, Vector3& from, Vector3& to)
{
	if (*existing)
	{
		(*existing)->AddAbility(type, up, from, to);
	}
	else
	{
		(*existing) = new Cube(type, up, from, to);
	}
	return (*existing);
}

Cube::Cube(CubeType type, Vector3& up, Vector3& from, Vector3& to) 
{
	m_type = 0;
	m_obj = NULL;
	AddType(type);
	AddType(Cube::NORMAL);
	m_abilities[type] = new Ability(up, from, to);
}

Cube::~Cube()
{
	for (std::map<long, Ability*>::iterator it = m_abilities.begin(); it != m_abilities.end(); it++)
	{
		delete it->second;
	}
}

void Cube::AddAbility(CubeType type, Vector3& up, Vector3& from, Vector3& to)
{
	if (!HasType(type))
	{
		AddType(type);
		m_abilities[type] = new Ability(up, from, to);
	}
}