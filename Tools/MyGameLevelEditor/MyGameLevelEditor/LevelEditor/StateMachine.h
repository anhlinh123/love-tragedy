#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_


class StateMachine
{
public:
	struct Transition
	{
		int stateFrom;
		int trigger;
		int stateTo;
	};

	struct State
	{
		int name;
		void (*onEnter)();
		void (*onExit)();
		void (*onUpdate)(float dt);
	};

public:
	StateMachine() {}
	~StateMachine() {}

	template<unsigned int SIZE>
	void InitStateMachine( const Transition(&transitionTable)[SIZE], const State* statesTable = NULL );

	void SetInitialState( int currentState );
	bool InjectEvent(int eventID);
	void Update(float dt);

	int GetCurrentState() const  { return m_currentState; }

protected:
	int               m_currentState;

	//  read only part
	const Transition* m_transitionTable;
	unsigned int      m_transitionTableSize;
	const State*      m_statesTable;
};

#endif