#include "stdafx.h"
#include "StateMachine.h"


template<unsigned int SIZE>
void StateMachine::InitStateMachine( const Transition(&transitionTable)[SIZE], const State* statesTable)
{
	m_transitionTable = transitionTable;
	m_statesTable = statesTable;
	m_transitionTableSize = SIZE;

	m_currentState = -1;
}

void StateMachine::SetInitialState( int currentState )
{
	m_currentState = currentState;

	if( m_statesTable && m_statesTable[m_currentState].onEnter )
	{
		m_statesTable[m_currentState].onEnter();
	}
}

bool StateMachine::InjectEvent(int eventID)
{
	//  Run through table & find matching transition
	for(unsigned int i = 0; i < m_transitionTableSize; ++i)
	{
		const Transition* transition = &m_transitionTable[i];

		//  Special ANY placeholder state, with ID < 0
		if(  ((transition->stateFrom < 0) || (transition->stateFrom == m_currentState)) 
			&& transition->trigger == eventID )
		{
			//  On Exit
			if( m_statesTable && m_statesTable[m_currentState].onExit )
			{
				m_statesTable[m_currentState].onExit();
			}

			//  Apply new state
			m_currentState = transition->stateTo;

			//  OnEnter
			if( m_statesTable && m_statesTable[m_currentState].onEnter )
			{
				m_statesTable[m_currentState].onEnter();
			}

			return true;
		}
	}

	return false;
}

void StateMachine::Update(float dt)
{
	if( m_statesTable && m_statesTable[m_currentState].onUpdate )
	{
		m_statesTable[m_currentState].onUpdate(dt);
	}
}