#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "jsoncpp/json/json.h"
#include "Utilities/Math.h"


class Character
{
public:
	Character() {}
	~Character() {}

	void Load(Json::Value& data);

public:
	Object* m_obj;
	Vector3 m_pos;
	Vector3 m_up;	// the direction that the boy is standing, casually the y axis
	Vector3 m_face;	// the boy face direction
};

Vector3 GetRotationVector(Vector3& from, Vector3& to, Vector3& up);
Vector3 RotatePoint(Vector3& point, Vector3& rot);

#endif