#ifndef _CUBE_H_
#define _CUBE_H_

#include <map>
#include "Utilities/Math.h"

class Object;


class Ability
{
public:
	Ability(Vector3& u, Vector3& f, Vector3& t) : up(u), from(f), to(t)  {}
	~Ability() {}

	Vector3 up;
	Vector3 from;
	Vector3 to;
};

typedef std::map<long, Ability*> AbilityMap;


class Cube
{
public:
	enum CubeType //bit wise
	{
		NORMAL			= 1,
		ROTABLE			= 2,
		JUMP_ENTRY		= 8,
		ELEVATOR_SWITCH = 16,
		OPEN_VIEW		= 32
	};

	Cube(CubeType type,	Vector3& up, Vector3& from, Vector3& to);
	~Cube();

	Ability* operator[](long key) { return m_abilities[key]; }

	void AddType(CubeType type) { m_type |= (long)type; }
	bool HasType(CubeType type) { return ((m_type & ((long)type)) != 0); }
	void AddAbility(CubeType type,	Vector3& up, Vector3& from, Vector3& to);

	long m_type;
	Object* m_obj;

	AbilityMap m_abilities;
};

Cube* Cube_AddCube(Cube** existing, Cube::CubeType type, Vector3& up, Vector3& from, Vector3& to);

#endif