#ifndef _COMMAND_H_
#define _COMMAND_H_

#include <vector>
#include <list>
#include <map>

// forward declare
class LevelEditor;
class Object;
class Command;

typedef void (*CommandCallback)(Command*);

class ArgumentBundle
{
public:
	ArgumentBundle(bool bFree = false) : m_bFreeMemory(bFree) {}
	ArgumentBundle(ArgumentBundle& args);
	~ArgumentBundle();

	void* operator[](const char* key) { return m_arguments[key]; }
	std::map<std::string, void*> m_arguments;
	bool m_bFreeMemory;
};


class Command
{
public:
	enum CommandType
	{
		MOVE_OBJECT,
		ROTATE_OBJECT,
		ROTATE_CAMERA,
		OPEN_VIEW
	};

	Command(CommandType type, Object* obj, ArgumentBundle& argument, LevelEditor* levelRef, 
			CommandCallback execCb, CommandCallback finCb = NULL, float speed = 1.0f);
	~Command();
	void Execute(float deltaTime);
	void Finish();

	CommandType m_type;
	Object* m_obj;
	ArgumentBundle m_arguments;	// defend on CommandType, argument can be different. 
								// Ex: type is MOVE_OBJECT --> argument is a Vector3 indicates the movement
	
	CommandCallback m_executeCallback;
	CommandCallback m_finishCallback;

	LevelEditor* m_levelRef;
	float m_deltaTime;
	float m_progress;
	float m_oldProgress;
	float m_speed;

	bool m_bIsRunning;
};

typedef std::list<Command*> CommandList;


// macros to make things look nicer

// Please use it as: VECTOR3_VAL(x) * float; instead of the inverse, or C++ will compile fail,
// because class Vector3 does not implement this way yet
#define VECTOR3_VAL(x)		(*((Vector3*)(x))) 
#define VEC3i_VAL(x)		(*((Vec3i*)(x)))
#define FLOAT_VAL(x)		(*((float*)(x)))

#endif