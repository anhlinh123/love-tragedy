#ifndef _LEVEL_H_
#define _LEVEL_H_

#include "Command.h"
#include "Character.h"
#include "Cube.h"
#include "Utilities/Math.h"
#include "Jsoncpp/json/json.h"

// forward declare
class Object;


#define		MAX_LEVEL_SIZE			30
#define		BOX_SIZE				1.0f	// the box model is 1.0f width/height
#define		HALF_BOX_SIZE			0.5f
#define		LEVEL_EDITOR_CONFIG		"../Data/LevelEditor.json"
#define		LEVEL_TEST				"../Data/Level2.json"
#define		LEVEL_TEST_SAVE			"../Data/LevelTest.json"


enum LevelEditorEditorControlKey
{
	LEK_PLACE_BLOCK			= 0x20,		//SPACE_KEY
	LEK_REMOVE_BLOCK		= 0x08,		//BACKSPACE_KEY
	LEK_SWITCH_CUBE_SIDE	= 0x09,		//TAB_KEY
	LEK_PLACE_ABILITY		= 0x0D,		//ENTER_KEY
	LEK_LOAD_LEVEL			= '1',
	LEK_SAVE_LEVEL			= '2'
};


class Vec3i
{
public:
	Vec3i(Vector3& vec3f);
	Vec3i(float a, float b, float c);
	~Vec3i() {}
	
	int x, y, z;
};


class LevelEditor
{
public:
	LevelEditor();
	~LevelEditor();

	void SaveLevel(const char* level);
	void LoadLevel(const char* level);
	void CreateNewLevel();
	void ProcessCommands(float deltaTime);
	void CreatePivot(Json::Value& root);
	
	// boy control functions
	void MovePivot(Vector3& move);
	void SyncPivotPosition(Vector3& newPos);
	void SyncPivotRotation(Vector3& rot, Vector3& move, Vector3& up);
	Character& GetPivot() { return m_pivot; }
	bool IsPivotMoving() { return m_bHasCommandRunning; }

	// Cube functions
	//bool IsMovable(Vector3& move);
	//bool IsRotable(Vector3& move);
	//bool IsJumpable(Cube* cube);
	//bool IsElevatorSwitch(Cube* cube);
	//void OnEnterCube(Cube* cube);
	//void SyncCubeMove(Vec3i& switchPos, Vec3i& from, Vec3i& to);

	void PlaceNewBlock();
	void RemoveBlock();
	void SyncOffset();

protected:
	Cube* m_levelMap[MAX_LEVEL_SIZE][MAX_LEVEL_SIZE][MAX_LEVEL_SIZE];
	Vector3 m_offset;

	Character m_pivot;
	Character m_previewBox;
	Character m_boy;

	CommandList m_commands;
	bool m_bHasCommandRunning;
	bool m_bCheckForRotable;
};

#endif