#ifndef _STATE_PLAY_H_
#define _STATE_PLAY_H_

#include "CoreEngine/StateManager/State.h"
#include "LevelEditor/LevelEditor.h"


class StateEditor : public State
{
public:
	StateEditor() : State("StateEditor") {}
	virtual ~StateEditor() {}

	virtual void Enter();
	virtual bool Update(float time_elapsed);
	virtual bool OnKeyUp(int keycode);

protected:
	LevelEditor m_levelEditor;
};

#endif