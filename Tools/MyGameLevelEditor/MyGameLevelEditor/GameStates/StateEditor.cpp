#include "stdafx.h"
#include "CoreEngine/Controller/Scene.h"
#include "CoreEngine/Controller/GameInput.h"
#include "StateEditor.h"
#include "LevelEditor/LevelEditor.h"
#include "LevelEditor/Character.h"


void StateEditor::Enter()
{
	m_levelEditor.CreateNewLevel();
	
	Character pivot = m_levelEditor.GetPivot();
	Camera* camera = new Camera();
	camera->SetProjection(0.1f, FAR_PLANE, FOV, ASPECT_RATIO);
	camera->Initialize(pivot.m_pos + pivot.m_up + Vector3(1.0f, 1.0f, 1.0f), pivot.m_pos + pivot.m_up, Vector3(10.0f, 10.0f, 10.0f), Vector3(1.0f, 1.0f, 1.0f), pivot.m_up);
	camera->SetBehindCharacter(pivot.m_pos + pivot.m_up, pivot.m_face, Vector2(3.5f, 1.5f));
	camera->SetActive();
	SCENE->SetCamera(camera);

	// change to look at character camera
	SCENE->GetCamera()->ChangeToNextMode();
	SCENE->GetCamera()->ChangeToNextMode();
}

bool StateEditor::Update(float time_elapsed)
{
	Character pivot = m_levelEditor.GetPivot();
	// Camera look at character
	if (SCENE->GetCamera()->m_mode == Camera::CAMERA_LOOK_AT_CHARACTER)
	{
		SCENE->GetCamera()->RotateAroundTarget(pivot.m_up * GAME_INPUT->m_deltaCameraYaw);
		SCENE->GetCamera()->RotateAroundTarget(pivot.m_up.Cross(pivot.m_face) * GAME_INPUT->m_deltaCameraPitch);
	}
	SCENE->GetCamera()->ChangeZoomLevel(GAME_INPUT->m_deltaCameraZoom * 0.5f); // make it slow

	// if player is not moving, check for new move
	//if (!m_level.IsPivotMoving())
	//{
	//	SCENE->GetCamera()->ComputeBasis(pivot.m_face, pivot.m_up);

	//	if (GAME_INPUT->m_deltaPlayerLR > 0.0f)
	//		m_level.MovePivot(SCENE->GetCamera()->m_leftBasis);
	//	else if (GAME_INPUT->m_deltaPlayerLR < 0.0f)
	//		m_level.MovePivot(SCENE->GetCamera()->m_rightBasis);
	//	else if (GAME_INPUT->m_deltaPlayerFB > 0.0f)
	//		m_level.MovePivot(SCENE->GetCamera()->m_forwardBasis);
	//	else if (GAME_INPUT->m_deltaPlayerFB < 0.0f)
	//		m_level.MovePivot(SCENE->GetCamera()->m_backwardBasis);
	//}
	//m_level.ProcessCommands(time_elapsed);
	return true;
}

bool StateEditor::OnKeyUp(int keycode)
{
	Character pivot = m_levelEditor.GetPivot();
	SCENE->GetCamera()->ComputeBasis(pivot.m_face, pivot.m_up);

	switch (keycode)
	{
	//case GK_SWITCH_CAMERA_MODE:
	//	SCENE->GetCamera()->ChangeToNextMode();
	//	break;

		// move pivot
	case GK_PLAYER_LEFT:
		m_levelEditor.MovePivot(SCENE->GetCamera()->m_leftBasis);
		break;
	case GK_PLAYER_RIGHT:
		m_levelEditor.MovePivot(SCENE->GetCamera()->m_rightBasis);
		break;
	case GK_PLAYER_UP:
		m_levelEditor.MovePivot(SCENE->GetCamera()->m_forwardBasis);
		break;
	case GK_PLAYER_DOWN:
		m_levelEditor.MovePivot(SCENE->GetCamera()->m_backwardBasis);
		break;

		// editor functions
	case LEK_PLACE_BLOCK:
		m_levelEditor.PlaceNewBlock();
		break;
	case LEK_REMOVE_BLOCK:
		m_levelEditor.RemoveBlock();
		break;
	case LEK_LOAD_LEVEL:
		m_levelEditor.LoadLevel(LEVEL_TEST);
		break;
	case LEK_SAVE_LEVEL:
		m_levelEditor.SaveLevel(LEVEL_TEST_SAVE);
		break;

	default:
		break;
	}
	return true;
}