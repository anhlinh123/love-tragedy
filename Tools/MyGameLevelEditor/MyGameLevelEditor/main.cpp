#include "stdafx.h"
#include "CoreEngine/Controller/Scene.h"
#include "GameStates/StateEditor.h"

void GameInit()
{
	printf("Game LevelEditor v1.0\n");
	SCENE->PushGameState(new StateEditor());
}