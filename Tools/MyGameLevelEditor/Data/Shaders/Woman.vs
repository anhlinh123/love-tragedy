attribute vec3 aPosition;
attribute vec2 aTexCoord0;

uniform mat4 uMatTransform;

varying vec2 vTexCoord0;

void main()
{
	vTexCoord0 = aTexCoord0;
	gl_Position = uMatTransform * vec4(aPosition, 1.0);
}