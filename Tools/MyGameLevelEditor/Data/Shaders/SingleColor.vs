attribute vec3 aPosition;
attribute vec2 aTexCoord0; // as color for each vertex

uniform mat4 uMatTransform;

varying vec3 vPosition;
varying vec2 vTexCoord0;

void main() {
	
	//Pass over local position
	vPosition = aPosition;
	vTexCoord0 = aTexCoord0;
	
	//Compute final position
	vec4 pos = uMatTransform * vec4(aPosition, 1.0);
	gl_Position = pos;
}