precision mediump float;

varying vec3 vPosition;

uniform samplerCube CubeTexture;

void main() 
{
	// make the object look opaque
	vec4 color = textureCube(CubeTexture, vPosition);
	color.a = 0.3;
	gl_FragColor = color;
}