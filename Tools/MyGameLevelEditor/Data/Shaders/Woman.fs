precision mediump float;

uniform sampler2D DiffuseTexture;
varying vec2 vTexCoord0;

void main()
{
	gl_FragColor = texture2D(DiffuseTexture, vTexCoord0);
}
