precision mediump float;

varying vec3 vPosition;
varying vec2 vTexCoord0;

void main() 
{
	// make the object look opaque
	vec4 color = vec4(vTexCoord0.x, 1.0, vTexCoord0.y, 1.0);
	gl_FragColor = color;
}