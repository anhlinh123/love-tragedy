attribute vec3 aPosition;

uniform mat4 uMatTransform;

varying vec3 vPosition;

void main() {
	
	//Pass over local position
	vPosition = aPosition;
	
	//Compute final position
	vec4 pos = uMatTransform * vec4(aPosition, 1.0);
	gl_Position = pos;
}