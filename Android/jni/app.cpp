#include <time.h>
#include <string.h>

#include "app.h"
#include "jni_base.h"

static JavaVM* cached_jvm;

// CoreEngine functions
extern int  Init();
extern void Update( float deltaTime);
extern void Touch( int type, int x, int y, int id);
extern void Draw();
extern void CleanUp();

void appInit()
{
    Init();
}

void appUpdate()
{
	static clock_t begin = clock();
	clock_t end = clock();

    Update( (float) (end - begin)/CLOCKS_PER_SEC);
    Draw();

	begin = end;
}

void appDestroy()
{
    CleanUp();
}

void appOnTouch( int type, int x, int y, int id)
{
	Touch( type, x, y, id);
}

void appOnKeyDown(int keyCode)
{
}

void appOnKeyUp(int keyCode)
{
}

void appPause()
{
}

void appResume()
{
}

///////////////////////////////////////////////////////////////////////
// TO DO: Clean this up
///////////////////////////////////////////////////////////////////////
#if USE_SOUND_VOX
extern "C" void VoxSetJavaVM(JavaVM* javaVM);
#endif

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    LOGI("JNI_OnLoad");

	cached_jvm = vm;
	JNIEnv *env;

    #if USE_SOUND_VOX
	LOGI("VoxSetJavaVM(vm)");
	VoxSetJavaVM(vm);
    #endif


	if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK)
	{
	    LOGE("vm->GetEnv((void**) &env, JNI_VERSION_1_4) FAIL");
		return -1;
	}

	LOGI("return JNI_VERSION_1_4;");
	return JNI_VERSION_1_4;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEnv* get_env()
{
    JNIEnv *env;

    int status = cached_jvm->AttachCurrentThread(&env, 0);
    if (status < 0)
    {
        LOGE("Error attaching thread\n");
        return NULL;
    }

	if (cached_jvm->GetEnv((void **)&env, JNI_VERSION_1_6))
	{
		LOGI("JNI version's not supported\n");
		return NULL;
	}

	return env;
}


