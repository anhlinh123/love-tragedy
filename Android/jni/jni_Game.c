#include "jni_base.h"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Method:		nativeInit
 * Signature:	(V)V
 * Comment:		Since in GLRender we already initialize the important stuff, here we're only registering the methods
 */
JNIEXPORT void JNICALL JNI_FUNCTION(Game_nativeInit) (JNIEnv*  env, jobject thiz)
{
    LOGI("Game_nativeInit");

	mObjectGame = (jobject) (*env)->NewGlobalRef(env, thiz);   

	jclass cls = (*env)->GetObjectClass(env, thiz);

    mMethodHideAd = (*env)->GetMethodID (env,cls, "hideAd", "()V");
    mMethodShowAd = (*env)->GetMethodID (env, cls, "showAd", "()V");

    mMethodLogin = (*env)->GetMethodID (env,cls, "login", "()V");
    mMethodShare = (*env)->GetMethodID (env,cls, "share", "()V");

    mMethodExit = (*env)->GetMethodID (env,cls, "Exit", "()V");

    mMethodSavePref = (*env)->GetMethodID (env,cls, "SavePref", "(I)V");
    mMethodLoadPref = (*env)->GetMethodID (env,cls, "LoadPref", "()I");
}

JNIEXPORT void JNI_FUNCTION(Game_nativeOnKeyDown)( JNIEnv*  env, jobject thiz, jint a )
{
	appOnKeyDown( a );
}

JNIEXPORT void JNI_FUNCTION(Game_nativeOnKeyUp)( JNIEnv*  env, jobject thiz, jint a )
{
	appOnKeyUp( a );
}

void nativeExit()
{
	JNIEnv* mEnv = get_env();
	if (mMethodExit)
	{
		(*mEnv)->CallVoidMethod(mEnv, mObjectGame, mMethodExit);
	}
}

void nativeHideAd()
{
	JNIEnv* mEnv = get_env();
	if (mMethodHideAd)
	{
		(*mEnv)->CallVoidMethod(mEnv, mObjectGame, mMethodHideAd);
	}		
}

void nativeShowAd()
{	
	JNIEnv* mEnv = get_env();
	if (mMethodShowAd)
	{
		(*mEnv)->CallVoidMethod(mEnv, mObjectGame, mMethodShowAd);
	}		
}

void nativeLogin()
{
	JNIEnv* mEnv = get_env();
	if (mMethodLogin)
	{
		(*mEnv)->CallVoidMethod(mEnv, mObjectGame, mMethodLogin);
	}		
}

void nativeShare()
{	
	JNIEnv* mEnv = get_env();
	if (mMethodShare)
	{
		(*mEnv)->CallVoidMethod(mEnv, mObjectGame, mMethodShare);
	}		
}

void nativeSave(int index)
{
	JNIEnv* env = get_env();
	if (mMethodSavePref)
	{
		(*env)->CallVoidMethod(env, mObjectGame, mMethodSavePref, index);
	}
}

int nativeLoad()
{
	JNIEnv* env = get_env();
	if (mMethodLoadPref)
	{
		return (*env)->CallIntMethod(env, mObjectGame, mMethodLoadPref);
	}
}


#ifdef  __cplusplus
}
#endif



