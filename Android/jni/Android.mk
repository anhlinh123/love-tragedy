# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)

#########################
#	CoreEngine project  #
#########################
include $(CLEAR_VARS)

LOCAL_MODULE    := CoreEngine
LOCAL_SRC_FILES := \
	../../CoreEngine/Controller/Scene.cpp \
    ../../CoreEngine/Controller/GameInput.cpp \
    ../../CoreEngine/Object/Camera.cpp \
    ../../CoreEngine/Object/Model.cpp \
    ../../CoreEngine/Object/GeometryChunk.cpp \
    ../../CoreEngine/Object/Mesh.cpp \
    ../../CoreEngine/Object/ModelAnimator.cpp \
    ../../CoreEngine/Object/Object.cpp \
    ../../CoreEngine/Object/PathDefine.cpp \
    ../../CoreEngine/Object/Renderable.cpp \
    ../../CoreEngine/Object/Renderer.cpp \
    ../../CoreEngine/Object/Shader.cpp \
    ../../CoreEngine/Object/Texture.cpp \
    ../../CoreEngine/Object/Uniform.cpp \
    ../../CoreEngine/StateManager/State.cpp \
    ../../CoreEngine/StateManager/StateStack.cpp \
	../../CoreEngine/Sound/SoundManager_Android.cpp \
    ../../CoreEngine/stdafx.cpp \
	
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../..
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../CoreEngine
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../CoreEngine/Sound
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../CoreEngine/Object
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Include/Angle
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/include

LOCAL_CFLAGS := -DANDROID
LOCAL_CFLAGS += -D_DEBUG
LOCAL_CFLAGS += -DCEGUI_STATIC
LOCAL_CFLAGS += -DCEGUI_BUILD_STATIC_FACTORY_MODULE
	
include $(BUILD_STATIC_LIBRARY)

####################
#	CEGUI project  #
####################
include $(CLEAR_VARS)

LOCAL_MODULE    := CEGUI
LOCAL_SRC_FILES := \
    ../../Lib/cegui/src/Affector.cpp \
    ../../Lib/cegui/src/Animation.cpp \
    ../../Lib/cegui/src/AnimationInstance.cpp \
    ../../Lib/cegui/src/AnimationManager.cpp \
    ../../Lib/cegui/src/Animation_xmlHandler.cpp \
    ../../Lib/cegui/src/Base.cpp \
    ../../Lib/cegui/src/BasicImage.cpp \
    ../../Lib/cegui/src/BasicRenderedStringParser.cpp \
    ../../Lib/cegui/src/BidiVisualMapping.cpp \
    ../../Lib/cegui/src/BoundSlot.cpp \
    ../../Lib/cegui/src/CentredRenderedString.cpp \
    ../../Lib/cegui/src/ChainedXMLHandler.cpp \
    ../../Lib/cegui/src/Clipboard.cpp \
    ../../Lib/cegui/src/Colour.cpp \
    ../../Lib/cegui/src/ColourRect.cpp \
    ../../Lib/cegui/src/CompositeResourceProvider.cpp \
    ../../Lib/cegui/src/Config_xmlHandler.cpp \
    ../../Lib/cegui/src/CoordConverter.cpp \
    ../../Lib/cegui/src/DataContainer.cpp \
    ../../Lib/cegui/src/DefaultLogger.cpp \
    ../../Lib/cegui/src/DefaultRenderedStringParser.cpp \
    ../../Lib/cegui/src/DefaultResourceProvider.cpp \
    ../../Lib/cegui/src/DynamicModule.cpp \
    ../../Lib/cegui/src/Element.cpp \
    ../../Lib/cegui/src/Event.cpp \
    ../../Lib/cegui/src/EventArgs.cpp \
    ../../Lib/cegui/src/EventSet.cpp \
    ../../Lib/cegui/src/Exceptions.cpp \
    ../../Lib/cegui/src/FactoryModule.cpp \
    ../../Lib/cegui/src/FactoryRegisterer.cpp \
    ../../Lib/cegui/src/falagard/Falagard_XMLHandler.cpp \
    ../../Lib/cegui/src/Font.cpp \
    ../../Lib/cegui/src/FontManager.cpp \
    ../../Lib/cegui/src/Font_xmlHandler.cpp \
    ../../Lib/cegui/src/FormattedRenderedString.cpp \
    ../../Lib/cegui/src/FreeTypeFont.cpp \
    ../../Lib/cegui/src/GeometryBuffer.cpp \
    ../../Lib/cegui/src/GlobalEventSet.cpp \
    ../../Lib/cegui/src/GUIContext.cpp \
    ../../Lib/cegui/src/GUILayout_xmlHandler.cpp \
    ../../Lib/cegui/src/Image.cpp \
    ../../Lib/cegui/src/ImageCodec.cpp \
    ../../Lib/cegui/src/ImageCodecModules/SILLY/SILLYImageCodec.cpp \
    ../../Lib/cegui/src/ImageCodecModules/SILLY/ImageCodecModule.cpp \
    ../../Lib/cegui/src/ImageManager.cpp \
    ../../Lib/cegui/src/JustifiedRenderedString.cpp \
    ../../Lib/cegui/src/JustifiedRenderedStringWordWrapper.cpp \
    ../../Lib/cegui/src/KeyFrame.cpp \
    ../../Lib/cegui/src/LeftAlignedRenderedString.cpp \
    ../../Lib/cegui/src/LinkedEvent.cpp \
    ../../Lib/cegui/src/Logger.cpp \
    ../../Lib/cegui/src/MouseCursor.cpp \
    ../../Lib/cegui/src/NamedElement.cpp \
    ../../Lib/cegui/src/NamedXMLResourceManager.cpp \
    ../../Lib/cegui/src/PixmapFont.cpp \
    ../../Lib/cegui/src/Property.cpp \
    ../../Lib/cegui/src/PropertyHelper.cpp \
    ../../Lib/cegui/src/PropertySet.cpp \
    ../../Lib/cegui/src/Quaternion.cpp \
    ../../Lib/cegui/src/Rect.cpp \
    ../../Lib/cegui/src/RenderedString.cpp \
    ../../Lib/cegui/src/RenderedStringComponent.cpp \
    ../../Lib/cegui/src/RenderedStringImageComponent.cpp \
    ../../Lib/cegui/src/RenderedStringTextComponent.cpp \
    ../../Lib/cegui/src/RenderedStringWidgetComponent.cpp \
    ../../Lib/cegui/src/RenderEffectManager.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_GeometryBufferBase.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_GL3FBOTextureTarget.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_GL3GeometryBuffer.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_GL3Renderer.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_RendererBase.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_Shader.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_ShaderManager.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_StateChangeWrapper.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_Texture.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_TextureTarget.cpp \
    ../../Lib/cegui/src/RendererModules/OpenGL/_ViewportTarget.cpp \
    ../../Lib/cegui/src/RenderingSurface.cpp \
    ../../Lib/cegui/src/RenderingWindow.cpp \
    ../../Lib/cegui/src/RenderQueue.cpp \
    ../../Lib/cegui/src/RenderTarget.cpp \
    ../../Lib/cegui/src/RightAlignedRenderedString.cpp \
    ../../Lib/cegui/src/Scheme.cpp \
    ../../Lib/cegui/src/SchemeManager.cpp \
    ../../Lib/cegui/src/Scheme_xmlHandler.cpp \
    ../../Lib/cegui/src/ScriptModule.cpp \
    ../../Lib/cegui/src/SimpleTimer.cpp \
    ../../Lib/cegui/src/String.cpp \
    ../../Lib/cegui/src/SubscriberSlot.cpp \
    ../../Lib/cegui/src/System.cpp \
    ../../Lib/cegui/src/SystemKeys.cpp \
    ../../Lib/cegui/src/TextUtils.cpp \
    ../../Lib/cegui/src/Window.cpp \
    ../../Lib/cegui/src/WindowFactory.cpp \
    ../../Lib/cegui/src/WindowFactoryManager.cpp \
    ../../Lib/cegui/src/WindowManager.cpp \
    ../../Lib/cegui/src/WindowRenderer.cpp \
    ../../Lib/cegui/src/WindowRendererManager.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Button.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Default.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Editbox.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_FrameWindow.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ItemEntry.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ItemListbox.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Listbox.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ListHeader.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ListHeaderSegment.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Menubar.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_MenuItem.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Module.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_MultiColumnList.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_MultiLineEditbox.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_PopupMenu.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ProgressBar.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ScrollablePane.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Scrollbar.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Slider.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Static.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_StaticImage.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_StaticText.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_TabButton.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_TabControl.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Titlebar.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_ToggleButton.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Tooltip.cpp \
    ../../Lib/cegui/src/WindowRendererSets/Core/Core_Tree.cpp \
    ../../Lib/cegui/src/XMLAttributes.cpp \
    ../../Lib/cegui/src/XMLHandler.cpp \
    ../../Lib/cegui/src/XMLParser.cpp \
    ../../Lib/cegui/src/XMLParserModules/TinyXML/TinyXML_XMLParser.cpp \
    ../../Lib/cegui/src/XMLParserModules/TinyXML/XMLParserModule.cpp \
    ../../Lib/cegui/src/XMLSerializer.cpp \
    ../../Lib/cegui/src/widgets/ButtonBase.cpp \
    ../../Lib/cegui/src/widgets/ClippedContainer.cpp \
    ../../Lib/cegui/src/widgets/Combobox.cpp \
    ../../Lib/cegui/src/widgets/ComboDropList.cpp \
    ../../Lib/cegui/src/widgets/DefaultWindow.cpp \
    ../../Lib/cegui/src/widgets/DragContainer.cpp \
    ../../Lib/cegui/src/widgets/Editbox.cpp \
    ../../Lib/cegui/src/widgets/FrameWindow.cpp \
    ../../Lib/cegui/src/widgets/GridLayoutContainer.cpp \
    ../../Lib/cegui/src/widgets/GroupBox.cpp \
    ../../Lib/cegui/src/widgets/HorizontalLayoutContainer.cpp \
    ../../Lib/cegui/src/widgets/ItemEntry.cpp \
    ../../Lib/cegui/src/widgets/ItemListBase.cpp \
    ../../Lib/cegui/src/widgets/ItemListbox.cpp \
    ../../Lib/cegui/src/widgets/LayoutCell.cpp \
    ../../Lib/cegui/src/widgets/LayoutContainer.cpp \
    ../../Lib/cegui/src/widgets/Listbox.cpp \
    ../../Lib/cegui/src/widgets/ListboxItem.cpp \
    ../../Lib/cegui/src/widgets/ListboxTextItem.cpp \
    ../../Lib/cegui/src/widgets/ListHeader.cpp \
    ../../Lib/cegui/src/widgets/ListHeaderSegment.cpp \
    ../../Lib/cegui/src/widgets/Menubar.cpp \
    ../../Lib/cegui/src/widgets/MenuBase.cpp \
    ../../Lib/cegui/src/widgets/MenuItem.cpp \
    ../../Lib/cegui/src/widgets/MultiColumnList.cpp \
    ../../Lib/cegui/src/widgets/MultiLineEditbox.cpp \
    ../../Lib/cegui/src/widgets/PopupMenu.cpp \
    ../../Lib/cegui/src/widgets/ProgressBar.cpp \
    ../../Lib/cegui/src/widgets/PushButton.cpp \
    ../../Lib/cegui/src/widgets/RadioButton.cpp \
    ../../Lib/cegui/src/widgets/ScrollablePane.cpp \
    ../../Lib/cegui/src/widgets/Scrollbar.cpp \
    ../../Lib/cegui/src/widgets/ScrolledContainer.cpp \
    ../../Lib/cegui/src/widgets/ScrolledItemListBase.cpp \
    ../../Lib/cegui/src/widgets/SequentialLayoutContainer.cpp \
    ../../Lib/cegui/src/widgets/Slider.cpp \
    ../../Lib/cegui/src/widgets/Spinner.cpp \
    ../../Lib/cegui/src/widgets/TabButton.cpp \
    ../../Lib/cegui/src/widgets/TabControl.cpp \
    ../../Lib/cegui/src/widgets/Thumb.cpp \
    ../../Lib/cegui/src/widgets/Titlebar.cpp \
    ../../Lib/cegui/src/widgets/ToggleButton.cpp \
    ../../Lib/cegui/src/widgets/Tooltip.cpp \
    ../../Lib/cegui/src/widgets/Tree.cpp \
    ../../Lib/cegui/src/widgets/TreeItem.cpp \
    ../../Lib/cegui/src/widgets/VerticalLayoutContainer.cpp \
    ../../Lib/cegui/src/falagard/ComponentBase.cpp \
    ../../Lib/cegui/src/falagard/Dimensions.cpp \
    ../../Lib/cegui/src/falagard/EventAction.cpp \
    ../../Lib/cegui/src/falagard/EventLinkDefinition.cpp \
    ../../Lib/cegui/src/falagard/FormattingSetting.cpp \
    ../../Lib/cegui/src/falagard/FrameComponent.cpp \
    ../../Lib/cegui/src/falagard/ImageryComponent.cpp \
    ../../Lib/cegui/src/falagard/ImagerySection.cpp \
    ../../Lib/cegui/src/falagard/LayerSpecification.cpp \
    ../../Lib/cegui/src/falagard/NamedArea.cpp \
    ../../Lib/cegui/src/falagard/PropertyDefinitionBase.cpp \
    ../../Lib/cegui/src/falagard/PropertyInitialiser.cpp \
    ../../Lib/cegui/src/falagard/SectionSpecification.cpp \
    ../../Lib/cegui/src/falagard/StateImagery.cpp \
    ../../Lib/cegui/src/falagard/TextComponent.cpp \
    ../../Lib/cegui/src/falagard/WidgetComponent.cpp \
    ../../Lib/cegui/src/falagard/WidgetLookFeel.cpp \
    ../../Lib/cegui/src/falagard/WidgetLookManager.cpp \
    ../../Lib/cegui/src/falagard/XMLEnumHelper.cpp \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftsystem.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftinit.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftdebug.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftbase.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftbbox.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftglyph.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftbdf.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftbitmap.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftcid.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftfstype.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftgasp.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftgxval.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftlcdfil.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftmm.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftotval.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftpatent.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftpfr.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftstroke.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftsynth.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/fttype1.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftwinfnt.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/base/ftxf86.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/bdf/bdf.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/cff/cff.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/cid/type1cid.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/pcf/pcf.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/pfr/pfr.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/sfnt/sfnt.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/truetype/truetype.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/type1/type1.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/type42/type42.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/winfonts/winfnt.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/raster/raster.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/smooth/smooth.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/autofit/autofit.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/cache/ftcache.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/gzip/ftgzip.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/lzw/ftlzw.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/gxvalid/gxvalid.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/otvalid/otvalid.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/psaux/psaux.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/pshinter/pshinter.c \
    ../../Lib/cegui/src/../dependencies/freetype-2.4.4/src/psnames/psnames.c \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYDataSource.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYFileDataSource.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYImage.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYImageContext.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYImageLoader.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYImageLoaderManager.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/SILLYMemoryDataSource.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/loaders/SILLYJPGImageContext.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/loaders/SILLYJPGImageLoader.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/loaders/SILLYPNGImageContext.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/loaders/SILLYPNGImageLoader.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/loaders/SILLYTGAImageContext.cpp \
    ../../Lib/cegui/src/../dependencies/SILLY/src/loaders/SILLYTGAImageLoader.cpp \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jaricom.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcapimin.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcapistd.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcarith.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jccoefct.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jccolor.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcdctmgr.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jchuff.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcinit.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcmainct.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcmarker.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcmaster.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcomapi.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcparam.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcprepct.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jcsample.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jctrans.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdapimin.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdapistd.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdarith.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdatadst.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdatasrc.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdcoefct.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdcolor.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jddctmgr.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdhuff.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdinput.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdmainct.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdmarker.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdmaster.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdmerge.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdpostct.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdsample.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jdtrans.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jerror.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jfdctflt.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jfdctfst.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jfdctint.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jidctflt.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jidctfst.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jidctint.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jmemmgr.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jmemnobs.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jquant1.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jquant2.c \
    ../../Lib/cegui/src/../dependencies/jpeg-8c/jutils.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/png.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngerror.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngget.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngmem.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngpread.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngread.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngrio.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngrtran.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngrutil.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngset.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngtrans.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngwio.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngwrite.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngwtran.c \
    ../../Lib/cegui/src/../dependencies/libpng-1.4.7/pngwutil.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/adler32.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/compress.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/crc32.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/deflate.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/gzclose.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/gzlib.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/gzread.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/gzwrite.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/infback.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/inffast.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/inflate.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/inftrees.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/trees.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/uncompr.c \
    ../../Lib/cegui/src/../dependencies/zlib-1.2.5/zutil.c \
    ../../Lib/cegui/src/../dependencies/tinyxml-2.6.2/tinystr.cpp \
    ../../Lib/cegui/src/../dependencies/tinyxml-2.6.2/tinyxml.cpp \
    ../../Lib/cegui/src/../dependencies/tinyxml-2.6.2/tinyxmlerror.cpp \
    ../../Lib/cegui/src/../dependencies/tinyxml-2.6.2/tinyxmlparser.cpp \
	
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Lib/cegui/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/freetype-2.4.4/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/SILLY/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/tinyxml-2.6.2
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/jpeg-8c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/libpng-1.4.7
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/zlib-1.2.5
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/dependencies/glm-0.9.3.1
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Include/Angle
	
LOCAL_CFLAGS := \
	-DANDROID \
	-D_DEBUG \
	-DCEGUI_STATIC \
	-DCEGUI_BUILD_STATIC_FACTORY_MODULE \
	-D_CRT_SECURE_NO_WARNINGS \
	-DNOMINMAX \
	-DFT2_BUILD_LIBRARY \
	-DSILLY_STATIC \
	-DSILLY_HAVE_JPG \
	-DSILLY_HAVE_PNG \
	-DTINYXML_STATIC \

include $(BUILD_STATIC_LIBRARY)

########################
#	Utilities project  #
########################
include $(CLEAR_VARS)

LOCAL_MODULE    := Utilities
LOCAL_SRC_FILES := \
	../../Lib/Utilities/esShader.cpp \
    ../../Lib/Utilities/Math.cpp \
    ../../Lib/Utilities/stdafx.cpp \
    ../../Lib/Utilities/TGA.cpp \
	
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Include/Angle
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../..
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Tools/Include
	
LOCAL_CFLAGS := \
	-DANDROID \
	-D_DEBUG \
	-D_LIB \

include $(BUILD_STATIC_LIBRARY)

#####################
#	MyGame project  #
#####################
include $(CLEAR_VARS)

LOCAL_MODULE    := MyGame
LOCAL_SRC_FILES := \
    ../../MyGame/GUI/GUIMenu.cpp \
    ../../MyGame/GUI/GUIPlay.cpp \
    ../../MyGame/GUI/GUILoadingScreen.cpp \
	../../MyGame/Level/Ability.cpp \
    ../../MyGame/Level/Cube.cpp \
    ../../MyGame/Level/Character.cpp \
    ../../MyGame/GameStates/StatePlay.cpp \
    ../../MyGame/Level/Command.cpp \
    ../../MyGame/Level/Level.cpp \
    ../../MyGame/Level/StateMachine.cpp \
    ../../MyGame/main.cpp \
    ../../MyGame/GameStates/StateAbout.cpp \
    ../../MyGame/GameStates/StateMenu.cpp \
    ../../MyGame/GameStates/StateOptions.cpp \
    ../../MyGame/GameStates/StateWS.cpp \
    ../../MyGame/GameStates/StateLoading.cpp \
    ../../MyGame/stdafx.cpp \
    ../../MyGame/GUI/GUIWelcomeScreen.cpp \
	../../MyGame/CoreEngine_Android.cpp \
	
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Include/Angle
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../..
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../MyGame
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../CoreEngine
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/cegui/include

LOCAL_CFLAGS := \
	-DANDROID \
	-D_DEBUG \
	-D_LIB \
	-DCEGUI_STATIC \
	-DCEGUI_BUILD_STATIC_FACTORY_MODULE \

include $(BUILD_STATIC_LIBRARY)

######################
#	Jsoncpp project  #
######################
include $(CLEAR_VARS)

LOCAL_MODULE    := Jsoncpp
LOCAL_SRC_FILES := \
	../../Lib/Jsoncpp/json_reader.cpp \
    ../../Lib/Jsoncpp/json_value.cpp \
    ../../Lib/Jsoncpp/json_writer.cpp \
	
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Include/Angle
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../..
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Lib/Jsoncpp
	
LOCAL_CFLAGS := \
	-DANDROID \
	-D_DEBUG \
	-D_LIB \

include $(BUILD_STATIC_LIBRARY)

#####################
#	Online project  #
#####################
#include $(CLEAR_VARS)

#LOCAL_MODULE    := Online
#LOCAL_SRC_FILES := \

#include $(BUILD_STATIC_LIBRARY)

######################
#	Android project  #
######################
include $(CLEAR_VARS)

LOCAL_MODULE    := Android
LOCAL_SRC_FILES := \
	app.cpp \
    jni_Game.c \
    jni_GameRenderer.c \
	jni_Sound.c \
	
LOCAL_STATIC_LIBRARIES := \
	MyGame \
	CoreEngine \
	CEGUI \
	Utilities \
	Jsoncpp \

LOCAL_LDLIBS    := -llog -lGLESv2
include $(BUILD_SHARED_LIBRARY)