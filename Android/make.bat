@echo off

if not exist "%~dp0setEnv.bat" (
	echo ERROR! you do not have "%~dp0setEnv.bat"!
	echo Create setEnv.bat with the content of setEnv.bat.template, and with your paths!
	goto :eof
) else (
	call setEnv.bat
)

set TYPE_OF_BUILD=%1
if "%TYPE_OF_BUILD%"=="release" (
	echo Building the release version!
) else if "%TYPE_OF_BUILD%"=="debug" (
	echo Building the debug version!
) else (
	echo "ERROR: parameter is wrong! Usage: make.bat <release|debug>"
	goto end
)

echo ================== COMPILE NATIVE CODE ==================
call make_native.bat
if errorlevel 1 (
	goto end
)

echo ================== MAKE APK ==================
if "%TYPE_OF_BUILD%"=="debug" (
	
	call make_android.bat debug
	if errorlevel 1 (
		goto end
	)

	set FINAL_APK=bin\%APP_NAME%-debug.apk
) else if "%TYPE_OF_BUILD%"=="release" (

	call make_android.bat release
	if errorlevel 1 (
		goto end
	)
	
	set FINAL_APK=bin\%APP_NAME%-release-unsigned.apk
)

if "%INSTALL_APK%"=="1" (
		echo ================== INSTALL ANDROID ==================
		%ADB% install -r %FINAL_APK%
	)

echo ===================================================

:end