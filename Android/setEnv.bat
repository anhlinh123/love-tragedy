@echo off
rem *******************************************************
rem                   Specific paths
rem *******************************************************
set JAVA_HOME=c:\Program Files\Java\jdk1.8.0_31
set ANDROID_HOME=c:\Program Files (x86)\Android\android-sdk
set ANDROID_NDK_HOME=e:\DevTools\android-ndk-r10
set APACHE_ANT=e:\DevTools\apache-ant-1.9.4\bin
set APP_NAME=OurGame
set INSTALL_APK=1
rem *******************************************************

set NDK_BUILD=%ANDROID_NDK_HOME%/ndk-build
set ADB="%ANDROID_HOME%/platform-tools/adb"
set /p ndk_ver=<%ANDROID_NDK_HOME%\RELEASE.TXT
echo "Using Android NDK %ndk_ver% from %ANDROID_NDK_HOME%"

