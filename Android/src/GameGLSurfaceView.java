package com.superstudio.android.ourGame;

import android.content.Context;
import android.util.Log;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

class GameGLSurfaceView extends GLSurfaceView       
{
	public GameGLSurfaceView(Context context){
        super(context);
		// Create an OpenGL ES 2.0 context
		setEGLContextClientVersion(2);
		setPreserveEGLContextOnPause(true);
        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(new GameRenderer());
    }
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int action = e.getActionMasked();
		int index  = e.getActionIndex();
		int x    = (int) e.getX(index);
		int y    = (int) e.getY(index);
		int id   = (int) e.getPointerId(index);
		
		switch (action) {
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				nativeOnTouch( 0, x, y, id);
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
				nativeOnTouch( 1, x, y, id);
				break;
			case MotionEvent.ACTION_MOVE:
				for(int i = 0; i < e.getPointerCount(); ++i)
				{
					x  = (int) e.getX(i);
					y  = (int) e.getY(i);
					id = (int) e.getPointerId(i);
					nativeOnTouch(2, x, y, id);
				}
				break;
			default: break;
		}
		
		return true;
	}
	
	private native void nativeOnTouch( int type, int x, int y, int id);
}
