package com.superstudio.android.ourGame;

import android.util.Log;
import android.app.Activity;
import android.view.KeyEvent;
import android.os.Bundle;
import android.opengl.GLSurfaceView;
import android.view.WindowManager;
import android.view.Window;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdSize;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.RelativeLayout;
//import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.graphics.Color;
import android.media.AudioManager;
import android.content.SharedPreferences;

//import android.widget.Button;
//import android.widget.EditText;

//import com.facebook.widget.LoginButton;
import android.view.View.OnClickListener;

import org.brickred.socialauth.Photo;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import android.widget.Toast;

public class Game extends Activity
{
	private GLSurfaceView mGLView = null;
	private AdView mAdView;
	private SoundManager mSoundManager = null;
    private boolean isLogged = false;

    // SocialAuth Component
    SocialAuthAdapter adapter;

    private native void nativeInit();
    private native void nativeOnKeyDown( int keyCode);
    private native void nativeOnKeyUp( int keyCode);
    private native void nativeOnTouch(int t, int x, int y, int id);

    final static String AD_UNIT_ID = "ca-app-pub-3940256099942544/6300978111";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);      
        System.loadLibrary("Android");

        nativeInit();

        // Create an ad.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(AD_UNIT_ID);
        mAdView.setBackgroundColor(Color.TRANSPARENT); 
        // Add the AdView to the view hierarchy. The view will have no size
        // until the ad is loaded.
        RelativeLayout layout = new RelativeLayout(this);
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,    LayoutParams.MATCH_PARENT));
        // Create an ad request.
        // get test ads on a physical device.
        AdRequest adRequest = new AdRequest.Builder()
        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addTestDevice("4A8CCB862D4AA74EA272039DB08E5746")
        .build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);


        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
           WindowManager.LayoutParams.FLAG_FULLSCREEN );
		// Turn off the title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        mGLView = new GameGLSurfaceView(this);


        RelativeLayout.LayoutParams adParams = 
        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 
            RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);   


        // LinearLayout L2 = new LinearLayout(this);

        // L2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        adapter = new SocialAuthAdapter(new ResponseListener());     

        // Button b1 = new Button(this);
        // b1.setText("SHARE");
        // b1.setOnClickListener(new OnClickListener() 
        // {
        //     public void onClick(View v) 
        //     {
        //         adapter.authorize(Game.this, Provider.FACEBOOK);
        //     }
        // });        

        // L2.addView(b1,(new LinearLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT,0.8f)));

        layout.addView(mGLView);
        layout.addView(mAdView, adParams);
        //layout.addView(L2);

        setContentView(layout);

        hideAd();
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mSoundManager = new SoundManager(this);
    }

    @Override
    protected void onPause()
    {
        Log.i("GAME" , "onPause");
        super.onPause();
        if (mAdView != null) 
        {
            mAdView.pause();
        }        
        mGLView.onPause();
		mSoundManager.pauseMusic();
    }

    @Override
    protected void onResume()
    {
        Log.i("GAME" , "onResume");
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }		
        mGLView.onResume();
		mSoundManager.resumeMusic();
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
		mSoundManager.stopMusic();
    }    

    public void Exit()
    {
      mGLView = null;
      int pid = android.os.Process.myPid();
      android.os.Process.killProcess(pid);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent msg)
    {
    		if(keyCode == 24 || keyCode == 25 || keyCode == KeyEvent.KEYCODE_CAMERA)//volume side keys
         return false;

         if(keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_SEARCH)
         {
             msg.startTracking();
         }

         nativeOnKeyDown(keyCode);
         return true;
     }

     @Override
     public boolean onKeyUp(int keyCode, KeyEvent msg)
     {
    		if(keyCode == 24 || keyCode == 25 ||keyCode == KeyEvent.KEYCODE_CAMERA)//volume side keys
         return false;

         nativeOnKeyUp(keyCode);
         return true;
     }

    public void showAd()
    {
        Log.i("GAME" , "Show ad");

        runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (mAdView != null
                    && mAdView.getVisibility() == AdView.GONE) {

                    AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

                    mAdView.loadAd(adRequest);
                    mAdView.setVisibility (View.VISIBLE);
                }                    
            }
        });
    }

    public void hideAd()
    {
        Log.i("GAME" , "hide ad");

        runOnUiThread(new Runnable()
        {
            public void run()
            {        
                if (mAdView != null
                    && mAdView.getVisibility() == AdView.VISIBLE) {
                        mAdView.destroy();
                        mAdView.setVisibility (View.GONE);       
                }
            }
        });        
    }

    public void login()
    {
        Log.i("GAME" , "logIn");
        if (isLogged == false)
        {
            runOnUiThread(new Runnable()
            {
                public void run()
                {          
                    adapter.authorize(Game.this, Provider.FACEBOOK);
                }
            });   
            isLogged = true;
        }
    }

    public void share()
    {
        Log.i("GAME" , "share");
        if (isLogged == true)
        {
            runOnUiThread(new Runnable()
            {
                public void run()
                {         
                    adapter.updateStatus("Hello World", new MessageListener(), false);
                }
            });
        }else
        {
            login();
        }
    }

    public int LoadPref()
    {
        SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
        int level = sharedPref.getInt("Level", -1);
        return level;
    }

    public void SavePref(int level)
    {
        SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("Level", level);
        editor.commit();
    }

    private final class ResponseListener implements DialogListener 
    {
        @Override
        public void onComplete(Bundle values) {
            Log.d("ShareButton", "Complete");
            Toast.makeText(Game.this, "Connect to FB successfully!",Toast.LENGTH_LONG).show();
        }


        @Override
        public void onError(SocialAuthError error) {
            Log.d("ShareButton", "Authentication Error: " + error.getMessage());
            Toast.makeText(Game.this, "Could not connect to FB!",Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCancel() {
            Log.d("ShareButton", "Authentication Cancelled");
            Toast.makeText(Game.this, "User Cancelled!",Toast.LENGTH_LONG).show();
        }

        @Override
        public void onBack() {
            Log.d("Share-Button", "Dialog Closed by pressing Back Key");
        }   
    }

    // To get status of message after authentication
    private final class MessageListener implements SocialAuthListener<Integer> {
        @Override
        public void onExecute(String provider, Integer t) {

            Integer status = t;
            if (status.intValue() == 200 || status.intValue() == 201 ||status.intValue() == 204)
               Toast.makeText(Game.this, "Message posted!",Toast.LENGTH_LONG).show();
           else
            Toast.makeText(Game.this, "Message not posted! :(",Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(SocialAuthError e) {
            Toast.makeText(Game.this, "Message not posted! :(",Toast.LENGTH_LONG).show();
        }   
    }

	//To do: On Touch function
}
