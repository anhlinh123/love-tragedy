package com.superstudio.android.ourGame;

import android.opengl.GLSurfaceView;
import android.util.Log;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

public class GameRenderer implements GLSurfaceView.Renderer
{
	public GameRenderer()
	{
        Log.i("GAMERENDERER", "GameRenderer");
	}

    public void onSurfaceCreated(GL10 unused, EGLConfig config)
	{
        Log.i("GAMERENDERER", "GameRenderer onSurfaceCreated");
		nativeInit();
	}

    public void onSurfaceChanged(GL10 gl, int w, int h)
	{
        Log.i("GAMERENDERER", "GameRenderer onSurfaceChanged");
        nativeResize(w, h);
    }

    public void onDrawFrame(GL10 unused)
    {
        nativeRender();
    }

	private native void nativeInit();
    private native void nativeResize(int w, int h);
    private native void nativeRender();
    private native void nativeDestroy();
}
