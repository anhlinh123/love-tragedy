
#include "stdafx.h"
#include "Math.h"
#include <cmath>
#include <math.h>
#include <limits>

//Vector2

GLfloat Vector2::Length()
{
	return sqrt(x*x + y*y);
}

Vector2 & Vector2::Normalize()
{
	GLfloat lenInv = 1.0f / Length();
	x *= lenInv;
	y *= lenInv;

	return *this;
}

Vector2 Vector2::operator + (const Vector2 & vector)
{
	return Vector2(x + vector.x, y + vector.y);
}

Vector2 & Vector2::operator += (const Vector2 & vector)
{
	x += vector.x;
	y += vector.y;

	return *this;
}

Vector2 Vector2::operator - ()
{
	return Vector2(-x, -y);
}

Vector2 Vector2::operator - (const Vector2 & vector)
{
	return Vector2(x - vector.x, y - vector.y);
}

Vector2 & Vector2::operator -= (const Vector2 & vector)
{
	x -= vector.x;
	y -= vector.y;

	return *this;
}

Vector2 Vector2::operator * (GLfloat k)
{
	return Vector2(x * k, y * k);
}

Vector2 & Vector2::operator *= (GLfloat k)
{
	x *= k;
	y *= k;

	return *this;
}

Vector2 Vector2::operator / (GLfloat k)
{
	GLfloat kInv = 1.0f / k;
	return Vector2(x * kInv, y * kInv);
}

Vector2 & Vector2::operator /= (GLfloat k)
{
	return operator *= (1.0f / k);
}

Vector2 & Vector2::operator = (const Vector2 & vector)
{
	x = vector.x;
	y = vector.y;

	return *this;
}

GLfloat Vector2::operator [] (unsigned int idx)
{
	return (&x)[idx];
}

Vector2 Vector2::Modulate(const Vector2 & vector)
{
	return Vector2(x * vector.x, y * vector.y);
}

GLfloat Vector2::Dot(const Vector2 & vector)
{
	return x * vector.x + y * vector.y;
}

//Vector3

GLfloat Vector3::Length() const
{
	return sqrt(x*x + y*y + z*z);
}

Vector3 & Vector3::Normalize()
{
	GLfloat lenInv = 1.0f / Length();
	x *= lenInv;
	y *= lenInv;
	z *= lenInv;

	return *this;
}

// implement a more precise Vector3 ==, in fact of coordinates are made of float values.
bool  Vector3::SameDirectionWith(const Vector3& other)
{
	return (this->Dot(other) > 0.98f);
}

Vector3 Vector3::operator + (const Vector3 & vector)
{
	return Vector3(x + vector.x, y + vector.y, z + vector.z);
}

Vector3 & Vector3::operator += (const Vector3 & vector)
{
	x += vector.x;
	y += vector.y;
	z += vector.z;

	return *this;
}

Vector3 Vector3::operator - ()
{
	return Vector3(-x, -y, -z);
}

Vector3 Vector3::operator - (const Vector3 & vector)
{
	return Vector3(x - vector.x, y - vector.y, z - vector.z);
}

Vector3 & Vector3::operator -= (const Vector3 & vector)
{
	x -= vector.x;
	y -= vector.y;
	z -= vector.z;

	return *this;
}

Vector3 Vector3::operator * (GLfloat k) const
{
	return Vector3(x * k, y * k, z * k);
}

Vector3 & Vector3::operator *= (GLfloat k)
{
	x *= k;
	y *= k;
	z *= k;

	return *this;
}

Vector3 Vector3::operator / (GLfloat k)
{
	GLfloat kInv = 1.0f / k;
	return Vector3(x * kInv, y * kInv, z * kInv);
}
	
Vector3 & Vector3::operator /= (GLfloat k)
{
	return operator *= (1.0f / k);
}

Vector3 & Vector3::operator = (const Vector3 & vector)
{
	x = vector.x;
	y = vector.y;
	z = vector.z;
	
	return *this;
}

GLfloat Vector3::operator [] (unsigned int idx)
{
	return (&x)[idx];
}

Vector3 Vector3::Modulate(const Vector3 & vector)
{
	return Vector3(x * vector.x, y * vector.y, z * vector.z);
}

GLfloat Vector3::Dot(const Vector3 & vector) const
{
	return x * vector.x + y * vector.y + z * vector.z;
}

Vector3 Vector3::Cross(const Vector3 & vector) const
{
	return Vector3(y * vector.z -  z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x);
}

//Vector4

GLfloat Vector4::Length()
{
	return sqrt(x*x + y*y + z*z + w*w);
}

Vector4 & Vector4::Normalize()
{
	GLfloat lenInv = 1.0f / Length();
	x *= lenInv;
	y *= lenInv;
	z *= lenInv;
	w *= lenInv;

	return *this;
}

Vector4 Vector4::operator + (const Vector4 & vector)
{
	return Vector4(x + vector.x, y + vector.y, z + vector.z, w + vector.w);
}

Vector4 & Vector4::operator += (const Vector4 & vector)
{
	x += vector.x;
	y += vector.y;
	z += vector.z;
	w += vector.w;

	return *this;
}

Vector4 Vector4::operator - ()
{
	return Vector4(-x, -y, -z, -w);
}

Vector4 Vector4::operator - (const Vector4 & vector)
{
	return Vector4(x - vector.x, y - vector.y, z - vector.z, w - vector.w);
}

Vector4 & Vector4::operator -= (const Vector4 & vector)
{
	x -= vector.x;
	y -= vector.y;
	z -= vector.z;
	w -= vector.w;

	return *this;
}

Vector4 Vector4::operator * (GLfloat k)
{
	return Vector4(x * k, y * k, z * k, w * k);
}

Vector4 & Vector4::operator *= (GLfloat k)
{
	x *= k;
	y *= k;
	z *= k;
	w *= k;

	return *this;
}

Vector4 Vector4::operator / (GLfloat k)
{
	GLfloat kInv = 1.0f / k;
	return Vector4(x * kInv, y * kInv, z * kInv, w * kInv);
}

Vector4 & Vector4::operator /= (GLfloat k)
{
	return operator *= (1.0f / k);
}

Vector4 & Vector4::operator = (const Vector4 & vector)
{
	x = vector.x;
	y = vector.y;
	z = vector.z;
	w = vector.w;
	return *this;
}

GLfloat Vector4::operator [] (unsigned int idx)
{
	return (&x)[idx];
}

Vector4 Vector4::Modulate(const Vector4 & vector)
{
	return Vector4(x * vector.x, y * vector.y, z * vector.z, w * vector.w);
}

GLfloat Vector4::Dot(const Vector4 & vector)
{
	return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
}


Vector4 Vector4::operator * ( Matrix & m )
{
	Vector4 res;
	res.x = x * m.m[0][0] + y * m.m[1][0] + z * m.m[2][0] + w * m.m[3][0];
	res.y = x * m.m[0][1] + y * m.m[1][1] + z * m.m[2][1] + w * m.m[3][1];
	res.z = x * m.m[0][2] + y * m.m[1][2] + z * m.m[2][2] + w * m.m[3][2];
	res.w = x * m.m[0][3] + y * m.m[1][3] + z * m.m[2][3] + w * m.m[3][3];

	return res;
}


//Matrix 4 X 4

Matrix::Matrix(GLfloat val)
{
	m[0][0] = val; m[0][1] = val; m[0][2] = val; m[0][3] = val;
	m[1][0] = val; m[1][1] = val; m[1][2] = val; m[1][3] = val;
	m[2][0] = val; m[2][1] = val; m[2][2] = val; m[2][3] = val;
	m[3][0] = val; m[3][1] = val; m[3][2] = val; m[3][3] = val;
}

Matrix::Matrix(const Matrix & mat)
{
	m[0][0] = mat.m[0][0]; m[0][1] = mat.m[0][1]; m[0][2] = mat.m[0][2]; m[0][3] = mat.m[0][3];
	m[1][0] = mat.m[1][0]; m[1][1] = mat.m[1][1]; m[1][2] = mat.m[1][2]; m[1][3] = mat.m[1][3];
	m[2][0] = mat.m[2][0]; m[2][1] = mat.m[2][1]; m[2][2] = mat.m[2][2]; m[2][3] = mat.m[2][3];
	m[3][0] = mat.m[3][0]; m[3][1] = mat.m[3][1]; m[3][2] = mat.m[3][2]; m[3][3] = mat.m[3][3];
}

Matrix::Matrix (const Matrix3& m)
{
	a1 = m.a1; a2 = m.a2; a3 = m.a3; a4 = 0.0;
	b1 = m.b1; b2 = m.b2; b3 = m.b3; b4 = 0.0;
	c1 = m.c1; c2 = m.c2; c3 = m.c3; c4 = 0.0;
	d1 = 0.0; d2 = 0.0; d3 = 0.0; d4 = 1.0;
}

Matrix::Matrix (const Vector3& scaling, const Quaternion& rotation, const Vector3& position)
{
	// build a 3x3 rotation matrix
	Matrix3 m = rotation.GetMatrix();

	a1 = m.a1 * scaling.x;
	a2 = m.a2 * scaling.x;
	a3 = m.a3 * scaling.x;
	a4 = position.x;

	b1 = m.b1 * scaling.y;
	b2 = m.b2 * scaling.y;
	b3 = m.b3 * scaling.y;
	b4 = position.y;
	
	c1 = m.c1 * scaling.z;
	c2 = m.c2 * scaling.z;
	c3 = m.c3 * scaling.z;
	c4= position.z;

	d1 = 0.0;
	d2 = 0.0;
	d3 = 0.0;
	d4 = 1.0;
}

Matrix::Matrix (GLfloat _a1, GLfloat _a2, GLfloat _a3, GLfloat _a4,
			 GLfloat _b1, GLfloat _b2, GLfloat _b3, GLfloat _b4,
			 GLfloat _c1, GLfloat _c2, GLfloat _c3, GLfloat _c4,
			 GLfloat _d1, GLfloat _d2, GLfloat _d3, GLfloat _d4) :
	a1(_a1), a2(_a2), a3(_a3), a4(_a4),
	b1(_b1), b2(_b2), b3(_b3), b4(_b4),
	c1(_c1), c2(_c2), c3(_c3), c4(_c4),
	d1(_d1), d2(_d2), d3(_d3), d4(_d4)
{

}

Matrix & Matrix::SetZero()
{
	m[0][0] = 0.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
	m[1][0] = 0.0f; m[1][1] = 0.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
	m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 0.0f; m[2][3] = 0.0f;
	m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 0.0f;

	return *this;
}

Matrix & Matrix::SetIdentity()
{
	m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
	m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
	m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
	m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;

	return *this;
}

Matrix & Matrix::SetRotationX(GLfloat angle)
{
	GLfloat s = sinf(angle);
	GLfloat c = cosf(angle);
	m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
	m[1][0] = 0.0f; m[1][1] =    c; m[1][2] =    s; m[1][3] = 0.0f;
	m[2][0] = 0.0f; m[2][1] =   -s; m[2][2] =    c; m[2][3] = 0.0f;
	m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
	return *this;
}

Matrix & Matrix::SetRotationY(GLfloat angle)
{
	GLfloat s = sinf(angle);
	GLfloat c = cosf(angle);
	m[0][0] =    c; m[0][1] = 0.0f; m[0][2] =   -s; m[0][3] = 0.0f;
	m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
	m[2][0] =    s; m[2][1] = 0.0f; m[2][2] =    c; m[2][3] = 0.0f;
	m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
	return *this;
}

Matrix & Matrix::SetRotationZ(GLfloat angle)
{
	GLfloat s = sinf(angle);
	GLfloat c = cosf(angle);
	m[0][0] =    c; m[0][1] =    s; m[0][2] = 0.0f; m[0][3] = 0.0f;
	m[1][0] =   -s; m[1][1] =    c; m[1][2] = 0.0f; m[1][3] = 0.0f;
	m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
	m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
	return *this;
}

Matrix & Matrix::SetRotationAngleAxis( float angle, float x, float y, float z )
{
	float sinAngle, cosAngle;
	float mag = sqrtf(x * x + y * y + z * z);

	sinAngle = sinf ( angle );
	cosAngle = cosf ( angle );
	if ( mag > 0.0f )
	{
		float xx, yy, zz, xy, yz, zx, xs, ys, zs;
		float oneMinusCos;
		float magInv = 1.0f / mag;

		x *= magInv;
		y *= magInv;
		z *= magInv;

		xx = x * x;
		yy = y * y;
		zz = z * z;
		xy = x * y;
		yz = y * z;
		zx = z * x;
		xs = x * sinAngle;
		ys = y * sinAngle;
		zs = z * sinAngle;
		oneMinusCos = 1.0f - cosAngle;

		m[0][0] = (oneMinusCos * xx) + cosAngle;
		m[1][0] = (oneMinusCos * xy) - zs;
		m[2][0] = (oneMinusCos * zx) + ys;
		m[3][0] = 0.0F; 

		m[0][1] = (oneMinusCos * xy) + zs;
		m[1][1] = (oneMinusCos * yy) + cosAngle;
		m[2][1] = (oneMinusCos * yz) - xs;
		m[3][1] = 0.0F;

		m[0][2] = (oneMinusCos * zx) - ys;
		m[1][2] = (oneMinusCos * yz) + xs;
		m[2][2] = (oneMinusCos * zz) + cosAngle;
		m[3][2] = 0.0F; 

		m[0][3] = 0.0F;
		m[1][3] = 0.0F;
		m[2][3] = 0.0F;
		m[3][3] = 1.0F;
		return * this;
	}
	else
		return SetIdentity();
}

Matrix & Matrix::SetRotationInverse( Vector3 &xaxis, Vector3 &yaxis, Vector3 &zaxis ) //for V calculation
{
	m[0][0] = xaxis.x; m[0][1] = yaxis.x; m[0][2] = zaxis.x; m[0][3] = 0.0F;
	m[1][0] = xaxis.y; m[1][1] = yaxis.y; m[1][2] = zaxis.y; m[1][3] = 0.0F;
	m[2][0] = xaxis.z; m[2][1] = yaxis.z; m[2][2] = zaxis.z; m[2][3] = 0.0F;
	m[3][0] = 0.0F; m[3][1] = 0.0F; m[3][2] = 0.0F; m[3][3] = 1.0F;

	return * this;
}

Matrix & Matrix::SetRotation( Vector3 &xaxis, Vector3 &yaxis, Vector3 &zaxis ) //for W calculation
{
	m[0][0] = xaxis.x; m[0][1] = xaxis.y; m[0][2] = xaxis.z; m[0][3] = 0.0F;
	m[1][0] = yaxis.x; m[1][1] = yaxis.y; m[1][2] = yaxis.z; m[1][3] = 0.0F;
	m[2][0] = zaxis.x; m[2][1] = zaxis.y; m[2][2] = zaxis.z; m[2][3] = 0.0F;
	m[3][0] = 0.0F; m[3][1] = 0.0F; m[3][2] = 0.0F; m[3][3] = 1.0F;

	return * this;
}

Matrix & Matrix::SetScale(GLfloat scale)
{
	m[0][0] = scale; m[0][1] = 0.0f;  m[0][2] = 0.0f;  m[0][3] = 0.0f;
	m[1][0] = 0.0f;  m[1][1] = scale; m[1][2] = 0.0f;  m[1][3] = 0.0f;
	m[2][0] = 0.0f;  m[2][1] = 0.0f;  m[2][2] = scale; m[2][3] = 0.0f;
	m[3][0] = 0.0f;  m[3][1] = 0.0f;  m[3][2] = 0.0f;  m[3][3] = 1.0f;

	return *this;
}

Matrix & Matrix::SetScale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ)
{
	m[0][0] = scaleX; m[0][1] = 0.0f;   m[0][2] = 0.0f;   m[0][3] = 0.0f;
	m[1][0] = 0.0f;   m[1][1] = scaleY; m[1][2] = 0.0f;   m[1][3] = 0.0f;
	m[2][0] = 0.0f;   m[2][1] = 0.0f;   m[2][2] = scaleZ; m[2][3] = 0.0f;
	m[3][0] = 0.0f;   m[3][1] = 0.0f;   m[3][2] = 0.0f;   m[3][3] = 1.0f;
	return *this;
}

Matrix & Matrix::SetScale(GLfloat * pScale)
{
	m[0][0] = pScale[0];   m[0][1] = 0.0f;        m[0][2] = 0.0f;        m[0][3] = 0.0f;
	m[1][0] = 0.0f;        m[1][1] = pScale[1];   m[1][2] = 0.0f;        m[1][3] = 0.0f;
	m[2][0] = 0.0f;        m[2][1] = 0.0f;        m[2][2] = pScale[2];   m[2][3] = 0.0f;
	m[3][0] = 0.0f;        m[3][1] = 0.0f;        m[3][2] = 0.0f;        m[3][3] = 1.0f;
	
	return *this;
}

Matrix & Matrix::SetScale(const Vector3 & scaleVec)
{
	m[0][0] = scaleVec.x; m[0][1] = 0.0f;       m[0][2] = 0.0f;       m[0][3] = 0.0f;
	m[1][0] = 0.0f;       m[1][1] = scaleVec.y; m[1][2] = 0.0f;       m[1][3] = 0.0f;
	m[2][0] = 0.0f;       m[2][1] = 0.0f;       m[2][2] = scaleVec.z; m[2][3] = 0.0f;
	m[3][0] = 0.0f;       m[3][1] = 0.0f;       m[3][2] = 0.0f;       m[3][3] = 1.0f;
	
	return *this;
}

Matrix & Matrix::SetTranslation(GLfloat x, GLfloat y, GLfloat z)
{
	m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
	m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
	m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
	m[3][0] =    x; m[3][1] =    y; m[3][2] =    z; m[3][3] = 1.0f;
	
	return *this;
}

Matrix & Matrix::SetTranslation( GLfloat *pTrans)
{
	m[0][0] = 1.0f;      m[0][1] = 0.0f;      m[0][2] = 0.0f;      m[0][3] = 0.0f;
	m[1][0] = 0.0f;      m[1][1] = 1.0f;      m[1][2] = 0.0f;      m[1][3] = 0.0f;
	m[2][0] = 0.0f;      m[2][1] = 0.0f;      m[2][2] = 1.0f;      m[2][3] = 0.0f;
	m[3][0] = pTrans[0]; m[3][1] = pTrans[1]; m[3][2] = pTrans[2]; m[3][3] = 1.0f;
	
	return *this;
}

Matrix & Matrix::SetTranslation(const Vector3 &vec )
{
	m[0][0] =  1.0f; m[0][1] =  0.0f; m[0][2] =  0.0f; m[0][3] = 0.0f;
	m[1][0] =  0.0f; m[1][1] =  1.0f; m[1][2] =  0.0f; m[1][3] = 0.0f;
	m[2][0] =  0.0f; m[2][1] =  0.0f; m[2][2] =  1.0f; m[2][3] = 0.0f;
	m[3][0] = vec.x; m[3][1] = vec.y; m[3][2] = vec.z; m[3][3] = 1.0f;
	
	return *this;
}

Matrix & Matrix::SetPerspective(GLfloat fovY, GLfloat aspect, GLfloat nearPlane, GLfloat farPlane)
{
	GLfloat height = 2.0f * nearPlane * tanf(fovY * 0.5f);
	GLfloat width = height * aspect;
	GLfloat n2 = 2.0f * nearPlane;
	GLfloat rcpnmf = 1.f / (nearPlane - farPlane);

	m[0][0] = n2 / width;	
	m[1][0] = 0;
	m[2][0] = 0;
	m[3][0] = 0;

	m[0][1] = 0;
	m[1][1] = n2 / height;
	m[2][1] = 0;
	m[3][1] = 0;

	m[0][2] = 0;
	m[1][2] = 0;
	m[2][2] = (farPlane + nearPlane) * rcpnmf;	
	m[3][2] = farPlane * rcpnmf * n2;

	m[0][3] = 0;
	m[1][3] = 0;
	m[2][3] = -1.f;
	m[3][3] = 0;

	return *this;
}

Matrix Matrix::Transpose()
{
	Matrix res;
	res.m[0][0] = m[0][0]; res.m[0][1] = m[1][0]; res.m[0][2] = m[2][0]; res.m[0][3] = m[3][0];
	res.m[1][0] = m[0][1]; res.m[1][1] = m[1][1]; res.m[1][2] = m[2][1]; res.m[1][3] = m[3][1];
	res.m[2][0] = m[0][2]; res.m[2][1] = m[1][2]; res.m[2][2] = m[2][2]; res.m[2][3] = m[3][2];
	res.m[3][0] = m[0][3]; res.m[3][1] = m[1][3]; res.m[3][2] = m[2][3]; res.m[3][3] = m[3][3];
	return res;
}

GLfloat Matrix::Determinant() const
{
	return a1*b2*c3*d4 - a1*b2*c4*d3 + a1*b3*c4*d2 - a1*b3*c2*d4
		+ a1*b4*c2*d3 - a1*b4*c3*d2 - a2*b3*c4*d1 + a2*b3*c1*d4
		- a2*b4*c1*d3 + a2*b4*c3*d1 - a2*b1*c3*d4 + a2*b1*c4*d3
		+ a3*b4*c1*d2 - a3*b4*c2*d1 + a3*b1*c2*d4 - a3*b1*c4*d2
		+ a3*b2*c4*d1 - a3*b2*c1*d4 - a4*b1*c2*d3 + a4*b1*c3*d2
		- a4*b2*c3*d1 + a4*b2*c1*d3 - a4*b3*c1*d2 + a4*b3*c2*d1;
}

Matrix& Matrix::Inverse()
{
	// Compute the reciprocal determinant
	const GLfloat det = Determinant();
	if(det == 0.0)
	{
		// Matrix not invertible. Setting all elements to nan is not really
		// correct in a mathematical sense but it is easy to debug for the
		// programmer.
		const GLfloat nan = std::numeric_limits<float>::quiet_NaN();
		*this = Matrix(
			nan,nan,nan,nan,
			nan,nan,nan,nan,
			nan,nan,nan,nan,
			nan,nan,nan,nan);

		return *this;
	}

	const GLfloat invdet = 1.0f / det;

	Matrix res;
	res.a1 = invdet  * (b2 * (c3 * d4 - c4 * d3) + b3 * (c4 * d2 - c2 * d4) + b4 * (c2 * d3 - c3 * d2));
	res.a2 = -invdet * (a2 * (c3 * d4 - c4 * d3) + a3 * (c4 * d2 - c2 * d4) + a4 * (c2 * d3 - c3 * d2));
	res.a3 = invdet  * (a2 * (b3 * d4 - b4 * d3) + a3 * (b4 * d2 - b2 * d4) + a4 * (b2 * d3 - b3 * d2));
	res.a4 = -invdet * (a2 * (b3 * c4 - b4 * c3) + a3 * (b4 * c2 - b2 * c4) + a4 * (b2 * c3 - b3 * c2));
	res.b1 = -invdet * (b1 * (c3 * d4 - c4 * d3) + b3 * (c4 * d1 - c1 * d4) + b4 * (c1 * d3 - c3 * d1));
	res.b2 = invdet  * (a1 * (c3 * d4 - c4 * d3) + a3 * (c4 * d1 - c1 * d4) + a4 * (c1 * d3 - c3 * d1));
	res.b3 = -invdet * (a1 * (b3 * d4 - b4 * d3) + a3 * (b4 * d1 - b1 * d4) + a4 * (b1 * d3 - b3 * d1));
	res.b4 = invdet  * (a1 * (b3 * c4 - b4 * c3) + a3 * (b4 * c1 - b1 * c4) + a4 * (b1 * c3 - b3 * c1));
	res.c1 = invdet  * (b1 * (c2 * d4 - c4 * d2) + b2 * (c4 * d1 - c1 * d4) + b4 * (c1 * d2 - c2 * d1));
	res.c2 = -invdet * (a1 * (c2 * d4 - c4 * d2) + a2 * (c4 * d1 - c1 * d4) + a4 * (c1 * d2 - c2 * d1));
	res.c3 = invdet  * (a1 * (b2 * d4 - b4 * d2) + a2 * (b4 * d1 - b1 * d4) + a4 * (b1 * d2 - b2 * d1));
	res.c4 = -invdet * (a1 * (b2 * c4 - b4 * c2) + a2 * (b4 * c1 - b1 * c4) + a4 * (b1 * c2 - b2 * c1));
	res.d1 = -invdet * (b1 * (c2 * d3 - c3 * d2) + b2 * (c3 * d1 - c1 * d3) + b3 * (c1 * d2 - c2 * d1));
	res.d2 = invdet  * (a1 * (c2 * d3 - c3 * d2) + a2 * (c3 * d1 - c1 * d3) + a3 * (c1 * d2 - c2 * d1));
	res.d3 = -invdet * (a1 * (b2 * d3 - b3 * d2) + a2 * (b3 * d1 - b1 * d3) + a3 * (b1 * d2 - b2 * d1));
	res.d4 = invdet  * (a1 * (b2 * c3 - b3 * c2) + a2 * (b3 * c1 - b1 * c3) + a3 * (b1 * c2 - b2 * c1));
	*this = res;

	return *this;
}

Matrix Matrix::operator + (Matrix & mat)
{
	Matrix res( *this );
	res += mat;
	return res;
}

Matrix & Matrix::operator += (Matrix & mat)
{
	m[0][0] += mat.m[0][0]; m[0][1] += mat.m[0][1]; m[0][2] += mat.m[0][2]; m[0][3] += mat.m[0][3];
	m[1][0] += mat.m[1][0]; m[1][1] += mat.m[1][1]; m[1][2] += mat.m[1][2]; m[1][3] += mat.m[1][3];
	m[2][0] += mat.m[2][0]; m[2][1] += mat.m[2][1]; m[2][2] += mat.m[2][2]; m[2][3] += mat.m[2][3];
	m[3][0] += mat.m[3][0]; m[3][1] += mat.m[3][1]; m[3][2] += mat.m[3][2]; m[3][3] += mat.m[3][3];

	return *this;
}

Matrix Matrix::operator - (Matrix & mat)
{
	Matrix res( *this );
	res -= mat;
	return res;
}

Matrix & Matrix::operator -= (Matrix & mat)
{
	m[0][0] -= mat.m[0][0]; m[0][1] -= mat.m[0][1]; m[0][2] -= mat.m[0][2]; m[0][3] -= mat.m[0][3];
	m[1][0] -= mat.m[1][0]; m[1][1] -= mat.m[1][1]; m[1][2] -= mat.m[1][2]; m[1][3] -= mat.m[1][3];
	m[2][0] -= mat.m[2][0]; m[2][1] -= mat.m[2][1]; m[2][2] -= mat.m[2][2]; m[2][3] -= mat.m[2][3];
	m[3][0] -= mat.m[3][0]; m[3][1] -= mat.m[3][1]; m[3][2] -= mat.m[3][2]; m[3][3] -= mat.m[3][3];

	return *this;
}

Matrix Matrix::operator * (const Matrix & mat)
{
	Matrix res;
	res.m[0][0] = m[0][0] * mat.m[0][0] + m[0][1] * mat.m[1][0] + m[0][2] * mat.m[2][0] + m[0][3] * mat.m[3][0];
	res.m[0][1] = m[0][0] * mat.m[0][1] + m[0][1] * mat.m[1][1] + m[0][2] * mat.m[2][1] + m[0][3] * mat.m[3][1];
	res.m[0][2] = m[0][0] * mat.m[0][2] + m[0][1] * mat.m[1][2] + m[0][2] * mat.m[2][2] + m[0][3] * mat.m[3][2];
	res.m[0][3] = m[0][0] * mat.m[0][3] + m[0][1] * mat.m[1][3] + m[0][2] * mat.m[2][3] + m[0][3] * mat.m[3][3];

	res.m[1][0] = m[1][0] * mat.m[0][0] + m[1][1] * mat.m[1][0] + m[1][2] * mat.m[2][0] + m[1][3] * mat.m[3][0];
	res.m[1][1] = m[1][0] * mat.m[0][1] + m[1][1] * mat.m[1][1] + m[1][2] * mat.m[2][1] + m[1][3] * mat.m[3][1];
	res.m[1][2] = m[1][0] * mat.m[0][2] + m[1][1] * mat.m[1][2] + m[1][2] * mat.m[2][2] + m[1][3] * mat.m[3][2];
	res.m[1][3] = m[1][0] * mat.m[0][3] + m[1][1] * mat.m[1][3] + m[1][2] * mat.m[2][3] + m[1][3] * mat.m[3][3];

	res.m[2][0] = m[2][0] * mat.m[0][0] + m[2][1] * mat.m[1][0] + m[2][2] * mat.m[2][0] + m[2][3] * mat.m[3][0];
	res.m[2][1] = m[2][0] * mat.m[0][1] + m[2][1] * mat.m[1][1] + m[2][2] * mat.m[2][1] + m[2][3] * mat.m[3][1];
	res.m[2][2] = m[2][0] * mat.m[0][2] + m[2][1] * mat.m[1][2] + m[2][2] * mat.m[2][2] + m[2][3] * mat.m[3][2];
	res.m[2][3] = m[2][0] * mat.m[0][3] + m[2][1] * mat.m[1][3] + m[2][2] * mat.m[2][3] + m[2][3] * mat.m[3][3];

	res.m[3][0] = m[3][0] * mat.m[0][0] + m[3][1] * mat.m[1][0] + m[3][2] * mat.m[2][0] + m[3][3] * mat.m[3][0];
	res.m[3][1] = m[3][0] * mat.m[0][1] + m[3][1] * mat.m[1][1] + m[3][2] * mat.m[2][1] + m[3][3] * mat.m[3][1];
	res.m[3][2] = m[3][0] * mat.m[0][2] + m[3][1] * mat.m[1][2] + m[3][2] * mat.m[2][2] + m[3][3] * mat.m[3][2];
	res.m[3][3] = m[3][0] * mat.m[0][3] + m[3][1] * mat.m[1][3] + m[3][2] * mat.m[2][3] + m[3][3] * mat.m[3][3];

	return res;
}

Matrix Matrix::operator * (GLfloat k)
{
	Matrix mat( *this );
	mat *= k;
	return mat;
}

Matrix & Matrix::operator *= (GLfloat k)
{
	m[0][0] *= k; m[0][1] *= k; m[0][2] *= k; m[0][3] *= k;
	m[1][0] *= k; m[1][1] *= k; m[1][2] *= k; m[1][3] *= k;
	m[2][0] *= k; m[2][1] *= k; m[2][2] *= k; m[2][3] *= k;
	m[3][0] *= k; m[3][1] *= k; m[3][2] *= k; m[3][3] *= k;

	return *this;
}


Vector4 Matrix::operator * (const Vector4 & vec)
{
	Vector4 res;
	res.x = vec.x * m[0][0] + vec.y * m[0][1] + vec.z * m[0][2] + vec.w * m[0][3];
	res.y = vec.x * m[1][0] + vec.y * m[1][1] + vec.z * m[1][2] + vec.w * m[1][3];
	res.z = vec.x * m[2][0] + vec.y * m[2][1] + vec.z * m[2][2] + vec.w * m[2][3];
	res.w = vec.x * m[3][0] + vec.y * m[3][1] + vec.z * m[3][2] + vec.w * m[3][3];

	return res;
}

Matrix & Matrix::operator = (const Matrix & mat)
{
	m[0][0] = mat.m[0][0]; m[0][1] = mat.m[0][1]; m[0][2] = mat.m[0][2]; m[0][3] = mat.m[0][3];
	m[1][0] = mat.m[1][0]; m[1][1] = mat.m[1][1]; m[1][2] = mat.m[1][2]; m[1][3] = mat.m[1][3];
	m[2][0] = mat.m[2][0]; m[2][1] = mat.m[2][1]; m[2][2] = mat.m[2][2]; m[2][3] = mat.m[2][3];
	m[3][0] = mat.m[3][0]; m[3][1] = mat.m[3][1]; m[3][2] = mat.m[3][2]; m[3][3] = mat.m[3][3];

	return *this;
}

bool Quaternion::operator== (const Quaternion& o) const
{
	return x == o.x && y == o.y && z == o.z && w == o.w;
}

bool Quaternion::operator!= (const Quaternion& o) const
{
	return !(*this == o);
}

bool Quaternion::Equal(const Quaternion& o, GLfloat epsilon) const {
	return
		std::abs(x - o.x) <= epsilon &&
		std::abs(y - o.y) <= epsilon &&
		std::abs(z - o.z) <= epsilon &&
		std::abs(w - o.w) <= epsilon;
}

Quaternion::Quaternion( const Matrix3 &pRotMatrix)
{
	GLfloat t = pRotMatrix.a1 + pRotMatrix.b2 + pRotMatrix.c3;

	// large enough
	if( t > 0)
	{
		GLfloat s = sqrt(1 + t) * 2.0f;
		x = (pRotMatrix.c2 - pRotMatrix.b3) / s;
		y = (pRotMatrix.a3 - pRotMatrix.c1) / s;
		z = (pRotMatrix.b1 - pRotMatrix.a2) / s;
		w = 0.25f * s;
	} // else we have to check several cases
	else if( pRotMatrix.a1 > pRotMatrix.b2 && pRotMatrix.a1 > pRotMatrix.c3 )  
	{	
		// Column 0: 
		GLfloat s = sqrt( 1.0f + pRotMatrix.a1 - pRotMatrix.b2 - pRotMatrix.c3) * 2.0f;
		x = 0.25f * s;
		y = (pRotMatrix.b1 + pRotMatrix.a2) / s;
		z = (pRotMatrix.a3 + pRotMatrix.c1) / s;
		w = (pRotMatrix.c2 - pRotMatrix.b3) / s;
	} 
	else if( pRotMatrix.b2 > pRotMatrix.c3) 
	{ 
		// Column 1: 
		GLfloat s = sqrt( 1.0f + pRotMatrix.b2 - pRotMatrix.a1 - pRotMatrix.c3) * 2.0f;
		x = (pRotMatrix.b1 + pRotMatrix.a2) / s;
		y = 0.25f * s;
		z = (pRotMatrix.c2 + pRotMatrix.b3) / s;
		w = (pRotMatrix.a3 - pRotMatrix.c1) / s;
	} else 
	{ 
		// Column 2:
		GLfloat s = sqrt( 1.0f + pRotMatrix.c3 - pRotMatrix.a1 - pRotMatrix.b2) * 2.0f;
		x = (pRotMatrix.a3 + pRotMatrix.c1) / s;
		y = (pRotMatrix.c2 + pRotMatrix.b3) / s;
		z = 0.25f * s;
		w = (pRotMatrix.b1 - pRotMatrix.a2) / s;
	}
}

Quaternion::Quaternion( GLfloat fPitch, GLfloat fYaw, GLfloat fRoll )
{
	const GLfloat fSinPitch(sin(fPitch*0.5f));
	const GLfloat fCosPitch(cos(fPitch*0.5f));
	const GLfloat fSinYaw(sin(fYaw*0.5f));
	const GLfloat fCosYaw(cos(fYaw*0.5f));
	const GLfloat fSinRoll(sin(fRoll*0.5f));
	const GLfloat fCosRoll(cos(fRoll*0.5f));
	const GLfloat fCosPitchCosYaw(fCosPitch*fCosYaw);
	const GLfloat fSinPitchSinYaw(fSinPitch*fSinYaw);
	x = fSinRoll * fCosPitchCosYaw     - fCosRoll * fSinPitchSinYaw;
	y = fCosRoll * fSinPitch * fCosYaw + fSinRoll * fCosPitch * fSinYaw;
	z = fCosRoll * fCosPitch * fSinYaw - fSinRoll * fSinPitch * fCosYaw;
	w = fCosRoll * fCosPitchCosYaw     + fSinRoll * fSinPitchSinYaw;
}

Matrix3 Quaternion::GetMatrix() const
{
	Matrix3 resMatrix;
	resMatrix.a1 = 1.0f - 2.0f * (y * y + z * z);
	resMatrix.a2 = 2.0f * (x * y - z * w);
	resMatrix.a3 = 2.0f * (x * z + y * w);
	resMatrix.b1 = 2.0f * (x * y + z * w);
	resMatrix.b2 = 1.0f - 2.0f * (x * x + z * z);
	resMatrix.b3 = 2.0f * (y * z - x * w);
	resMatrix.c1 = 2.0f * (x * z - y * w);
	resMatrix.c2 = 2.0f * (y * z + x * w);
	resMatrix.c3 = 1.0f - 2.0f * (x * x + y * y);

	return resMatrix;
}

Quaternion::Quaternion( Vector3 axis, GLfloat angle)
{
	axis.Normalize();

	const GLfloat sin_a = sin( angle / 2 );
	const GLfloat cos_a = cos( angle / 2 );
	x    = axis.x * sin_a;
	y    = axis.y * sin_a;
	z    = axis.z * sin_a;
	w    = cos_a;
}

Quaternion::Quaternion( Vector3 normalized)
{
	x = normalized.x;
	y = normalized.y;
	z = normalized.z;

	const GLfloat t = 1.0f - (x*x) - (y*y) - (z*z);

	if (t < 0.0f) {
		w = 0.0f;
	}
	else w = sqrt (t);
}

void Quaternion::Interpolate( Quaternion& pOut, const Quaternion& pStart, const Quaternion& pEnd, GLfloat pFactor)
{
	// calc cosine theta
	GLfloat cosom = pStart.x * pEnd.x + pStart.y * pEnd.y + pStart.z * pEnd.z + pStart.w * pEnd.w;

	// adjust signs (if necessary)
	Quaternion end = pEnd;
	if( cosom < 0.0)
	{
		cosom = -cosom;
		end.x = -end.x;   // Reverse all signs
		end.y = -end.y;
		end.z = -end.z;
		end.w = -end.w;
	} 

	// Calculate coefficients
	GLfloat sclp, sclq;
	if( (1.0f - cosom) > 0.0001) // 0.0001 -> some epsillon
	{
		// Standard case (slerp)
		GLfloat omega, sinom;
		omega = acos( cosom); // extract theta from dot product's cos theta
		sinom = sin( omega);
		sclp  = sin( (1.0f - pFactor) * omega) / sinom;
		sclq  = sin( pFactor * omega) / sinom;
	} else
	{
		// Very close, do linear interp (because it's faster)
		sclp = 1.0f - pFactor;
		sclq = pFactor;
	}

	pOut.x = sclp * pStart.x + sclq * end.x;
	pOut.y = sclp * pStart.y + sclq * end.y;
	pOut.z = sclp * pStart.z + sclq * end.z;
	pOut.w = sclp * pStart.w + sclq * end.w;
}

Quaternion& Quaternion::Normalize()
{
	// compute the magnitude and divide through it
	const GLfloat mag = sqrt(x*x + y*y + z*z + w*w);
	if (mag)
	{
		const GLfloat invMag = 1.0f/mag;
		x *= invMag;
		y *= invMag;
		z *= invMag;
		w *= invMag;
	}
	return *this;
}

Quaternion Quaternion::operator* (const Quaternion& t) const
{
	return Quaternion(w*t.w - x*t.x - y*t.y - z*t.z,
		w*t.x + x*t.w + y*t.z - z*t.y,
		w*t.y + y*t.w + z*t.x - x*t.z,
		w*t.z + z*t.w + x*t.y - y*t.x);
}

Quaternion& Quaternion::Conjugate ()
{
	x = -x;
	y = -y;
	z = -z;
	return *this;
}

Vector3 Quaternion::Rotate (const Vector3& v)
{
	Quaternion q2(0.f,v.x,v.y,v.z), q = *this, qinv = q;
	q.Conjugate();

	q = q*q2*qinv;
	return Vector3(q.x,q.y,q.z);

}