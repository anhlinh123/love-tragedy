#pragma once

#include "esUtil.h"


//Vector2

class Vector2
{
public:
	//Constructors
	Vector2() : x(0.0f), y(0.0f){}
	Vector2(GLfloat _x, GLfloat _y) : x(_x), y(_y) {}
	Vector2(GLfloat * pArg) : x(pArg[0]), y(pArg[1]) {}
	Vector2(const Vector2 & vector) : x(vector.x), y(vector.y) {}

	//Vector's operations
	GLfloat Length();
	Vector2 & Normalize();
	Vector2 operator + (const Vector2 & vector);
	Vector2 & operator += (const Vector2 & vector);
	Vector2 operator - ();
	Vector2 operator - (const Vector2 & vector);
	Vector2 & operator -= (const Vector2 & vector);
	Vector2 operator * (GLfloat k);
	Vector2 & operator *= (GLfloat k);
	Vector2 operator / (GLfloat k);
	Vector2 & operator /= (GLfloat k);
	Vector2 & operator = (const Vector2 & vector);
	Vector2 Modulate(const Vector2 & vector);
	GLfloat Dot(const Vector2 & vector);

	//access to elements
	GLfloat operator [] (unsigned int idx);

	//data members
	GLfloat x;
	GLfloat y;
};

//Vector3

class Vector3
{
public:
	//Constructors
	Vector3() : x(0.0f), y(0.0f), z(0.0f) {}
	Vector3(GLfloat _x, GLfloat _y, GLfloat _z) : x(_x), y(_y), z(_z) {}
	Vector3(GLfloat * pArg) : x(pArg[0]), y(pArg[1]), z(pArg[2]) {}
	Vector3(const Vector3 & vector) : x(vector.x), y(vector.y), z(vector.z) {}
	Vector3(int a, int b, int c) : x((float)a), y((float)b), z((float)c) {}
	
	//Vector's operations
	GLfloat Length() const;
	Vector3 & Normalize();
	Vector3 operator + (const Vector3 & vector);
	Vector3 & operator += (const Vector3 & vector);
	Vector3 operator - ();
	Vector3 operator - (const Vector3 & vector);
	Vector3 & operator -= (const Vector3 & vector);
	Vector3 operator * (GLfloat k) const;
	Vector3 & operator *= (GLfloat k);
	Vector3 operator / (GLfloat k);
	Vector3 & operator /= (GLfloat k);
	Vector3 & operator = (const Vector3 & vector);
	Vector3 Modulate(const Vector3 & vector);
	GLfloat Dot(const Vector3 & vector) const;
	Vector3 Cross(const Vector3 & vector) const;

	// implement a more precise Vector3 ==, in fact of coordinates are made of float values.
	bool SameDirectionWith(const Vector3& other);
	bool operator== (const Vector3& other) const {return x == other.x && y == other.y && z == other.z;}
	bool operator!= (const Vector3& other) const {return x != other.x || y != other.y || z != other.z;}
	bool operator < (const Vector3& other) const {return x != other.x ? x < other.x : y != other.y ? y < other.y : z < other.z;}

	//access to elements
	GLfloat operator [] (unsigned int idx);

	// data members
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

inline Vector3 operator - (const Vector3 & vector1, const Vector3 & vector2)
{
	return Vector3(vector1.x - vector2.x, vector1.y - vector2.y, vector1.z - vector2.z);
}

inline Vector3 operator + (const Vector3 & vector1, const Vector3 & vector2)
{
	return Vector3(vector1.x + vector2.x, vector1.y + vector2.y, vector1.z + vector2.z);
}

//Vector4

class Matrix;

class Vector4
{
public:
	//Constructors
	Vector4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
	Vector4(GLfloat _x, GLfloat _y, GLfloat _z) : x(_x), y(_y), z(_z), w(1.0f) {}
	Vector4(GLfloat _x, GLfloat _y, GLfloat _z, GLfloat _w) : x(_x), y(_y), z(_z), w(_w) {}
	Vector4(GLfloat * pArg) : x(pArg[0]), y(pArg[1]), z(pArg[2]), w(pArg[3]) {}
	Vector4(const Vector3 & vector) : x(vector.x), y(vector.y), z(vector.z), w(1.0f){}
	Vector4(const Vector3 & vector, GLfloat _w) : x(vector.x), y(vector.y), z(vector.z), w(_w) {}
	Vector4(const Vector4 & vector) : x(vector.x), y(vector.y), z(vector.z), w(vector.w) {}

	//Vector's operations
	GLfloat Length();
	Vector4 & Normalize();
	Vector4 operator + (const Vector4 & vector);
	Vector4 & operator += (const Vector4 & vector);
	Vector4 operator - ();
	Vector4 operator - (const Vector4 & vector);
	Vector4 & operator -= (const Vector4 & vector);
	Vector4 operator * (GLfloat k);
	Vector4 & operator *= (GLfloat k);
	Vector4 operator / (GLfloat k);
	Vector4 & operator /= (GLfloat k);
	Vector4 & operator = (const Vector4 & vector);
	Vector4 Modulate(const Vector4 & vector);
	GLfloat Dot(const Vector4 & vector);

	//matrix multiplication
	Vector4 operator * ( Matrix & m );

	//access to elements
	GLfloat operator [] (unsigned int idx);

	//data members
	GLfloat x;
	GLfloat y;
	GLfloat z;
	GLfloat w;
};

class Quaternion;
template<typename TReal> class aiMatrix4x4t;
struct Matrix3 {

	float a1, a2, a3;
	float b1, b2, b3;
	float c1, c2, c3;
};

//Matrix 4 X 4


class Matrix
{
public:
	//constructors
	Matrix() { SetIdentity(); }
	Matrix(GLfloat val);
	Matrix ( GLfloat _a1, GLfloat _a2, GLfloat _a3, GLfloat _a4,
			 GLfloat _b1, GLfloat _b2, GLfloat _b3, GLfloat _b4,
			 GLfloat _c1, GLfloat _c2, GLfloat _c3, GLfloat _c4,
			 GLfloat _d1, GLfloat _d2, GLfloat _d3, GLfloat _d4);
	explicit Matrix( const Matrix3& m);
	Matrix(const Vector3& scaling, const Quaternion& rotation, const Vector3& position);
	Matrix(const Matrix & mat);
	// Shit conversion
	Matrix(const aiMatrix4x4t<float>& mat);

	// Matrix operations
	Matrix & SetZero();
	Matrix & SetIdentity();

	Matrix & SetRotationX(GLfloat angle);
	Matrix & SetRotationY(GLfloat angle);
	Matrix & SetRotationZ(GLfloat angle);
	Matrix & SetRotationAngleAxis( float angle, float x, float y, float z );
	Matrix & SetRotationInverse( Vector3 &xaxis, Vector3 &yaxis, Vector3 &zaxis ); //for Vcamera calculation
	Matrix & SetRotation( Vector3 &xaxis, Vector3 &yaxis, Vector3 &zaxis ); //for Wcamera calculation

	Matrix & SetScale(GLfloat scale);
	Matrix & SetScale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ);
	Matrix & SetScale(GLfloat * pScale);
	Matrix & SetScale(const Vector3 &scaleVec);

	Matrix & SetTranslation(GLfloat x, GLfloat y, GLfloat z);
	Matrix & SetTranslation(GLfloat *pTrans);
	Matrix & SetTranslation(const Vector3 &vec);

	Matrix & SetPerspective(GLfloat fovY, GLfloat aspect, GLfloat nearPlane, GLfloat farPlane);

	Matrix Transpose();
	// -------------------------------------------------------------------
	/** @brief Invert the matrix.
	 *  If the matrix is not invertible all elements are set to qnan.
	 *  Beware, use (f != f) to check whether a TReal f is qnan.
	 */
	Matrix& Inverse();
	GLfloat Determinant() const;

	Matrix operator + (Matrix & mat);
	Matrix & operator += (Matrix & mat);
	Matrix operator - (Matrix & mat);
	Matrix &operator -= (Matrix & mat);

	Matrix operator * (const Matrix & mat);
	Matrix operator * (GLfloat k);
	Matrix & operator *= (GLfloat k);

	Vector4 operator * (const Vector4 & vec);

	Matrix & operator = (const Matrix & mat);

	//data members
	union
	{
		GLfloat m[4][4];
		struct
		{
			float a1, a2, a3, a4;
			float b1, b2, b3, b4;
			float c1, c2, c3, c4;
			float d1, d2, d3, d4;
		};
	};
};

class Quaternion
{
public:
	Quaternion() : w(1.0), x(), y(), z() {}
	Quaternion(GLfloat pw, GLfloat px, GLfloat py, GLfloat pz) 
		: w(pw), x(px), y(py), z(pz) {}

	/** Construct from rotation matrix. Result is undefined if the matrix is not orthonormal. */
	Quaternion( const Matrix3& pRotMatrix);

	/** Construct from euler angles */
	Quaternion( GLfloat rotx, GLfloat roty, GLfloat rotz);

	/** Construct from an axis-angle pair */
	Quaternion( Vector3 axis, GLfloat angle);

	/** Construct from a normalized quaternion stored in a vec3 */
	Quaternion( Vector3 normalized);

	/** Returns a matrix representation of the quaternion */
	Matrix3 GetMatrix() const;

public:

	bool operator== (const Quaternion& o) const;
	bool operator!= (const Quaternion& o) const;

	bool Equal(const Quaternion& o, GLfloat epsilon = 1e-6) const;

public:

	/** Normalize the quaternion */
	Quaternion& Normalize();

	/** Compute quaternion conjugate */
	Quaternion& Conjugate ();

	/** Rotate a point by this quaternion */
	Vector3 Rotate (const Vector3& in);

	/** Multiply two quaternions */
	Quaternion operator* (const Quaternion& two) const;

public:

	/** Performs a spherical interpolation between two quaternions and writes the result into the third.
	 * @param pOut Target object to received the interpolated rotation.
	 * @param pStart Start rotation of the interpolation at factor == 0.
	 * @param pEnd End rotation, factor == 1.
	 * @param pFactor Interpolation factor between 0 and 1. Values outside of this range yield undefined results.
	 */
	static void Interpolate( Quaternion& pOut, const Quaternion& pStart, 
		const Quaternion& pEnd, GLfloat pFactor);

public:

	//! w,x,y,z components of the quaternion
	GLfloat w, x, y, z;	
} ;

// convenience marcos
#define		VECTOR3_ZERO	Vector3(0.0f, 0.0f, 0.0f)
#define		VECTOR3_IDEN	Vector3(1.0f, 1.0f, 1.0f)