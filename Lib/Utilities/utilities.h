// if you use STL, please include this header AFTER all other include
#pragma once

#include "esUtil.h"
#include "Math.h"
#include "TGA.h"
#ifdef WIN32
#include "MemoryOperators.h"
#include "esUtil_win.h"
#endif