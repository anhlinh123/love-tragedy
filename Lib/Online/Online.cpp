#include "stdafx.h"
#include "Online.h"
#include "Pointcut\Action.h"
#include "Ads\AdsInterface.h"
#include "InAppPurchase\InAppPurchase.h"
#include "Jsoncpp\json\json.h"


//
Online * Online::ms_pInstance = NULL;

Online * Online::GetInstance() 
{
	if ( ms_pInstance == NULL )
		ms_pInstance = new Online();
	return ms_pInstance;
}

void Online::DestroyInstance() 
{
	if ( ms_pInstance )
	{
		ms_pInstance->CleanUp();
		delete ms_pInstance;
		ms_pInstance = NULL;
	}
}

void Online::Init()
{
	// here we read the config file (json) and manage it into a structure.
	std::ifstream in("online.json", std::ios::in | std::ios::binary);
	std::stringstream sstr;
	sstr << in.rdbuf();

	Json::Value root;
	Json::Reader reader;

	if ( !reader.parse( sstr.str(), root ) )
		return;

	LoadConfig(root);

	// init various online features
#ifdef OS_ANDROID
	AdsInterface::InitAndroid();
	InAppPurchase::InitAndroid();
#endif
}

void Online::Update(float deltaTime )
{
}

void Online::CleanUp()
{
	for (unsigned int i = 0; i < m_pointcutList.size(); i++)
	{
		delete (m_pointcutList.at(i));
	}

	DESTROY_IAP
}

void Online::LoadConfig(Json::Value data)
{
	// loads pointcut actions
	Json::Value actions = data["pointcut"];

	for (unsigned int i = 0; i < actions.size(); i++)
	{
		Json::Value a = actions[i];
		m_pointcutList.push_back(
			new Action(	a["type"].asString(), 
						a["name"].asString(),
						a["location"].asString(),
						a["frequency"].asFloat()));
	}

	// loads iap items
	IAP->LoadConfig(data["in_app_purchase"]);
}

void Online::TriggerPointcut(const char* name, const char* location)
{
	for (ActionVector::iterator it = m_pointcutList.begin(); it != m_pointcutList.end(); it++)
	{
		if ( (*it)->CheckTrigger(name, location) )
		{
			switch ( (*it)->GetType())
			{
			case PA_SHOW_ADS_BANNER:
				AdsInterface::ShowBanner();
				break;
			case PA_SHOW_ADS_INTERSTIAL:
				AdsInterface::ShowFullscreenAd();
				break;
			default:
				break;
			}
		}
	}
}

//{
//	"pointcut"	:
//	[
//		{
//			"type"		: "show_ads_banner",	
//			"name"		: "enter_section",
//			"location"	: "option",
//			"frequency"	: 0.5
//		},
//		{
//			"type"		: "show_ads_banner",	
//			"name"		: "enter_section",
//			"location"	: "new_game",
//			"frequency"	: 1.0
//		},
//	]
//	"in_app_purchase"	:
//	[
//		{
//			"name"		: "Holy light", 	
//			"item_id"	: "game_disable_ads",
//			"price"		: "0,99$"
//		}
//	]
//}