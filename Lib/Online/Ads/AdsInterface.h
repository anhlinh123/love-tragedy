#ifndef _ADS_INTERFACE_H_
#define _ADS_INTERFACE_H_

class AdsInterface
{
public:
	static void ShowBanner();
	static void HideBanner();
	static void ShowFullscreenAd();
	static void CancelFullScreenAd();

#ifdef OS_ANDROID
	static void InitAndroid();
#endif
};

#endif