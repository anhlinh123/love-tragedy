#include "stdafx.h"
#include "AdsInterface.h"

#ifdef OS_ANDROID

#include <jni.h>
#include <stdlib.h>
#include <android/log.h>
#include <acp_utils.h>

// JNI Bindings
#define JNI_ADS_FUNC_VOID(function) extern "C" JNIEXPORT void JNICALL Java_com_ads_ ## function
#define ADS_DEBUG(...) __android_log_print(ANDROID_LOG_INFO, "Ads_cpp", __VA_ARGS__)
#define ADS_ERR(...) __android_log_print(ANDROID_LOG_ERROR, "Ads_cpp", __VA_ARGS__)


static jmethodID show_banner = NULL;
static jmethodID hide_banner = NULL;
static jmethodID show_fullscreen_ad = NULL;
static jmethodID cancel_fullscreen_ad = NULL;

jclass ClassAds = NULL;

void AdsInterface::InitAndroid()
{
	JNIEnv* env = NULL;
	acp_utils::ScopeGetEnv	s_env(env);

	ADS_DEBUG("Caching JNI method IDs...");

	jclass cls = env->FindClass("com/gameloft/glads/GLAds"); 
	ClassAds = (jclass)env->NewGlobalRef(cls);

	show_banner = env->GetStaticMethodID(ClassAds, "showBanner", "()V");
	hide_banner = env->GetStaticMethodID(ClassAds, "hideBanner", "()V");
	show_fullscreen_ad = env->GetStaticMethodID(ClassAds, "showFullScreenAd", "()V");
	cancel_fullscreen_ad = env->GetStaticMethodID(ClassGLAds, "cancelFullScreenAd", "()V");
}

#endif

// common interface

void AdsInterface::ShowBanner()
{
#ifdef OS_ANDROID
	if (show_banner)
	{
		JNIEnv*	pEnv = NULL;
		acp_utils::ScopeGetEnv	scope_env(pEnv);

		pEnv->CallStaticVoidMethod(ClassAds, show_banner);
	}
#endif
}

void AdsInterface::HideBanner()
{
#ifdef OS_ANDROID
	if (hide_banner)
	{
		JNIEnv*	pEnv = NULL;
		acp_utils::ScopeGetEnv	scope_env(pEnv);

		pEnv->CallStaticVoidMethod(ClassAds, hide_banner);
	}
#endif
}

void AdsInterface::ShowFullscreenAd()
{
#ifdef OS_ANDROID
	if (showFullScreenAd)
	{
		JNIEnv*	pEnv = NULL;
		acp_utils::ScopeGetEnv	scope_env(pEnv);

		pEnv->CallStaticVoidMethod(ClassGLAds, showFullScreenAd);
	}
#endif
}

void AdsInterface::CancelFullScreenAd()
{
#ifdef OS_ANDROID
	if (cancelFullScreenAd)
	{
		JNIEnv*	pEnv = NULL;
		acp_utils::ScopeGetEnv	scope_env(pEnv);

		pEnv->CallStaticVoidMethod(ClassGLAds, cancelFullScreenAd);
	}
#endif
}