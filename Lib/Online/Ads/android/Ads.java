package com.mot_cai_ten_deo_gi_day;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

 // googleplay ads
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class Ads
{
	private static ViewGroup mParentView = null;
	private static AdView mAdView = new AdView();
	private static LayoutParams mAdParams = null;
	
	// common methods
	public static void setParentView(ViewGroup parentView)
	{
		mParentView = parentView;
	}
	
	public static void pause()
	{
		if (mAdView != null) {
			mAdView.pause();
		}
	}
	
	public static void resume()
	{
		if (mAdView != null) {
			mAdView.resume();
		}
	}

	public static void destroy()
	{
		if (mAdView != null) {
			mAdView.destroy();
		}
	}
	
	/*private static void setBannerLayoutParams()
	{
		Display display = ((WindowManager) Utils.getContext().getSystemService(Activity.WINDOW_SERVICE)).getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		
		float density = metrics.density;
		
		int banner_width = 0;
		int banner_height = 0;
		
		if (GLAds.isBigScreen())
		{
			banner_width = (int) (0.9f * Math.min(display.getWidth(), display.getHeight()));
			banner_height = (banner_width * 90) / 728;
		}
		else
		{
			banner_width = dip(BANNER_WIDTH_SMALL, density);
			banner_height = dip(BANNER_HEIGHT_SMALL, density);
		}
		
		int startX = 0, startY = 0;
		int[] rules = { RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.ALIGN_PARENT_BOTTOM };
		
		switch(_anchor)
		{
		case GLAds.AD_BOTTOM_CENTER:
			startX = (display.getWidth() - banner_width) / 2 + _positionX;
			startY = display.getHeight() - banner_height + _positionY;
			
			rules[0] = RelativeLayout.CENTER_HORIZONTAL;
			rules[1] = RelativeLayout.ALIGN_PARENT_BOTTOM;
			break;
		case GLAds.AD_BOTTOM_LEFT:
			startX = _positionX;
			startY = display.getHeight() - banner_height + _positionY;
			
			rules[0] = RelativeLayout.ALIGN_PARENT_LEFT;
			rules[1] = RelativeLayout.ALIGN_PARENT_BOTTOM;
			break;			
		case GLAds.AD_BOTTOM_RIGHT:
			startX = display.getWidth() - banner_width + _positionX;
			startY = display.getHeight() - banner_height + _positionY;
			
			rules[0] = RelativeLayout.ALIGN_PARENT_RIGHT;
			rules[1] = RelativeLayout.ALIGN_PARENT_BOTTOM;
			break;
		case GLAds.AD_TOP_CENTER:
			startX = (display.getWidth() - banner_width) / 2 + _positionX;
			startY = _positionY;
			
			rules[0] = RelativeLayout.ALIGN_PARENT_TOP;
			rules[1] = RelativeLayout.CENTER_HORIZONTAL;
			break;
		case GLAds.AD_TOP_LEFT:
			startX = _positionX;
			startY = _positionY;
			
			rules[0] = RelativeLayout.ALIGN_PARENT_TOP;
			rules[1] = RelativeLayout.ALIGN_PARENT_LEFT;
			break;
		case GLAds.AD_TOP_RIGHT:
			startX = display.getWidth() - banner_width + _positionX;
			startY = _positionY;
			
			rules[0] = RelativeLayout.ALIGN_PARENT_TOP;
			rules[1] = RelativeLayout.ALIGN_PARENT_RIGHT;
			break;
		}
		
		if (GLAds.getParentView() instanceof RelativeLayout)
		{
			mWebParams = new RelativeLayout.LayoutParams(banner_width, banner_height);
			((RelativeLayout.LayoutParams)mWebParams).addRule(rules[0]);
			((RelativeLayout.LayoutParams)mWebParams).addRule(rules[1]);
			
			((RelativeLayout.LayoutParams)mWebParams).setMargins(
					(_positionX >= 0) ? _positionX : 0, 
					(_positionY >= 0) ? _positionY : 0, 
					(_positionX < 0) ? -_positionX : 0, 
					(_positionY < 0) ? -_positionY : 0);
		}
		else if (GLAds.getParentView() instanceof FrameLayout)
		{
			mWebParams = new FrameLayout.LayoutParams(banner_width, banner_height);
			((FrameLayout.LayoutParams)mWebParams).setMargins(
					startX, 
					startY, 
					0, 
					0);
		}
		else if (GLAds.getParentView() instanceof AbsoluteLayout)
		{
			mWebParams = new AbsoluteLayout.LayoutParams(
					banner_width, 
					banner_height, 
					startX, 
					startY);
		}
	}*/
	
	// ads methods
	public static void showBanner()
	{
		// align it to some where on the screen (ie: middle bottom)
		mParentView.addView(mAdView, mAdParams);

		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device. e.g.
		// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.build();

		// Start loading the ad in the background.
		mAdView.loadAd(adRequest);
	}
	
	public static void hideBanner()
	{
		mParentView.removeView(mAdView);
	}
	
	public static void showFullScreenAd()
	{
	}
	
	public static void cancelFullScreenAd()
	{
	}
}