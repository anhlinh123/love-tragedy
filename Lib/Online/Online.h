#ifndef _ONLINE_H_
#define _ONLINE_H_

#include "Pointcut\Action.h"
#include "Ads\AdsInterface.h"
#include "InAppPurchase\InAppPurchase.h"
#include "Jsoncpp\json\json.h"


class Online
{
private:
	Online () {}
	~Online () {}
	Online (const Online &);
	Online& operator = (const Online &);

public:
	static Online * GetInstance ();
	static void DestroyInstance ();

	void Init();
	void Update(float deltaTime );
	void CleanUp();
	void LoadConfig(Json::Value data);

	//
	void TriggerPointcut(const char* name, const char* location);
	
protected:
	static Online * ms_pInstance;

	ActionVector m_pointcutList;
};

#define ONLINE Online::GetInstance()
#define DESTROY_ONLINE Online::DestroyInstance();

#endif