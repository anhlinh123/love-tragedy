#ifndef _ACTION_H_
#define _ACTION_H_

#include <string>
#include <vector>

// example of an pointcut Action:
// 
//	{
//		"type"		: "show_ads_banner",	
//		"name"		: "enter_section",
//		"location"	: "option",
//		"frequency"	: 0.5,
//	}
//

enum ActionType
{
	PA_SHOW_ADS_BANNER,
	PA_SHOW_ADS_INTERSTIAL,
	PA_UNKNOWN
};

class Action
{
public:
	Action(std::string type, std::string name, std::string location, float frequency);
	~Action();

	bool CheckTrigger(const char* name, const char* location);
	ActionType GetType() { return m_type; }

protected:
	ActionType m_type;
	std::string m_name;
	std::string m_location;
	float m_frequency;
};

typedef std::vector<Action *>	ActionVector;

#endif