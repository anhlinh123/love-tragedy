#include "stdafx.h"
#include "Action.h"

Action::Action(std::string type, std::string name, std::string location, float frequency)
	: m_name(name)
	, m_location(location)
	, m_frequency(frequency)
{
	if (type == "show_ad_banner")
		m_type = PA_SHOW_ADS_BANNER;
	else if (type == "show_ad_interstial")
		m_type = PA_SHOW_ADS_INTERSTIAL;
	else
		m_type = PA_UNKNOWN;
}

Action::~Action()
{
}

bool Action::CheckTrigger(const char* name, const char* location)
{
	bool bTrigger = false;
	if (m_name == name && m_location == location)
	{
		//check limit here

		//roll to trigger
		float random = (float)(rand() % 100);
		if (random / 100.0f < m_frequency)
			bTrigger = true;
	}
	return bTrigger;
}