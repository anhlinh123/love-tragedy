#ifndef _IAP_ITEM_H_
#define _IAP_ITEM_H_

#include <vector>
#include <string>

// example of an IAPItem:
// 
//	{
//		"name"		: "Holy light", 	
//		"item_id"	: "game_disable_ads",
//		"price"		: "0,99$"
//	}
//

class IAPItem
{
public:
	IAPItem(std::string name, std::string itemId, std::string price);
	~IAPItem();

	std::string GetItemId() { return m_itemId; }

protected:
	std::string m_name;
	std::string m_itemId;
	std::string m_price;	// just display price, the real price is binded to itemId configed on market
};

typedef std::vector<IAPItem *>	IAPItemVector;

#endif