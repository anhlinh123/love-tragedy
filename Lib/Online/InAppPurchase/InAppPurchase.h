#ifndef _IAP_H_
#define _IAP_H_

#include "IAPItem.h"
#include "Jsoncpp\json\json.h"


class InAppPurchase
{
private:
	InAppPurchase () {}
	~InAppPurchase () {}
	InAppPurchase (const InAppPurchase &);
	InAppPurchase& operator = (const InAppPurchase &);

public:
	static InAppPurchase * GetInstance ();
	static void DestroyInstance ();

public:
	void LoadConfig(Json::Value data);
	void CleanUp();
	void Buy(const char* itemId);
	IAPItemVector& GetIAPItems() { return m_IAPItemList; }

#ifdef OS_ANDROID
	static void InitAndroid();
#endif

protected:
	static InAppPurchase * ms_pInstance;
	IAPItemVector m_IAPItemList;
};

#define IAP InAppPurchase::GetInstance()
#define DESTROY_IAP InAppPurchase::DestroyInstance();

#endif