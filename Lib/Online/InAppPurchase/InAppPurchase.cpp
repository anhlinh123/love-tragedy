#include "stdafx.h"
#include "InAppPurchase.h"

// Jni part
#ifdef OS_ANDROID

#include <jni.h>
#include <stdlib.h>
#include <android/log.h>
#include <acp_utils.h>

// JNI Bindings
#define JNI_IAP_FUNC_VOID(function) extern "C" JNIEXPORT void JNICALL Java_com_InAppPurchase_ ## function
#define IAP_DEBUG(...) __android_log_print(ANDROID_LOG_INFO, "Iap_cpp", __VA_ARGS__)
#define IAP_ERR(...) __android_log_print(ANDROID_LOG_ERROR, "Iap_cpp", __VA_ARGS__)


static jmethodID buy_item = NULL;

jclass ClassIAP = NULL;

void InAppPurchase::InitAndroid()
{
	JNIEnv* env = NULL;
	acp_utils::ScopeGetEnv	s_env(env);

	IAP_DEBUG("Caching JNI method IDs...");

	jclass cls = env->FindClass("com/gameloft/glads/GLAds"); 
	ClassIAP = (jclass)env->NewGlobalRef(cls);

	buy_item = env->GetStaticMethodID(ClassIAP, "buyItem", "()V");
}

#endif

// native part
InAppPurchase * InAppPurchase::ms_pInstance = NULL;

InAppPurchase * InAppPurchase::GetInstance() 
{
	if ( ms_pInstance == NULL )
		ms_pInstance = new InAppPurchase();
	return ms_pInstance;
}

void InAppPurchase::DestroyInstance() 
{
	if ( ms_pInstance )
	{
		ms_pInstance->CleanUp();
		delete ms_pInstance;
		ms_pInstance = NULL;
	}
}

void InAppPurchase::CleanUp()
{
	for (unsigned int i = 0; i < m_IAPItemList.size(); i++)
	{
		delete (m_IAPItemList.at(i));
	}
}

void InAppPurchase::LoadConfig(Json::Value data)
{
	// loads IAP items
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value item = data[i];
		m_IAPItemList.push_back(
			new IAPItem(item["name"].asString(), 
						item["item_id"].asString(),
						item["price"].asString()));
	}
}

void InAppPurchase::Buy(const char* itemId)
{
	for (IAPItemVector::iterator it = m_IAPItemList.begin(); it != m_IAPItemList.end(); it++)
	{
		if ( (*it)->GetItemId() == itemId)
		{
			//item matched! buy it
#ifdef OS_ANDROID
			if (buy_item)
			{
				JNIEnv*	pEnv = NULL;
				acp_utils::ScopeGetEnv	scope_env(pEnv);

				pEnv->CallStaticVoidMethod(ClassIAP, buy_item);
			}
#endif
			break;
		}
	}
}