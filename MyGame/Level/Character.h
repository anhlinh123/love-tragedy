#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "jsoncpp/json/json.h"
#include "Utilities/Math.h"
#include "StateMachine.h"

#define CHAR_OFFSET 1.0f

// enums
enum CHARACTER_ANIM_STATE
{
	CHARACTER_ANIM_STATE_IDLE,
	CHARACTER_ANIM_STATE_RUNNING,
	CHARACTER_ANIM_STATE_JUMPING
};

enum CHARACTER_ANIM_EVENT
{
	CHARACTER_ANIM_EVENT_RUN,
	CHARACTER_ANIM_EVENT_STOP,
	CHARACTER_ANIM_EVENT_JUMP
};


class Character
{
public:
	Character() {}
	~Character() {}

	void Load(Json::Value& data);

	// anim functions
	void OnEnterRun();
	void OnEnterIdle();
	void OnEnterJump();
	void InjectAnimEvent(CHARACTER_ANIM_EVENT ev);

public:
	Object* m_obj;
	Vector3 m_pos;
	Vector3 m_up;	// the direction that the boy is standing, casually the y axis
	Vector3 m_face;	// the boy face direction
	Vector3 m_currentCamPos;
	Vector3 m_currentCamUp;

	StateMachine<Character> m_animationStates;
};

#endif