#include "stdafx.h"
#include <math.h>
#include "Level.h"
#include "CoreEngine\Controller\Scene.h"
#include "CoreEngine\Object\Model.h"
#include "CoreEngine\Object\PathDefine.h"
#include "Utilities\utilities.h"
#include "Jsoncpp\json\json.h"

#include "CEGUI\WindowManager.h"
#include "CEGUI\GUIContext.h"
#include "CEGUI\ImageManager.h"
#include "CEGUI\Font.h"
#include "CEGUI\Scheme.h"
#include "CEGUI\falagard\WidgetLookManager.h"
#include "CEGUI\SchemeManager.h"
#include "CEGUI\widgets\DefaultWindow.h"
#include "CEGUI\widgets\ItemListBox.h"
#include "CEGUI\widgets\MenuItem.h"

#include "CoreEngine\Object\Renderer.h"
#include "GameStates/StateLoading.h"
#include "GameStates/StatePlay.h"

#ifdef ANDROID
extern "C" void nativeHideAd();
extern "C" void nativeSave(int index);
extern "C" int nativeLoad();
#endif

// global variables
const char* LevelList[MAXLEVEL] =
{
	"Level4.json",
	"Level6.json",
	"Level5.json",
	"Level7.json"
};

int ms_currentLevel = -1;


void Level_ReplayLevel()
{
	// ATTENTION: current state must be StatePlay, so we pop pop pop until meet StatePlay
	while ( strcmp(SCENE->GetCurrentState()->GetStateName(), "StatePlay") != 0 )
		SCENE->PopGameState("StateAny");

	StatePlay* current = static_cast<StatePlay*>(SCENE->GetCurrentState());
	current->m_level.UnloadLevel();
	current->m_level.LoadLevel(LevelList[ms_currentLevel]);
}

void Level_PlayNextLevel()
{
#ifdef ANDROID
	nativeHideAd();
#endif
	ms_currentLevel++;
#ifdef ANDROID
	nativeSave(ms_currentLevel);
#endif
	if (ms_currentLevel <= MAXLEVEL)
	{
		SCENE->SetGameState(new StatePlay(LevelList[ms_currentLevel]));
	}
}


// utils
std::string key_string(int i, int j, int k)
{
	char key[20];
	sprintf(key, "[%d][%d][%d]", i, j, k);
	return std::string(key);
}

std::string key_string(Json::Value& pos) { return key_string(pos[0].asInt(), pos[1].asInt(), pos[2].asInt()); }
std::string key_string(Vec3i& pos) { return key_string(pos.x, pos.y, pos.z); }

// vec3i
Vec3i::Vec3i(const Vector3& vec3f)
{
	x = (int)floor(vec3f.x + 0.5f);
	y = (int)floor(vec3f.y + 0.5f);
	z = (int)floor(vec3f.z + 0.5f);
}

Vec3i::Vec3i(float a, float b, float c)
{
	x = (int)floor(a + 0.5f);
	y = (int)floor(b + 0.5f);
	z = (int)floor(c + 0.5f);
}

// temporary place here
Vector3 GetRotationVector(const Vector3& from, const Vector3& to, const Vector3& up)
{
	// round and normalize everything before calculate
	Vector3 f = from; f.Normalize();
	Vector3 t = to; t.Normalize();
	Vector3 u = up; u.Normalize();
	Vec3i fi(f); 
	Vec3i ti(t); 
	Vec3i ui(u);
	f = Vector3(fi.x, fi.y, fi.z);
	t = Vector3(ti.x, ti.y, ti.z);
	u = Vector3(ui.x, ui.y, ui.z);

	// in fact I just test it with kinds of vector (0, 0, 1) or (1, 0, 0) or so so...
	float sign = - t.Cross(f).Dot(u);
	sign = ((int)sign == 0)? 1.0f:sign;
	float angle = sign * acos(t.Dot(f));
	return (u * angle);
}

// hard-coded function, from should always be (0, 0, -1) for Arrow-face or (1, 0, 0) for Arrow-up
Vector3 GetRotationVector2(const Vector3& from, const Vector3& to, const Vector3& up)
{
	// round and normalize everything before calculate
	Vector3 f = from; f.Normalize();
	Vector3 t = to; t.Normalize();
	Vector3 u = up; u.Normalize();
	Vec3i fi(f); 
	Vec3i ti(t); 
	Vec3i ui(u);
	f = Vector3(fi.x, fi.y, fi.z);
	t = Vector3(ti.x, ti.y, ti.z);
	u = Vector3(ui.x, ui.y, ui.z);

	if ( f.SameDirectionWith(t * (-1.0f)) || f.SameDirectionWith(t) ) // f reverse/same direct to t => u is unknown
		u = GetHardcodedUp(f, t);

	// in fact I just test it with kinds of vector (0, 0, 1) or (1, 0, 0) or so so...
	float sign = - t.Cross(f).Dot(u);
	sign = ((int)sign == 0)? 1.0f:sign;
	float angle = sign * acos(t.Dot(f));
	return (u * angle);
}

Vector3 RotatePoint(const Vector3& point, const Vector3& rot)
{
	Matrix m;
	Vector4 p = Vector4(point.x, point.y, point.z, 1.0f);

	p = p * m.SetRotationZ(rot.z);
	p = p * m.SetRotationX(rot.x);
	p = p * m.SetRotationY(rot.y);
	return Vector3(p.x, p.y, p.z);
}

Vector3 GetHardcodedUp(const Vector3& from, const Vector3& to)
{
	Vector3 f = from; f.Normalize();
	Vector3 t = to; t.Normalize();
	Vec3i fi(f); 
	Vec3i ti(t);

	if (fi == Vec3i(1, 0, 0))
	{
		if (ti == Vec3i(1, 0, 0))
			return Vector3(0.0f, 0.0f, 1.0f);
		else if (ti == Vec3i(-1, 0, 0))
			return Vector3(0.0f, 0.0f, 1.0f);
	}
	else if (fi == Vec3i(0, 0, -1))
	{
		if (ti == Vec3i(0, 0, 1))
			return Vector3(0.0f, 1.0f, 0.0f);
		else if (ti == Vec3i(0, 0, -1))
			return Vector3(0.0f, 1.0f, 0.0f);
	}
	else if (fi == Vec3i(0, 1, 0))
	{
		if (ti == Vec3i(0, 1, 0))
			return Vector3(1.0f, 0.0f, 0.0f);
		else if (ti == Vec3i(0, -1, 0))
			return Vector3(1.0f, 0.0f, 0.0f);
	}
	else if (fi == Vec3i(0, 0, 1))
	{
		if (ti == Vec3i(0, 0, 1))
			return Vector3(0.0f, 1.0f, 0.0f);
		else if (ti == Vec3i(0, 0, -1))
			return Vector3(0.0f, 1.0f, 0.0f);
	}
	
	return VECTOR3_ZERO;
}

////////////////////////////////////////////////////////////////////////////
// transformation callbacks
struct callback
{
	//
	static void DoMoveCharacter(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		Vector3 deltaMove = VECTOR3_VAL(cmd->m_arguments["move"]) * cmd->m_deltaTime;
		cmd->m_obj->Move(deltaMove);
		SCENE->GetCamera()->MovePosAndTarget(deltaMove);
	}

	static void MoveCharacterCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
		level->CloseGuide();
		level->SyncCharacterPosition(level->GetActiveCharacter()->m_pos + VECTOR3_VAL(cmd->m_arguments["move"]) );
	}
	//
	static void DoRotateCharacter(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
		cmd->m_obj->Rotate(deltaRot, VECTOR3_VAL(cmd->m_arguments["offset"]));

		if (cmd->m_type == Command::ROTATE_CHAR)
		{
			Vector3 upup = (level->GetActiveCharacter()->m_up * (1.0f - cmd->m_progress)) 
				+ VECTOR3_VAL(cmd->m_arguments["up"]) * cmd->m_progress;
			Vector3 lightDirection = upup * (-1.0f);
			SetLight(lightDirection.x, lightDirection.y, lightDirection.z);
		}
	}

	static void RotateCharacterCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
		level->SyncCharacterRotation( VECTOR3_VAL(cmd->m_arguments["rotate"]), 
								VECTOR3_VAL(cmd->m_arguments["newPos"]), 
								VECTOR3_VAL(cmd->m_arguments["up"]) );
	}
	//
	static void DoJumpCharacter(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		float radius = FLOAT_VAL(cmd->m_arguments["R"]);
		float angle = 3.1416f * cmd->m_progress;
		float oldAngle = 3.1416f * cmd->m_oldProgress;

		Vector3 deltaMoveUp = VECTOR3_VAL(cmd->m_arguments["up"]) * radius * (sin(angle) - sin(oldAngle));
		Vector3 deltaMoveFace = VECTOR3_VAL(cmd->m_arguments["face"]) * radius * (cos(oldAngle) - cos(angle));

		cmd->m_obj->Move(deltaMoveUp + deltaMoveFace);
		SCENE->GetCamera()->MovePosAndTarget(deltaMoveFace);
	}

	// jump to higher position
	// ex: jump from (0, 0, 0) to (0, 1, 0)
	// move by the parabol function: y = -4x^2 + x + 1.5
	// note: this function is only for jumping within two adjacent cubes
	static void DoJumpCharacterUp(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		float v0 = 1.0f;

		float deltaUp = -4.0f * v0 * cmd->m_deltaTime * (v0 * (cmd->m_progress + cmd->m_oldProgress) - 1.0f) + v0 * cmd->m_deltaTime;
		float deltaFace = v0 * cmd->m_deltaTime;

		Vector3 deltaMoveUp = VECTOR3_VAL(cmd->m_arguments["up"]) * deltaUp;
		Vector3 deltaMoveFace = VECTOR3_VAL(cmd->m_arguments["face"]) * deltaFace;

		cmd->m_obj->Move(deltaMoveUp + deltaMoveFace);
		SCENE->GetCamera()->MovePosAndTarget(deltaMoveUp + deltaMoveFace);
	}

	// jump to lower position
	// ex: jump from (0, 1, 0) to (0, 0, 0)
	// move by the parabol function: y = -4x^2 + x + 1.5
	// note: this function is only for jumping within two adjacent cubes
	static void DoJumpCharacterDown(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		float v0 = 1.0f;

		float deltaUp = -4.0f * v0 * cmd->m_deltaTime * (v0 * (cmd->m_progress + cmd->m_oldProgress) - 1.0f) - v0 * cmd->m_deltaTime;
		float deltaFace = v0 * cmd->m_deltaTime;

		Vector3 deltaMoveUp = VECTOR3_VAL(cmd->m_arguments["up"]) * deltaUp;
		Vector3 deltaMoveFace = VECTOR3_VAL(cmd->m_arguments["face"]) * deltaFace;

		cmd->m_obj->Move(deltaMoveUp + deltaMoveFace);
		SCENE->GetCamera()->MovePosAndTarget(deltaMoveUp + deltaMoveFace);
	}

	static void JumpCharacterCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
		level->GetActiveCharacter()->InjectAnimEvent(CHARACTER_ANIM_EVENT_STOP);
		level->SyncCharacterPosition(VECTOR3_VAL(cmd->m_arguments["dest"]));
	}
	//
	static void DoRotateCamera(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
		// Camera lock to behind character
		SCENE->GetCamera()->RotateAroundTarget(deltaRot);
	}

	static void DoRotateCameraWithPoint(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
		// Camera lock to behind character
		SCENE->GetCamera()->RotatePosAndTarget( deltaRot, VECTOR3_VAL(cmd->m_arguments["point"]) );
	}

	static void RotateCameraCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
	}

	static void DoMoveCube(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		Vector3 deltaMove = VECTOR3_VAL(cmd->m_arguments["move"]) * cmd->m_deltaTime;
		cmd->m_obj->Move(deltaMove);
	}

	static void MoveCubeCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
		level->SyncCubeMove(VEC3i_VAL(cmd->m_arguments["switch"]),
							VEC3i_VAL(cmd->m_arguments["from"]), 
							VEC3i_VAL(cmd->m_arguments["to"]) );
	}
	//
	static void DoZoomCamera(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = true;
		float deltaZoom = FLOAT_VAL(cmd->m_arguments["zoom"]) * cmd->m_deltaTime; // "zoom" = -1.0f means zoom in
		SCENE->GetCamera()->ChangeZoomLevel(deltaZoom);
	}

	static void ZoomCameraCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
		level->OnZoomCompleted(FLOAT_VAL(cmd->m_arguments["zoom"]) < 0.0f);		
	}

	static void DoOpenView(Command* cmd)
	{
		float target = FLOAT_VAL(cmd->m_arguments["target"]);
		float base = FLOAT_VAL(cmd->m_arguments["base"]);
		float deltaZoom = (target - base) * cmd->m_progress;
		//cmd->m_speed;
		SCENE->GetCamera()->OpenView(base + deltaZoom);
	}
	
	static void OpenViewCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->m_bIsRotating = false;
		float target = FLOAT_VAL(cmd->m_arguments["target"]);
		SCENE->GetCamera()->OpenView(target);
	}

	// ending animation
	static void DoEndingAnimation(Command* cmd)
	{
		Vector3 deltaRot = VECTOR3_VAL(cmd->m_arguments["rotate"]) * cmd->m_deltaTime;
		SCENE->GetCamera()->RotateAroundTarget(deltaRot);
	}

	static void EndingAnimationCompleted(Command* cmd)
	{
		Level* level = cmd->m_levelRef;
		level->OnLevelEndingAnimationCompleted();
	}
};


Level::Level() 
	: m_bIsLevelEnding(false),
	m_bIsViewOpen(false),
	m_bIsRotating(false)
{
#ifdef ANDROID
	ms_currentLevel = nativeLoad();
#endif
}

Level::~Level()
{
	for (LevelMap::iterator it = m_levelMap.begin(); it != m_levelMap.end(); it++)
	{
		if (it->second)
			delete it->second;
	}

	delete[] m_listMsg;

	m_levelMap.clear();
}

Cube* Level::GetCubeAt(std::string key)
{
	LevelMap::const_iterator iterator = m_levelMap.find(key);
	if (iterator != m_levelMap.end())
		return iterator->second;
	else
		return NULL;
}

void Level::LoadLevel(const char* level)
{
	char fullPath[250];

	GetFullPath(DATA_PATH, level, fullPath);
	std::ifstream in(fullPath, std::ios::in | std::ios::binary);
	std::stringstream sstr;
	sstr << in.rdbuf();

	Json::Value root;
	Json::Reader reader;

	bool parsingSuccessful = reader.parse( sstr.str(), root );
	if ( !parsingSuccessful )
		return;

	// reset the level
	m_levelMap.clear();

	// Load background
	Json::Value data = root["background"];
	{
		Vector3 scale = Vector3(data["scale"][0].asFloat(), data["scale"][1].asFloat(), data["scale"][2].asFloat());
		Object* o = new Object(CreateModel(data["name"].asString().c_str()));
		o->SetGeometry(VECTOR3_ZERO, VECTOR3_ZERO, scale);
		SCENE->AddObject(o);
	}

	// load the level
	data = root["normal"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		Cube* newNormal = Cube_AddCube(GetCubeAt(key), Cube::NORMAL, 
										Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
										VECTOR3_ZERO, VECTOR3_ZERO, VECTOR3_ZERO);
		m_levelMap[key] = newNormal;
	}

	// load the rotable cubes
	data = root["rotable"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		Cube* newRotable = 	Cube_AddCube(
			GetCubeAt(key),
			Cube::ROTABLE, 
			Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
			VECTOR3_ZERO,
			Vector3(datai["from"][0].asFloat(), datai["from"][1].asFloat(), datai["from"][2].asFloat()), 
			Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()));

		m_levelMap[key] = newRotable;
	}

	// load the jumpable cubes
	data = root["jumpable"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		// add jump entry cube
		Cube* newJumpable = Cube_AddCube(
			GetCubeAt(key),
			Cube::JUMP_ENTRY,
			Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
			Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()), 
			VECTOR3_ZERO,
			Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()));

		m_levelMap[key] = newJumpable;

		// add jump destination cube
		std::string key2 = key_string(datai["to"]);

		Cube* newNormal = Cube_AddCube(GetCubeAt(key2), Cube::NORMAL, 
										Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()),
										VECTOR3_ZERO, VECTOR3_ZERO, VECTOR3_ZERO);
		m_levelMap[key2] = newNormal;
	}

	// add elevator switch cubes
	data = root["elevator_switch"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		// add elevator switch cube
		Cube* newElevator = Cube_AddCube(
			GetCubeAt(key),
			Cube::ELEVATOR_SWITCH, 
			Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
			Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()), 
			Vector3(datai["from"][0].asFloat(), datai["from"][1].asFloat(), datai["from"][2].asFloat()), 
			Vector3(datai["to"][0].asFloat(), datai["to"][1].asFloat(), datai["to"][2].asFloat()));

		m_levelMap[key] = newElevator;
	}

	// add open view cubes
	data = root["open_view"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		Cube* newNormal = Cube_AddCube(GetCubeAt(key), Cube::OPEN_VIEW, 
										Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
										Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()),
										VECTOR3_ZERO, VECTOR3_ZERO);
		m_levelMap[key] = newNormal;
	}

	// add guide cubes
	data = root["guide"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		Cube* newNormal = Cube_AddCube(GetCubeAt(key), Cube::GUIDE, 
										Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
										Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()),
										Vector3(datai["id"][0].asFloat(), datai["id"][1].asFloat(), datai["id"][2].asFloat()), 
										VECTOR3_ZERO);
		m_levelMap[key] = newNormal;
	}

	//add text data
	data = root["texts"];
	m_listMsg = new std::string[data.size()];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string msg = datai["msg"].asString();
		m_listMsg[i] = msg;
	}

	// Load consumables
	data = root["consumable"];
	for (unsigned int i = 0; i < data.size(); i++)
	{
		Json::Value datai = data[i];
		std::string key = key_string(datai["pos"]);

		// add elevator switch cube
		Cube* theCube = GetCubeAt(key);
		if (theCube)
		{
			theCube->AddConsumable(	Vector3(datai["pos"][0].asFloat(), datai["pos"][1].asFloat(), datai["pos"][2].asFloat()),
									Vector3(datai["up"][0].asFloat(), datai["up"][1].asFloat(), datai["up"][2].asFloat()),
									Vector3(datai["rotate"][0].asFloat(), datai["rotate"][1].asFloat(), datai["rotate"][2].asFloat()),
									datai["name"].asString().c_str() );
		}
	}

	// Load main chars
	data = root["boy"];
	m_boy.Load(data);
	data = root["girl"];
	m_girl.Load(data);

	Vec3i posb(m_boy.m_pos);
	OnEnterCube(m_boy, GetCubeAt(key_string(posb)));
	Vec3i posg(m_girl.m_pos);
	OnEnterCube(m_girl, GetCubeAt(key_string(posg)));

	m_activeCharacter = &m_boy;
	Vector3 lightDirection = m_activeCharacter->m_up * (-1.0f);
	SetLight(lightDirection.x, lightDirection.y, lightDirection.z);

	// Set up camera
	SCENE->GetCamera()->SetLookAtCharacter(	m_activeCharacter->m_pos + m_activeCharacter->m_up, 
											m_activeCharacter->m_currentCamPos,
											m_activeCharacter->m_currentCamUp);

	m_bIsLevelEnding = false;
}

void Level::ProcessCommands(float deltaTime)
{
	bool bHasCommandRunning = false;
	for (CommandList::iterator it = m_commands.begin(); it != m_commands.end(); )
	{
		Command* cmd = (*it);
		if (!cmd->m_bIsRunning)
		{
			m_commands.erase(it++);
			delete cmd;
		}
		else
		{
			bHasCommandRunning = true;
			cmd->Execute(deltaTime);
			++it;
		}
	}

	// if no commands for 5 updates then the boy is IDLE
	static int idle_times = 0;
	if (!bHasCommandRunning && !m_bIsLevelEnding)
	{
		if (idle_times == 5)
		{
			idle_times++;
			m_activeCharacter->InjectAnimEvent(CHARACTER_ANIM_EVENT_STOP);
			
			// when character becomes idle, check for endgame condition
			if ( (m_boy.m_pos - m_girl.m_pos).Length() < 1.1f &&	// they are on adjacent cubes
				  m_boy.m_up.SameDirectionWith(m_girl.m_up) )		// ... and same cubes side
				  OnLevelCompleted();
		}
		else if (idle_times < 5)
			idle_times++;
	}
	else
	{
		idle_times = 0;
	}
}

bool Level::IsMovable(Vector3& move)
{
	Vec3i np(m_activeCharacter->m_pos + m_activeCharacter->m_up + move);
	Vec3i ng(m_activeCharacter->m_pos + move);

	Cube* checkForBlock  = GetCubeAt(key_string(np));
	Cube* checkForGround = GetCubeAt(key_string(ng));

	return (!checkForBlock && checkForGround);
}

int Level::IsRotable(Vector3& move)
{
	Vec3i pos(m_activeCharacter->m_pos);
	Vec3i newPos(m_activeCharacter->m_pos + m_activeCharacter->m_up + move);

	Cube* checkForBlock  = GetCubeAt(key_string(newPos));
	Cube* checkForRotable = GetCubeAt(key_string(pos));

	Ability* abi = NULL;
	if ( checkForRotable && ( (abi = checkForRotable->GetAbility(Cube::ROTABLE, m_activeCharacter->m_up, move)) != NULL) )
	{
		if ( (abi->from.SameDirectionWith(m_activeCharacter->m_up) && abi->to.SameDirectionWith(move)) ||
			 (abi->to.SameDirectionWith(m_activeCharacter->m_up) && abi->from.SameDirectionWith(move)) ) // rotate should be two ways
		{
			if (checkForBlock)
				return -1; // block! should be rotate-move to another cube
			else
				return 1; // rotate-move to another face of the current cube
		}
	}
	return 0; // unrotable
}

bool Level::IsJumpable(Cube* cube)
{
	int cur;
	bool bRet = false;
	if ( (cur = cube->HasType(Cube::JUMP_ENTRY)) != -1 ) // hardcode: todo: fix general algorithms
	{
		Ability& abi = *(cube->m_abilities[Cube::JUMP_ENTRY]);
		bRet = abi.up.SameDirectionWith(m_activeCharacter->m_up);

		if (bRet)
		{
			Vector3 jump = (abi.to - m_activeCharacter->m_pos).Normalize();
			bRet = bRet && (jump.Dot(m_activeCharacter->m_face) > 0.2f); // we are jumping to right direction
		}
	}
	return bRet; 
}

bool Level::IsElevatorSwitch(Cube* cube)
{
	if ( cube->HasType(Cube::ELEVATOR_SWITCH) != -1 ) // hardcode: todo: fix general algorithms
	{
		return ( (cube->m_abilities[Cube::ELEVATOR_SWITCH])->up.SameDirectionWith(m_activeCharacter->m_up) );
	}
	return false;
}

void Level::MoveCharacter(Vector3& move)
{
	// if end level condition is met, do nothing
	if ( (m_boy.m_pos - m_girl.m_pos).Length() < 1.1f &&	// they are on adjacent cubes
		m_boy.m_up.SameDirectionWith(m_girl.m_up) )		// ... and same cubes side
		return;

	//Don't show guide any more

	// rotate the boy face to the moving direction
	Vector3 rotate = GetRotationVector( m_activeCharacter->m_face , move, m_activeCharacter->m_up );
	bool bNeedRotate = rotate.Length() > 0.1f;
	int icheck = 0;

	// make command to rotate character face
	if (bNeedRotate)
	{
		m_activeCharacter->InjectAnimEvent(CHARACTER_ANIM_EVENT_RUN);
		// rotate character face
		{
			ArgumentBundle args;
			args.m_arguments["newPos"] = new Vector3(m_activeCharacter->m_pos);
			args.m_arguments["rotate"] = new Vector3(rotate);
			args.m_arguments["up"] = new Vector3(m_activeCharacter->m_up);
			args.m_arguments["offset"] = new VECTOR3_ZERO;

			Command* cmd = new Command(Command::ROTATE_OBJECT, m_activeCharacter->m_obj, args, this, callback::DoRotateCharacter, callback::RotateCharacterCompleted, PLAYER_ROTATE_SPEED); //with faster speed than move
			m_commands.push_back(cmd);
		}
		// ...and rotate the camera also
		if (SCENE->GetCamera()->m_mode == Camera::CAMERA_LOCK_BEHIND)
		{
			ArgumentBundle args;
			args.m_arguments["rotate"] = new Vector3(rotate);

			Command* cmd = new Command(Command::ROTATE_CAMERA, NULL, args, this, callback::DoRotateCamera, NULL, CAMERA_ROTATE_SPEED);
			m_commands.push_back(cmd);
		}
	}
	// rotate face take a turn, so if we rotated face then return now, else check for move/rotate-move
	else
	{
		// check for available move
		if (IsMovable(move))
		{
			m_activeCharacter->InjectAnimEvent(CHARACTER_ANIM_EVENT_RUN);
			ArgumentBundle args;
			args.m_arguments["move"] = new Vector3(move);

			Command* cmd = new Command(Command::MOVE_OBJECT, m_activeCharacter->m_obj, args, this, callback::DoMoveCharacter, callback::MoveCharacterCompleted, PLAYER_MOVE_SPEED);
			m_commands.push_back(cmd);
		}
		// or available rotate-move
		else if ( (icheck = IsRotable(move)) != 0)
		{
			m_activeCharacter->InjectAnimEvent(CHARACTER_ANIM_EVENT_RUN);
			
			// rotate the object around the cube axis
			Vector3 m = move * (float)icheck;
			Vector3 newPos = (icheck == 1)? m_activeCharacter->m_pos:Vector3(m_activeCharacter->m_pos + m_activeCharacter->m_up + move);
			Vector3 cameraOffset = (icheck == 1)? VECTOR3_ZERO:m_activeCharacter->m_up;

			Vector3 rotate = GetRotationVector( m_activeCharacter->m_up , m, m.Cross(m_activeCharacter->m_up) );

			{
				ArgumentBundle args;
				args.m_arguments["newPos"] = new Vector3(newPos);
				args.m_arguments["rotate"] = new Vector3(rotate);
				args.m_arguments["up"] = new Vector3(m);
				args.m_arguments["offset"] = new Vector3(m_activeCharacter->m_up * CHAR_OFFSET * (float)icheck);

				Command* cmd = new Command(Command::ROTATE_CHAR, m_activeCharacter->m_obj, args, this, callback::DoRotateCharacter, callback::RotateCharacterCompleted, PLAYER_ROTATE_SPEED);
				m_commands.push_back(cmd);
			}

			if (SCENE->GetCamera()->m_mode == Camera::CAMERA_LOCK_BEHIND ||
				SCENE->GetCamera()->m_mode == Camera::CAMERA_LOOK_AT_CHARACTER)
			{
				ArgumentBundle args;
				args.m_arguments["rotate"] = new Vector3(rotate);
				args.m_arguments["point"] = new Vector3(m_activeCharacter->m_pos + cameraOffset);

				Command* cmd = new Command(Command::ROTATE_CAMERA, NULL, args, this, callback::DoRotateCameraWithPoint, callback::RotateCameraCompleted, CAMERA_ROTATE_SPEED);
				m_commands.push_back(cmd);
			}
		}
	} // bNeedRotate = false
}

void Level::SyncCharacterPosition(const Vector3& newPos)
{
	// unrelate the boy from old cube
	Vec3i oldpos(m_activeCharacter->m_pos);
	GetCubeAt(key_string(oldpos))->m_obj->UnrelateObject(m_activeCharacter->m_obj);

	m_activeCharacter->m_pos = newPos;

	Vec3i pos(m_activeCharacter->m_pos);
	OnEnterCube(*m_activeCharacter, GetCubeAt(key_string(pos)));
}

void Level::SyncCharacterRotation(const Vector3& rot, const Vector3& newPos, const Vector3& up)
{
	m_activeCharacter->m_up = up;
	Vector3 lightDirection = m_activeCharacter->m_up * (-1.0f);
	SetLight(lightDirection.x, lightDirection.y, lightDirection.z);
	if (rot.Length() > 0.1f)
		m_activeCharacter->m_face = RotatePoint(m_activeCharacter->m_face, rot);

	// we may change face, and trigger some jump so call OnEnterCube to check
	Vec3i pos(m_activeCharacter->m_pos);
	Vec3i newPosi(newPos);

	// if it's a rotate-move to another cube, update character pos
	if ( !(pos == newPosi))
	{
		m_activeCharacter->m_pos = newPos;
		Cube* cube = GetCubeAt(key_string(newPosi));
		OnEnterCube(*m_activeCharacter, cube);
	}
	else // keep the same pos
	{
		Cube* cube = GetCubeAt(key_string(pos));
		if (IsJumpable(cube))
		{
			OnEnterCube(*m_activeCharacter, cube);
		}
	}
}

void Level::ShowGuide(std::string text)
{
	CEGUI::Window* rootWnd = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::DefaultWindow* tutorialFrame = static_cast<CEGUI::DefaultWindow*>( rootWnd->getChild("FrameTutorial") );
	tutorialFrame->setText(text.c_str());
	tutorialFrame->setVisible(true);
}

void Level::CloseGuide()
{
	CEGUI::Window* rootWnd = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::DefaultWindow* tutorialFrame = static_cast<CEGUI::DefaultWindow*>( rootWnd->getChild("FrameTutorial") );
	tutorialFrame->setText("");
	tutorialFrame->setVisible(false);
}

void Level::OnEnterCube(Character& character, Cube* cube)
{
	// add the related object (boy) to the cube, so that when the cube move ~> boy also move
	cube->m_obj->RelateObject(character.m_obj);
	cube->DoConsume(character.m_up);

	//Check for open view grid
	if ( !m_bIsViewOpen && (cube->HasType(Cube::OPEN_VIEW) != -1) ) // hardcode: todo: fix general algorithms
	{
		m_bIsViewOpen = true;

		ArgumentBundle args;
		args.m_arguments["base"] = new float(FAR_PLANE);
		args.m_arguments["target"] = new float(TARGET_FARPLANE);

		Command* cmd = new Command(Command::OPEN_VIEW, NULL, args, this, callback::DoOpenView, callback::OpenViewCompleted, OPEN_VIEW_SPEED);
		m_commands.push_back(cmd);
	}
	//end

	//Check for guide grid
	if ( cube->HasType(Cube::GUIDE) != -1 ) // hardcode: todo: fix general algorithms
	{
		Ability& abi = *(cube->m_abilities[Cube::GUIDE]);
		int repeat = (int) abi.from.y;
		int forChar = (int) abi.from.z;
		if ((forChar == 0 && GetActiveCharacter() == &m_boy)
			||(forChar == 1 && GetActiveCharacter() == &m_girl))
		{
			if (repeat == 0)
			{
			}
			else if (repeat == 1)
			{

				abi.from.y = 0;
				int id = (int) abi.from.x;
				Level::ShowGuide(m_listMsg[id]);
				abi.obj->SetVisible(false);
			}else if (repeat == 2)
			{
				int id = (int) abi.from.x;
				Level::ShowGuide(m_listMsg[id]);
			}
		}
	}

	if (IsJumpable(cube))
	{
		m_activeCharacter->InjectAnimEvent(CHARACTER_ANIM_EVENT_JUMP);

		Ability& abi = *(cube->m_abilities[Cube::JUMP_ENTRY]);
		float radius = (abi.to - character.m_pos).Length() / 2.0f;

		// command to jump the character
		{
			ArgumentBundle args;
			args.m_arguments["R"] = new float(radius);
			args.m_arguments["up"] = new Vector3(character.m_up);
			args.m_arguments["face"] = new Vector3(character.m_face);
			args.m_arguments["dest"] = new Vector3(abi.to);

			// check if jump to higher position, ex: (0, 0, 0) to (0, 1, 0)
			Vector3 jump = abi.to - character.m_pos;
			jump.x *= abi.up.x; jump.y *= abi.up.y; jump.z *= abi.up.z;
			Vec3i jumpi(jump);
			
			if (jumpi.x > 0 || jumpi.y > 0 || jumpi.z > 0)
			{
				Command* cmd = new Command(Command::MOVE_OBJECT, character.m_obj, args, this, callback::DoJumpCharacterUp, callback::JumpCharacterCompleted, JUMP_SPEED);
				m_commands.push_back(cmd);
			}
			else
			if (jumpi.x < 0 || jumpi.y < 0 || jumpi.z < 0)
			{
				Command* cmd = new Command(Command::MOVE_OBJECT, character.m_obj, args, this, callback::DoJumpCharacterDown, callback::JumpCharacterCompleted, JUMP_SPEED);
				m_commands.push_back(cmd);
			}
			else
			{
				Command* cmd = new Command(Command::MOVE_OBJECT, character.m_obj, args, this, callback::DoJumpCharacter, callback::JumpCharacterCompleted, JUMP_SPEED);
				m_commands.push_back(cmd);
			}
		}
	}
	else if (IsElevatorSwitch(cube))
	{
		Ability& abi = *(cube->m_abilities[Cube::ELEVATOR_SWITCH]);
		Vec3i pos(abi.from);
		//cuong.doquoc Add null check, will do nothing in this case
		if (GetCubeAt(key_string(pos)) == NULL)
		{
			return;
		}
		//cuong.doquoc end
		Object* objToMove = GetCubeAt(key_string(pos))->m_obj;

		// command to move the 'from' grid to 'to' position
		{
			ArgumentBundle args;
			args.m_arguments["move"] = new Vector3(abi.to - abi.from);
			args.m_arguments["switch"] = new Vec3i(character.m_pos);
			args.m_arguments["from"] = new Vec3i(abi.from);
			args.m_arguments["to"] = new Vec3i(abi.to);

			Command* cmd = new Command(Command::MOVE_OBJECT, objToMove, args, this, callback::DoMoveCube, callback::MoveCubeCompleted, BOX_MOVE_SPEED);
			m_commands.push_back(cmd);
		}
	}
}

void Level::SyncCubeMove(Vec3i& switchPos, Vec3i& from, Vec3i& to)
{
	m_levelMap[key_string(to)] = GetCubeAt(key_string(from));
	m_levelMap.erase(key_string(from));
	
	// switch 'from' and 'to' so it is a two way elevator
	Ability* abi = GetCubeAt(key_string(switchPos))->m_abilities[Cube::ELEVATOR_SWITCH];
	Vector3 temp = abi->from;
	abi->from = abi->to;
	abi->to = temp;

	// if characters are on the moved cube, then sync their pos also
	Vec3i boypos(m_boy.m_pos);
	Vec3i girlpos(m_girl.m_pos);
	if (boypos == from)
		m_boy.m_pos = Vector3((float)to.x, (float)to.y, (float)to.z);
	if (girlpos == from)
		m_girl.m_pos = Vector3((float)to.x, (float)to.y, (float)to.z);
}

void Level::SwitchCharacter()
{
	if (ms_currentLevel != SWITCH_LEVEL
		|| m_bIsRotating == true)
		return;

	m_bIsRotating = true;
	// save current camera pos of the current active character, so we can restore when switch back
	m_activeCharacter->m_currentCamPos = SCENE->GetCamera()->m_position;
	m_activeCharacter->m_currentCamUp = SCENE->GetCamera()->m_up;

	// change the camera to behind character
	m_activeCharacter = (m_activeCharacter == &m_boy)? &m_girl:&m_boy;
	Vector3 lightDirection = m_activeCharacter->m_up * (-1.0f);
	SetLight(lightDirection.x, lightDirection.y, lightDirection.z);

	// make a command to zoom out camera, then when it complete zoom it into new character
	{
		ArgumentBundle args;
		args.m_arguments["zoom"] = new float(1.0f);

		Command* cmd = new Command(Command::ZOOM_CAMERA, NULL, args, this, callback::DoZoomCamera, callback::ZoomCameraCompleted, CAMERA_ZOOM_SPEED);
		m_commands.push_back(cmd);
	}
}

void Level::OnLevelCompleted()
{
	m_bIsLevelEnding = true;

	// rotate characters face to each other
	Vector3 boy2girl = m_girl.m_pos - m_boy.m_pos;
	Vector3 girl2boy = m_boy.m_pos - m_girl.m_pos;
	
	{
		Vector3 rotate = GetRotationVector( m_boy.m_face , boy2girl, m_boy.m_up );

		ArgumentBundle args;
		args.m_arguments["rotate"] = new Vector3(rotate);
		args.m_arguments["offset"] = new VECTOR3_ZERO;

		Command* cmd = new Command(Command::ROTATE_OBJECT, m_boy.m_obj, args, this, callback::DoRotateCharacter, callback::EndingAnimationCompleted, PLAYER_ROTATE_SPEED);
		m_commands.push_back(cmd);
	}

	{
		Vector3 rotate = GetRotationVector( m_girl.m_face , girl2boy, m_girl.m_up );

		ArgumentBundle args;
		args.m_arguments["rotate"] = new Vector3(rotate);
		args.m_arguments["offset"] = new VECTOR3_ZERO;

		Command* cmd = new Command(Command::ROTATE_OBJECT, m_girl.m_obj, args, this, callback::DoRotateCharacter, callback::EndingAnimationCompleted, PLAYER_ROTATE_SPEED);
		m_commands.push_back(cmd);
	}
}

void Level::OnLevelEndingAnimationCompleted()
{
	static int ending_state = 0;
	ending_state ++;

	if (ending_state == 1) // one character finish rotate face
	{
		//do nothing
	}
	else if (ending_state == 2) // both characters finish rotate face
	{
		// trigger the rotate around effect
		// rotate camera around 2 characters
		{
			ArgumentBundle args;
			args.m_arguments["rotate"] = new Vector3(m_boy.m_up * 7.0f); // rotate more than one round

			Command* cmd = new Command(Command::ROTATE_CAMERA, NULL, args, this, callback::DoEndingAnimation, callback::EndingAnimationCompleted, 0.1f);
			m_commands.push_back(cmd);
		}
	}
	else if (ending_state > 2) // rotate around effect ends
	{
		ending_state = 0; //reset it

		UnloadLevel();
		SCENE->SetGameState(new StateLoading());
	}
}

void Level::UnloadLevel()
{
	for (LevelMap::iterator it = m_levelMap.begin(); it != m_levelMap.end(); it++)
	{
		if (it->second)
			delete it->second;
	}
	m_levelMap.clear();
	m_boy.m_obj = m_girl.m_obj = NULL;
	SCENE->Clear();
}

void Level::OnZoomCompleted(bool bZoomIn)
{
	if (!bZoomIn)
	{
		m_bIsRotating = true;
		// if zoom-out completed, set new target and push a new command to zoom-in
		SCENE->GetCamera()->SetLookAtCharacter(	m_activeCharacter->m_pos + m_activeCharacter->m_up, 
												m_activeCharacter->m_currentCamPos, m_activeCharacter->m_currentCamUp, true);

		
		ArgumentBundle args;
		args.m_arguments["zoom"] = new float(-1.0f);

		Command* cmd = new Command(Command::ZOOM_CAMERA, NULL, args, this, callback::DoZoomCamera, callback::ZoomCameraCompleted, CAMERA_ZOOM_SPEED);
		m_commands.push_back(cmd);
	}
	else
	{
		// if zoom-in completed, reset new target too
			SCENE->GetCamera()->SetLookAtCharacter(	m_activeCharacter->m_pos + m_activeCharacter->m_up, 
												m_activeCharacter->m_currentCamPos, m_activeCharacter->m_currentCamUp, true);
	}
}

bool Level::IsCharacterMoving()
{
	for (CommandList::iterator it = m_commands.begin(); it != m_commands.end(); it++)
	{
		if ( (*it) && (*it)->m_bIsRunning ) 
			return true;
	}
	return false;
}