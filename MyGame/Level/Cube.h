#ifndef _CUBE_H_
#define _CUBE_H_

#include <list>
#include "Utilities/Math.h"
#include "CoreEngine/Object/Object.h"
#include "Ability.h"


class Consumable
{
public:
	Consumable(const Vector3& u, Object* o) : up(u), obj(o) {}
	~Consumable() {}

	Vector3 up;
	Object* obj;
};

typedef std::list<Consumable*>		ConsumableList;


class Cube
{
public:
	// bit wise, each type can has max 3
	// for ex: a cube has 3 different rotate points then its type will have all: ROTABLE=32768, ROTABLE1=65536, ROTABLE2=131072
	enum CubeType 
	{
		NORMAL			= 1,
		JUMP_ENTRY		= 8,
		ELEVATOR_SWITCH = 64,
		OPEN_VIEW		= 512,
		GUIDE			= 4096,
		ROTABLE			= 32768
	};

	Cube();
	~Cube();

	Ability* operator[](long key) { return m_abilities[key]; }

	void Load(const Vector3& pos, const char* boxmodel);
	
	void AddType(CubeType type) { m_type |= (long)type; }
	int HasType(CubeType type);
	bool IsKindOf(CubeType type, CubeType kind);

	void AddAbility(CubeType type, const Vector3& up, const Vector3& from, const Vector3& to);
	void AddConsumable(const Vector3& pos, const Vector3& up, const Vector3& rot, const char* name);
	void DoConsume(Vector3& characterUp);
	Ability* GetAbility(CubeType type, Vector3& from, Vector3& to);
	
	Object* CreateAbilityObject(CubeType type, const Vector3& up, const Vector3& from, const Vector3& to);
	void TriggerAbility(CubeType type);

	long m_type;
	Object* m_obj;
	ConsumableList m_consumables;
	Vector3 m_pos;

	AbilityMap m_abilities;
};

Cube* Cube_AddCube(Cube* existing, Cube::CubeType type, const Vector3& pos, const Vector3& up, const Vector3& from, const Vector3& to);

#endif