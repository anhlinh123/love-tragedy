#ifndef _ABILITY_H_
#define _ABILITY_H_

#include <map>
#include "Utilities/Math.h"
#include "CoreEngine/Object/Object.h"
#include "StateMachine.h"


// enums
enum ABI_ANIM_STATE
{
	ABI_ANIM_STATE_IDLE,
	ABI_ANIM_STATE_TRIGGER
};

enum ABI_ANIM_EVENT
{
	ABI_ANIM_EVENT_TRIGGER,
	ABI_ANIM_EVENT_STOP
};


class Ability
{
public:
	Ability(const Vector3& u, const Vector3& f, const Vector3& t, Object* o) : up(u), from(f), to(t), obj(o)  {}
	~Ability() {}

	void InitAnimation();
	void OnEnterTrigger();
	void OnEnterIdle();
	void InjectAnimEvent(ABI_ANIM_EVENT ev);

	Vector3 up;
	Vector3 from;
	Vector3 to;
	Object* obj; // can be null - for normal ability

	StateMachine<Ability> m_animationStates;
};

typedef std::map<long, Ability*>	AbilityMap;

#endif