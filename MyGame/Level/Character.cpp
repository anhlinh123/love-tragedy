#include "stdafx.h"
#include <math.h>
#include "CoreEngine\Controller\Scene.h"
#include "CoreEngine\Object\Model.h"
#include "Character.h"


void Character::Load(Json::Value& data)
{
	m_pos = Vector3(data["pos"][0].asFloat(), data["pos"][1].asFloat(), data["pos"][2].asFloat());
	m_up = Vector3(data["up"][0].asFloat(), data["up"][1].asFloat(), data["up"][2].asFloat());
	m_face = Vector3(data["face"][0].asFloat(), data["face"][1].asFloat(), data["face"][2].asFloat());

	Vector3 original_up = Vector3(data["original_up"][0].asFloat(), data["original_up"][1].asFloat(), data["original_up"][2].asFloat());
	Vector3 rotate = Vector3(data["rotate"][0].asFloat(), data["rotate"][1].asFloat(), data["rotate"][2].asFloat());
	Vector3 scale = Vector3(data["scale"][0].asFloat(), data["scale"][1].asFloat(), data["scale"][2].asFloat());

	Object* o = new Object(CreateModel(data["name"].asString().c_str()));
	o->SetGeometry( m_pos + m_up * CHAR_OFFSET,			// position is at center of the cube, so this hack help character stand on top of the cube
					rotate,	scale);
	SCENE->AddObject(o);
	m_obj = o;

	// set up camera for this character
	Vector3 offset = Vector3(data["camera_offset"][0].asFloat(), data["camera_offset"][1].asFloat(), data["camera_offset"][2].asFloat());
	Vector3 xaxis = m_face.Normalize();
	Vector3 yaxis = m_up.Normalize();
	Vector3 zaxis = m_face.Cross(m_up).Normalize();

	m_currentCamPos = (m_pos + m_up) + (xaxis * offset.x + yaxis * offset.y + zaxis * offset.z);
	m_currentCamUp = m_up;
	
	// animation states
	static const StateMachine<Character>::Transition  transitionTable[] =
	{
	  //State From							Transition Event					State To
	  { CHARACTER_ANIM_STATE_IDLE,			CHARACTER_ANIM_EVENT_RUN,			CHARACTER_ANIM_STATE_RUNNING },
	  { CHARACTER_ANIM_STATE_RUNNING,		CHARACTER_ANIM_EVENT_STOP,			CHARACTER_ANIM_STATE_IDLE },
	  { CHARACTER_ANIM_STATE_RUNNING,		CHARACTER_ANIM_EVENT_JUMP,			CHARACTER_ANIM_STATE_JUMPING },
	  { CHARACTER_ANIM_STATE_JUMPING,		CHARACTER_ANIM_EVENT_STOP,			CHARACTER_ANIM_STATE_RUNNING }
	};

	static const StateMachine<Character>::State  states[] =
	{
	  // Name								OnEnter,						OnExit,             OnUpdate
	  { CHARACTER_ANIM_STATE_IDLE,			&Character::OnEnterIdle,		NULL,               NULL },
	  { CHARACTER_ANIM_STATE_RUNNING,		&Character::OnEnterRun,			NULL,               NULL },
	  { CHARACTER_ANIM_STATE_JUMPING,		&Character::OnEnterJump,		NULL,               NULL }
	};

	m_animationStates.InitStateMachine<4>(transitionTable, this, states);
	m_animationStates.SetInitialState(CHARACTER_ANIM_STATE_IDLE);
}

void Character::OnEnterRun() 
{
	//printf("running\n");
	m_obj->PlayAnimation("walk");
}

void Character::OnEnterIdle() 
{
	//printf("idling\n");
	m_obj->PlayAnimation("idle");
}

void Character::OnEnterJump() 
{
	//printf("idling\n");
	m_obj->PlayAnimation("jump");
}

void Character::InjectAnimEvent(CHARACTER_ANIM_EVENT ev)
{
	m_animationStates.InjectEvent(ev);
}