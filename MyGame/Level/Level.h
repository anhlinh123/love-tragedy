#pragma once

#ifndef _LEVEL_H_
#define _LEVEL_H_

#include <map>
#include "Command.h"
#include "Character.h"
#include "Cube.h"
#include "Utilities/Math.h"
#include "Jsoncpp/json/json.h"

// forward declare
class Object;


#define		MAX_LEVEL_SIZE	30
#define		BOX_SIZE		1.0f	// the box model is 1.0f width/height
#define		HALF_BOX_SIZE	0.5f
#define		MAXLEVEL		4

#define		CAMERA_ROTATE_SPEED		1.5f
#define		PLAYER_MOVE_SPEED		4.0f
#define		PLAYER_ROTATE_SPEED		4.0f
#define		CAMERA_ZOOM_SPEED		2.0f
#define		BOX_MOVE_SPEED			3.0f
#define		JUMP_SPEED				2.0f
#define		SWITCH_LEVEL			1
#define		TARGET_FARPLANE			150.0f
#define		OPEN_VIEW_SPEED			0.3f


// Use this map instead of 3-dimension array.
// For ex: to access the cube at (0, 1, 0) then we use the key "[0][1][0]"
typedef std::map<std::string, Cube*> LevelMap;


class Vec3i
{
public:
	Vec3i(const Vector3& vec3f);
	Vec3i(float a, float b, float c);
	Vec3i(int a, int b, int c) : x(a), y(b), z(c) {}
	~Vec3i() {}
	bool operator== (const Vec3i& v) { return (this->x == v.x && this->y == v.y && this->z == v.z); }
	
	int x, y, z;
};


class Level
{
public:
	Level();
	~Level();

	void LoadLevel(const char* level);
	void ProcessCommands(float deltaTime);
	
	// boy control functions
	void MoveCharacter(Vector3& move);
	void SyncCharacterPosition(const Vector3& newPos);
	void SyncCharacterRotation(const Vector3& rot, const Vector3& newPos, const Vector3& up);
	Character* GetActiveCharacter() { return m_activeCharacter; }
	bool IsCharacterMoving();
	void SwitchCharacter();

	// Cube functions
	bool IsMovable(Vector3& move);
	int IsRotable(Vector3& move);
	bool IsJumpable(Cube* cube);
	bool IsElevatorSwitch(Cube* cube);

	void OnEnterCube(Character& character, Cube* cube);
	void SyncCubeMove(Vec3i& switchPos, Vec3i& from, Vec3i& to);
	Cube* GetCubeAt(std::string key);

	//
	void OnLevelCompleted();
	void OnLevelEndingAnimationCompleted();
	bool IsLevelEnding() { return m_bIsLevelEnding; }
	void UnloadLevel();
	void ShowGuide(std::string text);
	void CloseGuide();
	void OnZoomCompleted(bool bZoomIn);

	bool m_bIsRotating;
protected:
	LevelMap m_levelMap;

	Character m_boy;
	Character m_girl;
	Character* m_activeCharacter;

	std::string* m_listMsg;

	CommandList m_commands;
	bool m_bIsLevelEnding;
	bool m_bIsViewOpen;
};

// key construction
std::string key_string(int i, int j, int k);
std::string key_string(Json::Value& pos);
std::string key_string(Vec3i& pos);

// Replay/Start new level
void Level_ReplayLevel();
void Level_PlayNextLevel();

// utils
Vector3 GetRotationVector(const Vector3& from, const Vector3& to, const Vector3& up);
Vector3 GetRotationVector2(const Vector3& from, const Vector3& to, const Vector3& up);
Vector3 RotatePoint(const Vector3& point, const Vector3& rot);
Vector3 GetHardcodedUp(const Vector3& from, const Vector3& to);

#endif