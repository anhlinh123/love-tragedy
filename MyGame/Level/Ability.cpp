#include "stdafx.h"
#include "Ability.h"


void Ability::InitAnimation()
{
	// animation states
	static const StateMachine<Ability>::Transition  transitionTable[] =
	{
	  //State From					Transition Event			State To
	  { ABI_ANIM_STATE_IDLE,        ABI_ANIM_EVENT_TRIGGER,			ABI_ANIM_STATE_TRIGGER },
	  { ABI_ANIM_STATE_TRIGGER,     ABI_ANIM_EVENT_STOP,        ABI_ANIM_STATE_IDLE }
	};

	static const StateMachine<Ability>::State  states[] =
	{
	  // Name						OnEnter,						OnExit,             OnUpdate
	  { ABI_ANIM_STATE_IDLE,		&Ability::OnEnterIdle,			NULL,               NULL },
	  { ABI_ANIM_STATE_TRIGGER,		&Ability::OnEnterTrigger,		NULL,               NULL }
	};

	m_animationStates.InitStateMachine<2>(transitionTable, this, states);
	m_animationStates.SetInitialState(ABI_ANIM_STATE_IDLE);
}

void Ability::OnEnterTrigger() 
{
	//printf("running\n");
	obj->PlayAnimation("trigger");
}

void Ability::OnEnterIdle() 
{
	//printf("idling\n");
	obj->PlayAnimation("idle");
}

void Ability::InjectAnimEvent(ABI_ANIM_EVENT ev)
{
	m_animationStates.InjectEvent(ev);
}