#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_


template<typename OWNER>
class StateMachine
{
public:
	struct Transition
	{
		int stateFrom;
		int trigger;
		int stateTo;
	};

	struct State
	{
		int name;
		void (OWNER::*onEnter)(void);
		void (OWNER::*onExit)(void);
		void (OWNER::*onUpdate)(float dt);
	};

public:
	StateMachine() {}
	~StateMachine() {}

	template<unsigned int SIZE>
	void InitStateMachine( const Transition(&transitionTable)[SIZE], OWNER* owner, const State* statesTable = NULL )
	{
		m_transitionTable = transitionTable;
		m_statesTable = statesTable;
		m_transitionTableSize = SIZE;
		m_owner = owner;

		m_currentState = -1;
	}

	void SetInitialState(int currentState )
	{
		m_currentState = currentState;

		if( m_statesTable && m_statesTable[m_currentState].onEnter )
		{
			(m_owner->*m_statesTable[m_currentState].onEnter)();
		}
	}

	bool InjectEvent(int eventID)
	{
		//  Run through table & find matching transition
		for(unsigned int i = 0; i < m_transitionTableSize; ++i)
		{
			const Transition* transition = &m_transitionTable[i];

			//  Special ANY placeholder state, with ID < 0
			if(  ((transition->stateFrom < 0) || (transition->stateFrom == m_currentState)) 
				&& transition->trigger == eventID )
			{
				//  On Exit
				if( m_statesTable && m_statesTable[m_currentState].onExit )
				{
					(m_owner->*m_statesTable[m_currentState].onExit)();
				}

				//  Apply new state
				m_currentState = transition->stateTo;

				//  OnEnter
				if( m_statesTable && m_statesTable[m_currentState].onEnter )
				{
					(m_owner->*m_statesTable[m_currentState].onEnter)();
				}

				return true;
			}
		}

		return false;
	}

	void Update(float dt)
	{
		if( m_statesTable && m_statesTable[m_currentState].onUpdate )
		{
			m_statesTable[m_currentState].onUpdate(dt);
		}
	}

	int GetCurrentState() const  { return m_currentState; }

protected:
	int               m_currentState;

	//  read only part
	const Transition* m_transitionTable;
	unsigned int      m_transitionTableSize;
	const State*      m_statesTable;
	OWNER*			  m_owner;
};

#endif