#include "stdafx.h"
#include "CoreEngine\Object\Model.h"
#include "CoreEngine/Controller/Scene.h"
#include "Cube.h"
#include "Level.h"
#include "Jsoncpp/json/json.h"


Cube* Cube_AddCube(Cube* existing, Cube::CubeType type, const Vector3& pos, const Vector3& up, const Vector3& from, const Vector3& to)
{
	if (existing)
	{
		existing->AddAbility(type, up, from, to);
		return existing;
	}
	else
	{
		Cube* newCube = new Cube();
		if (type == Cube::ELEVATOR_SWITCH || type == Cube::JUMP_ENTRY)
			newCube->Load(pos, "Box2.json");
		else
			newCube->Load(pos, "Box.json");
		newCube->AddAbility(type, up, from, to);
		return newCube;
	}
}

Cube::Cube() 
{
	m_type = 0;
	m_obj = NULL;
	AddType(Cube::NORMAL);
}

void Cube::Load(const Vector3& pos, const char* boxmodel)
{
	// if exist then do nothing, else load the cube model
	if (!m_obj)
	{
		m_pos = pos;
		Object* o = new Object(CreateModel(boxmodel));
		o->SetGeometry(	m_pos + Vector3(0.0f, 0.0f, -0.45f), VECTOR3_ZERO, Vector3(0.03f, 0.03f, 0.03f));
		SCENE->AddObject(o);
		m_obj = o;
	}
}

Cube::~Cube()
{
	for (AbilityMap::iterator it = m_abilities.begin(); it != m_abilities.end(); it++)
	{
		delete it->second;
	}
	for (ConsumableList::iterator it = m_consumables.begin(); it != m_consumables.end(); it++)
	{
		delete (*it);
	}
}

void Cube::AddAbility(CubeType type, const Vector3& up, const Vector3& from, const Vector3& to)
{
	int cur;
	if ( (cur = HasType(type)) != 2) // we do not add max type of a kind yet
	{
		type = (CubeType)( (long)type << (cur + 1));
		AddType(type);
		Object* o = CreateAbilityObject(type, up, from, to);
		m_abilities[type] = new Ability(up, from, to, o);
	}
}

void Cube::AddConsumable(const Vector3& pos, const Vector3& up, const Vector3& rot, const char* name)
{
	Object* o = new Object(CreateModel("gift.json"));

	// rotate the ability object. hardcoded now due to our 3d asset.
	Vector3 aup_rot = GetRotationVector2( Vector3(0.0f, 0.0f, 1.0f), up, Vector3(0.0f, 0.0f, 1.0f).Cross(up) );

	o->SetGeometry(	m_pos + up * 0.7f, 
					aup_rot, 
					Vector3(0.04f, 0.04f, 0.04f));
	SCENE->AddObject(o);
	m_consumables.push_back(new Consumable(up, o));
	m_obj->RelateObject(o);
}

void Cube::DoConsume(Vector3& characterUp)
{
	for (ConsumableList::iterator it = m_consumables.begin(); it != m_consumables.end(); it++)
	{
		Consumable* cons = (*it);
		if ( cons->up.SameDirectionWith(characterUp) ) // same direction
		{
			cons->obj->SetVisible(false);
			//m_obj->UnrelateObject(cons->obj);

			//delete the consumable
			delete cons;
			m_consumables.erase(it);
			break;
		}
	}
}

Object* Cube::CreateAbilityObject(CubeType type, const Vector3& up, const Vector3& from, const Vector3& to)
{
	// create a visual object for the ability
	// ex: an arrow, a button...

	if ( IsKindOf(type, Cube::NORMAL) )
		return NULL;

	Object* o = NULL;
	if ( IsKindOf(type, Cube::GUIDE) )
	{
		o = new Object(CreateModel("help.json"));

		// rotate the ability object. hardcoded now due to our 3d asset.
		//Vector3 aup_rot = GetRotationVector2( Vector3(0.0f, 0.0f, 1.0f), up, Vector3(0.0f, 0.0f, 1.0f).Cross(up) );

		o->SetGeometry(	m_pos + up * 0.65f, VECTOR3_ZERO, Vector3(0.01f, 0.01f, 0.01f));
		//aup_rot 000;
		//up
	}else if ( IsKindOf(type, Cube::ROTABLE) )
	{
		o = new Object(CreateModel("Rotate.json"));

		// rotate the ability object. hardcoded now due to our 3d asset.
		Vector3 direct = to/* * (-1.0f)*/;
		Vector3 aface_rot = GetRotationVector2( Vector3(0.0f, 0.0f, -1.0f), direct, Vector3(0.0f, 0.0f, -1.0f).Cross(direct) );
		Vector3 aup_rot = GetRotationVector2( Vector3(1.0f, 0.0f, 0.0f), from, Vector3(1.0f, 0.0f, 0.0f).Cross(from) );

		o->SetGeometry(	m_pos + ((from + to) * 0.5f), aface_rot + aup_rot, Vector3(0.03f, 0.03f, 0.03f));
	}
	else if ( IsKindOf(type, Cube::JUMP_ENTRY) )
	{
		o = new Object(CreateModel("Arrow.json"));

		// rotate the ability object. hardcoded now due to our 3d asset.
		Vector3 direct = to - m_pos;
		direct.x *= up.x; direct.y *= up.y; direct.z *= up.z;
		if (direct.Length() < 0.1f) // imply direct equals VECTOR3_ZERO
			direct = to - m_pos;
		else if (direct.x > 0.0f || direct.y > 0.0f || direct.z > 0.0f)
			direct = to - m_pos - up; //jump up
		else if (direct.x < 0.0f || direct.y < 0.0f || direct.z < 0.0f)
			direct = to - m_pos + up; //jump down

		Vector3 aface_rot = GetRotationVector2( Vector3(0.0f, 0.0f, -1.0f), direct, Vector3(0.0f, 0.0f, -1.0f).Cross(direct) );
		Vector3 aup_rot = GetRotationVector2( Vector3(1.0f, 0.0f, 0.0f), up, Vector3(1.0f, 0.0f, 0.0f).Cross(up) );

		o->SetGeometry(	m_pos + up * 0.7f, aface_rot + aup_rot, Vector3(0.02f, 0.02f, 0.02f));
	}
	else if ( IsKindOf(type, Cube::ELEVATOR_SWITCH) )
	{
		o = new Object(CreateModel("Push.json"));

		// rotate the ability object. hardcoded now due to our 3d asset.
		Vector3 aup_rot = GetRotationVector2( Vector3(0.0f, 0.0f, 1.0f), up, Vector3(0.0f, 0.0f, 1.0f).Cross(up) );

		o->SetGeometry(	m_pos + up * 0.45f, aup_rot, Vector3(0.02f, 0.02f, 0.02f));
	}
	else
	{
		o = new Object(CreateModel("Item.json"));
		o->SetGeometry(	m_pos + up * 0.5f, VECTOR3_ZERO, Vector3(0.08f, 0.08f, 0.08f));
	}
	SCENE->AddObject(o);
	m_obj->RelateObject(o);
	return o;
}

void Cube::TriggerAbility(CubeType type)
{
	if (HasType(type) != -1)
	{
		AbilityMap::const_iterator iterator = m_abilities.find(type);
		if (iterator != m_abilities.end())
		{
			// found the ability! trigger it
			Ability* abi = iterator->second;
			if (abi)
			{
				abi->InjectAnimEvent(ABI_ANIM_EVENT_TRIGGER);
			}
		}
	}
}

// return the current max type of the same kind
int Cube::HasType(CubeType type) 
{ 
	if ((m_type & ((long)type)) != 0)
		return 0;
	else if ((m_type & ((long)type<<1)) != 0)
		return 1;
	else if ((m_type & ((long)type<<2)) != 0)
		return 2;
	return -1;
}

bool Cube::IsKindOf(CubeType type, CubeType kind)
{
	if (type == kind)
		return true;
	else if ( (long)type == (long)(kind<<1))
		return true;
	else if ( (long)type == (long)(kind<<2))
		return true;
	return false;
}

Ability* Cube::GetAbility(CubeType type, Vector3& from, Vector3& to)
{
	for (int i = 0; i < 3; i++)
	{
		type = (CubeType)((long)type << i);
		if ((m_type & ((long)type)) != 0)
		{
			Ability* abi = m_abilities[type];
			if (	(abi->from.SameDirectionWith(from) && abi->to.SameDirectionWith(to)) ||
					(abi->from.SameDirectionWith(to) && abi->to.SameDirectionWith(from)) )
				return abi;
		}
	}
	return NULL;
}