#include "StdAfx.h"
#include "GUIWelcomeScreen.h"

#include "CEGUI\WindowManager.h"
#include "CEGUI\GUIContext.h"
#include "CEGUI\ImageManager.h"
#include "CEGUI\Font.h"
#include "CEGUI\Scheme.h"
#include "CEGUI\falagard\WidgetLookManager.h"
#include "CEGUI\SchemeManager.h"

GUIWelcomeScreen::GUIWelcomeScreen()
{
	CEGUI::SchemeManager::getSingleton().createFromFile("Game.scheme");
	CEGUI::Window* rootWnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile( "WelcomeScreen.layout" );
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( rootWnd );
}

GUIWelcomeScreen::~GUIWelcomeScreen(void)
{
}

void GUIWelcomeScreen::changeImage(const char* filename)
{
	if (!CEGUI::ImageManager::getSingleton().isDefined(filename))
		CEGUI::ImageManager::getSingleton().addFromImageFile(filename, filename);
	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("Image")->setProperty("Image", filename);
}