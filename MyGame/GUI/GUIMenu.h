#pragma once
#include "cegui/widgets/PushButton.h"

class GUIMenu
{
public:
	GUIMenu(void);
	~GUIMenu(void);

	void ResetGUI();

private:
	CEGUI::PushButton* m_btnPlay;
	CEGUI::PushButton* m_btnAbout;
	CEGUI::PushButton* m_btnExit;
	CEGUI::Window* m_aboutWnd;
	CEGUI::Window* m_background;

	bool OnPlayClick(const CEGUI::EventArgs& args);
	bool OnAboutBtnClick(const CEGUI::EventArgs& args);
	bool OnExitClick(const CEGUI::EventArgs& args);
};
