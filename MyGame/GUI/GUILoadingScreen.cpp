#include "StdAfx.h"
#include "GUILoadingScreen.h"

#include "CEGUI\WindowManager.h"
#include "CEGUI\GUIContext.h"
#include "CEGUI\ImageManager.h"
#include "CEGUI\Font.h"
#include "CEGUI\Scheme.h"
#include "CEGUI\falagard\WidgetLookManager.h"
#include "CEGUI\SchemeManager.h"

#ifdef ANDROID
extern "C" void nativeHideAd();
#endif

GUILoadingScreen::GUILoadingScreen()
{
	CEGUI::Window* rootWnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile( "LoadingScreen.layout" );
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( rootWnd );
#ifdef ANDROID
	nativeHideAd();
#endif
}


GUILoadingScreen::~GUILoadingScreen()
{
}

void GUILoadingScreen::UpdateProgress(float percentage)
{
}