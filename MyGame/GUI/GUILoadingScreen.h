#pragma once

#include "CEGUI\widgets\Progressbar.h"

class GUILoadingScreen
{
public:
	GUILoadingScreen();
	~GUILoadingScreen();

	void UpdateProgress(float percentage);

protected:
	CEGUI::ProgressBar* m_progressbar;
};

