#include "StdAfx.h"
#include "GUIMenu.h"
#include "CEGUI\WindowManager.h"
#include "CoreEngine/Controller/Scene.h"
#include "GameStates/StateLoading.h"
#include "GameStates/StateAbout.h"
#include "Sound/SoundManager.h"

#ifdef ANDROID
extern "C" void nativeShowAd();
#endif

GUIMenu::GUIMenu(void)
{
	CEGUI::Window* rootWnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile( "GameMenu.layout" );
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( rootWnd );

	m_btnPlay  = static_cast<CEGUI::PushButton*>( rootWnd->getChild("ButtonContainer/ButtonPlay") );
	m_btnAbout = static_cast<CEGUI::PushButton*>( rootWnd->getChild("ButtonContainer/ButtonAbout") );
	m_btnExit  = static_cast<CEGUI::PushButton*>( rootWnd->getChild("ButtonContainer/ButtonExit") );

	m_background = rootWnd->getChild("Background");
	m_aboutWnd = rootWnd->getChild("Background/About");
	m_aboutWnd->setVisible(false);

	m_btnPlay->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUIMenu::OnPlayClick, this));
	m_btnAbout->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUIMenu::OnAboutBtnClick, this));
	m_btnExit->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUIMenu::OnExitClick, this));

#ifdef ANDROID
	nativeShowAd();
#endif
}

GUIMenu::~GUIMenu(void)
{
}

bool GUIMenu::OnPlayClick(const CEGUI::EventArgs& args)
{
	SOUND->PlaySound("Music1");
	SCENE->SetGameState(new StateLoading());
	return true;
}

bool GUIMenu::OnAboutBtnClick(const CEGUI::EventArgs& args)
{
	SOUND->PlaySound("Music1");
	m_btnPlay->setVisible(false);
	m_btnAbout->setVisible(false);
	m_btnExit->setVisible(false);
	m_aboutWnd->setVisible(true);
	m_background->setProperty("ImageColours", "tl:55555555 tr:55555555 bl:55555555 br:55555555");
	return true;
}

#ifdef ANDROID
extern "C" void nativeExit();
#endif

bool GUIMenu::OnExitClick(const CEGUI::EventArgs& args)
{
	//Exit
#ifdef ANDROID
	nativeExit();
#endif
	SOUND->PlaySound("Music1");

	return true;
}

void GUIMenu::ResetGUI()
{
	m_btnPlay->setVisible(true);
	m_btnAbout->setVisible(true);
	m_btnExit->setVisible(true);
	m_aboutWnd->setVisible(false);
	m_background->setProperty("ImageColours", "tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF");
}