#include "StdAfx.h"
#include "GUIPlay.h"

#include "CEGUI\WindowManager.h"
#include "CEGUI\GUIContext.h"
#include "CEGUI\ImageManager.h"
#include "CEGUI\Font.h"
#include "CEGUI\Scheme.h"
#include "CEGUI\falagard\WidgetLookManager.h"
#include "CEGUI\SchemeManager.h"

#include "CoreEngine/Controller/GameInput.h"
#include "CoreEngine/Controller/Scene.h"

#include "GameStates/StateOptions.h"
#include "Level\Level.h"
#include "Sound/SoundManager.h"
#include "GameStates/StateMenu.h"

extern int ms_currentLevel;

GUIPlay::GUIPlay()
{
	CEGUI::Window* rootWnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile( "Gameplay.layout" );
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( rootWnd );

	m_pauseBtn      = static_cast<CEGUI::PushButton*>( rootWnd->getChild("Button_Pause") );
	m_controlTabBtn = static_cast<CEGUI::PushButton*>( rootWnd->getChild("Button_ControlTab") );
	m_changeBtn     = static_cast<CEGUI::PushButton*>( rootWnd->getChild("Button_ChangeCharacter") );
	m_controlBtn    = static_cast<CEGUI::PushButton*>( rootWnd->getChild("Button_ControlTab/Button_ControlPoint") );

	m_pauseMenu     = rootWnd->getChild("PauseTab");
	m_resumeBtn     = static_cast<CEGUI::PushButton*>( m_pauseMenu->getChild("ButtonResume") );
	m_replayBtn     = static_cast<CEGUI::PushButton*>( m_pauseMenu->getChild("ButtonReplay") );
	m_soundOnBtn    = static_cast<CEGUI::PushButton*>( m_pauseMenu->getChild("ButtonSoundOn") );
	m_soundOffBtn   = static_cast<CEGUI::PushButton*>( m_pauseMenu->getChild("ButtonSoundOff") );
	m_exitBtn       = static_cast<CEGUI::PushButton*>( m_pauseMenu->getChild("ButtonExit") );
	m_pauseMenu->setVisible(false);
	
	CEGUI::Window* tutorialFrame = rootWnd->getChild("FrameTutorial");
	tutorialFrame->setVisible(false);

	if (ms_currentLevel != SWITCH_LEVEL)
	{
		m_changeBtn->setVisible(false);
	}
	else
	{
		m_changeBtn->setVisible(true);
	}

	m_pauseBtn->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUIPlay::OnPauseClick, this));
	m_changeBtn->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUIPlay::OnChangeClick, this));
	m_controlTabBtn->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&GUIPlay::OnControlDown, this));
	m_controlTabBtn->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUIPlay::OnControlUp, this));
	m_controlTabBtn->subscribeEvent(CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber(&GUIPlay::OnControlDrag, this));
	m_resumeBtn->subscribeEvent(CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber(&GUIPlay::OnResumeClick, this));
	m_replayBtn->subscribeEvent(CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber(&GUIPlay::OnReplayClick, this));
	m_soundOnBtn->subscribeEvent(CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber(&GUIPlay::OnSoundOnClick, this));
	m_soundOffBtn->subscribeEvent(CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber(&GUIPlay::OnSoundOffClick, this));
	m_exitBtn->subscribeEvent(CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber(&GUIPlay::OnExitClick, this));
}

void GUIPlay::ResetGUI()
{
	if (m_pauseMenu->isVisible())
	{
		if (ms_currentLevel != SWITCH_LEVEL)
		{
			m_changeBtn->setVisible(false);
		}
		else
		{
			m_changeBtn->setVisible(true);
		}
		m_pauseBtn->setVisible(true);
		m_controlTabBtn->setVisible(true);
		m_pauseMenu->setVisible(false);
	}
}

bool GUIPlay::OnPauseClick(const CEGUI::EventArgs& args)
{
	m_pauseMenu->setVisible(true);
	m_changeBtn->setVisible(false);
	m_pauseBtn->setVisible(false);
	m_controlTabBtn->setVisible(false);
	SOUND->PlaySound("Music1");
	return true;
}

bool GUIPlay::OnChangeClick(const CEGUI::EventArgs& args)
{
	SOUND->PlaySound("Music1");
	GAME_INPUT->InjectKey(GK_BUTTON_X, false);
	return true;
}

bool GUIPlay::OnControlDown(const CEGUI::EventArgs& args)
{
	CEGUI::Vector2f mousePos = m_controlTabBtn->getGUIContext().getMouseCursor().getPosition();
	CEGUI::Vector2f tabPos = m_controlTabBtn->getPixelPosition();
	CEGUI::Sizef tabSize = m_controlTabBtn->getPixelSize();
	int disX = (int) (mousePos.d_x - tabPos.d_x - tabSize.d_width / 2.0f);
	int disY = (int) (mousePos.d_y - tabPos.d_y - tabSize.d_height / 2.0f);

	int controlPosX, controlPosY;
	CEGUI::Sizef controlSize = (tabSize - m_controlBtn->getPixelSize()) * 0.5f;
	if ( disX <= -controlSize.d_width)
	{
		controlPosX = -(int)controlSize.d_width;
	}
	else if (disX >= controlSize.d_width)
	{
		controlPosX = (int)controlSize.d_width;
	}
	else
	{
		controlPosX = disX;
	}
	if ( disY <= -controlSize.d_height)
	{
		controlPosY = -(int)controlSize.d_height;
	}
	else if (disY >= controlSize.d_height)
	{
		controlPosY = (int)controlSize.d_height;
	}
	else
	{
		controlPosY = disY;
	}
	m_controlBtn->setPosition(CEGUI::UVector2( CEGUI::UDim(0, (float) controlPosX), CEGUI::UDim(0, (float) controlPosY)));

	if ( abs(disX) >= abs(disY))
	{
		if ( disX > 0)
		{
			GAME_INPUT->InjectKey(GK_PLAYER_RIGHT, true);
		}
		else
		{
			GAME_INPUT->InjectKey(GK_PLAYER_LEFT, true);
		}
	}
	else
	{
		if ( disY > 0)
		{
			GAME_INPUT->InjectKey(GK_PLAYER_DOWN, true);
		}
		else
		{
			GAME_INPUT->InjectKey(GK_PLAYER_UP, true);
		}
	}
	return true;
}

bool GUIPlay::OnControlUp(const CEGUI::EventArgs& args)
{
	CEGUI::Vector2f mousePos = m_controlTabBtn->getGUIContext().getMouseCursor().getPosition();
	CEGUI::Vector2f tabPos = m_controlTabBtn->getPixelPosition();
	CEGUI::Sizef tabSize = m_controlTabBtn->getPixelSize();
	int disX = (int) (mousePos.d_x - tabPos.d_x - tabSize.d_width / 2.0f);
	int disY = (int) (mousePos.d_y - tabPos.d_y - tabSize.d_height / 2.0f);

	m_controlBtn->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0)));
	if ( abs(disX) >= abs(disY))
	{
		if ( disX > 0)
		{
			GAME_INPUT->InjectKey(GK_PLAYER_RIGHT, false);
		}
		else
		{
			GAME_INPUT->InjectKey(GK_PLAYER_LEFT, false);
		}
	}
	else
	{
		if ( disY > 0)
		{
			GAME_INPUT->InjectKey(GK_PLAYER_DOWN, false);
		}
		else
		{
			GAME_INPUT->InjectKey(GK_PLAYER_UP, false);
		}
	}
	return true;
}

bool GUIPlay::OnControlDrag(const CEGUI::EventArgs& args)
{
	static GameKeyConfig oldState = GK_PLAYER_NONE;
	GameKeyConfig newState = GK_PLAYER_NONE;
	
	CEGUI::Vector2f mousePos = m_controlTabBtn->getGUIContext().getMouseCursor().getPosition();
	CEGUI::Vector2f tabPos = m_controlTabBtn->getPixelPosition();
	CEGUI::Sizef tabSize = m_controlTabBtn->getPixelSize();
	int disX = (int) (mousePos.d_x - tabPos.d_x - tabSize.d_width / 2.0f);
	int disY = (int) (mousePos.d_y - tabPos.d_y - tabSize.d_height / 2.0f);

	int controlPosX, controlPosY;
	CEGUI::Sizef controlSize = (tabSize - m_controlBtn->getPixelSize()) * 0.5f;
	if ( disX <= -controlSize.d_width)
	{
		controlPosX = -(int)controlSize.d_width;
	}
	else if (disX >= controlSize.d_width)
	{
		controlPosX = (int)controlSize.d_width;
	}
	else
	{
		controlPosX = disX;
	}
	if ( disY <= -controlSize.d_height)
	{
		controlPosY = -(int)controlSize.d_height;
	}
	else if (disY >= controlSize.d_height)
	{
		controlPosY = (int)controlSize.d_height;
	}
	else
	{
		controlPosY = disY;
	}
	m_controlBtn->setPosition(CEGUI::UVector2( CEGUI::UDim(0, (float) controlPosX), CEGUI::UDim(0, (float) controlPosY)));

	if ( abs(disX) >= abs(disY))
	{
		if ( disX > 0)
		{
			newState = GK_PLAYER_RIGHT;
		}
		else
		{
			newState = GK_PLAYER_LEFT;
		}
	}
	else
	{
		if ( disY > 0)
		{
			newState = GK_PLAYER_DOWN;
		}
		else
		{
			newState = GK_PLAYER_UP;
		}
	}

	if ( oldState != newState)
	{
		if ( oldState != GK_PLAYER_NONE)
		{
			GAME_INPUT->InjectKey(oldState, false);
		}
		GAME_INPUT->InjectKey(newState, true);
		oldState = newState;
	}
	return true;
}

bool GUIPlay::OnResumeClick(const CEGUI::EventArgs& args)
{
	ResetGUI();
	return true;
}

bool GUIPlay::OnReplayClick(const CEGUI::EventArgs& args)
{
	Level_ReplayLevel();
	//Replay
	return true;
}

bool GUIPlay::OnSoundOnClick(const CEGUI::EventArgs& args)
{
	m_soundOnBtn->setVisible(false);
	m_soundOffBtn->setVisible(true);
	SOUND->PlayMusic("Once We Were Shadows.mp3");
	return true;
}

bool GUIPlay::OnSoundOffClick(const CEGUI::EventArgs& args)
{
	m_soundOnBtn->setVisible(true);
	m_soundOffBtn->setVisible(false);
	SOUND->StopMusic();
	return true;
}

bool GUIPlay::OnExitClick(const CEGUI::EventArgs& args)
{
	//Exit
	SCENE->PopGameState("StatePlay");
	SCENE->SetGameState(new StateMenu());
	return true;
}

GUIPlay::~GUIPlay()
{
}
