#pragma once

#include "cegui/widgets/PushButton.h"

class GUIPlay
{
public:
	GUIPlay();
	~GUIPlay();

	void ResetGUI();

protected:
	CEGUI::PushButton* m_pauseBtn;
	CEGUI::PushButton* m_controlTabBtn;
	CEGUI::PushButton* m_changeBtn;
	CEGUI::PushButton* m_controlBtn;
	CEGUI::PushButton* m_resumeBtn;
	CEGUI::PushButton* m_replayBtn;
	CEGUI::PushButton* m_soundOnBtn;
	CEGUI::PushButton* m_soundOffBtn;
	CEGUI::PushButton* m_exitBtn;
	CEGUI::Window*     m_pauseMenu;

	bool OnPauseClick(const CEGUI::EventArgs& args);
	bool OnChangeClick(const CEGUI::EventArgs& args);
	bool OnControlDown(const CEGUI::EventArgs& args);
	bool OnControlUp(const CEGUI::EventArgs& args);
	bool OnControlDrag(const CEGUI::EventArgs& args);
	bool OnResumeClick(const CEGUI::EventArgs& args);
	bool OnReplayClick(const CEGUI::EventArgs& args);
	bool OnSoundOnClick(const CEGUI::EventArgs& args);
	bool OnSoundOffClick(const CEGUI::EventArgs& args);
	bool OnExitClick(const CEGUI::EventArgs& args);
};

