#pragma once

#include "CoreEngine/StateManager/State.h"
#include "GUI/GUIWelcomeScreen.h"

class StateWS : public State
{
public:
	StateWS() : State("StateWS") {}
	~StateWS() {delete m_gui;}

	virtual void Enter();
	virtual bool Update(float time_elapsed);
	virtual bool OnKeyUp(int keycode);
	virtual void Exit();
	virtual void OnGlobalTouchUp(int id, float x, float y);

protected:
	GUIWelcomeScreen* m_gui;
};
