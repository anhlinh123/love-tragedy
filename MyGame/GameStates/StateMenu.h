#pragma once

#include "CoreEngine/StateManager/State.h"
#include "GUI/GUIMenu.h"

class StateMenu : public State
{
public:
	StateMenu() : State("StateMenu") {}
	~StateMenu() {delete m_gui;}

	virtual void Enter();
	virtual bool Update(float time_elapsed);
	virtual bool OnKeyUp(int keycode);
	virtual void OnGlobalTouchUp(int id, float x, float y);

private:
	GUIMenu* m_gui;
};

