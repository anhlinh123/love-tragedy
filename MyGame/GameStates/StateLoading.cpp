#include "StdAfx.h"
#include "StateLoading.h"
#include "CoreEngine/Controller/Scene.h"
#include "StatePlay.h"
#include "Level/Level.h"
#include "GameStates/StateMenu.h"

#include "CEGUI\GUIContext.h"
#include "CEGUI\SchemeManager.h"

extern int ms_currentLevel;

#ifdef ANDROID
extern "C" void nativeHideAd();
#endif

void StateLoading::Enter()
{

#ifdef ANDROID
	nativeHideAd();
#endif

	m_gui = new GUIWelcomeScreen();
	m_elapsedTime = 0.0f;

		switch (ms_currentLevel)
		{
			case -1:
				{
					m_gui->changeImage("canh_0.png");
					break;
				}
			case 0:
				{
					m_gui->changeImage("canh_3.png");
					break;
				}
			case 1:
				{
					m_gui->changeImage("canh_5.png");
					break;
				}
			case 2:
				{
					m_gui->changeImage("canh_7.png");
					break;
				}
			case 3:
				{
					m_gui->changeImage("canh_10.png");
					break;
				}
		}
}

bool StateLoading::Update(float time_elapsed)
{
		switch (ms_currentLevel)
		{
			case -1:
				{
					if (m_elapsedTime >= 3.0f)
					{
						m_gui->changeImage("canh_1.png");
					}
					if (m_elapsedTime >= 6.0f)
					{
						m_gui->changeImage("canh_2.png");
					}
					if (m_elapsedTime >= 9.0f)
					{
						Level_PlayNextLevel();
					}
					break;
				}
			case 0:
				{
					if (m_elapsedTime >= 3.0f)
					{
						m_gui->changeImage("canh_4.png");
					}
					if (m_elapsedTime >= 6.0f)
					{
						Level_PlayNextLevel();
					}
					break;
				}
			case 1:
				{
					if (m_elapsedTime >= 3.0f)
					{
						
						m_gui->changeImage("canh_6.png");
					}
					if (m_elapsedTime >= 6.0f)
					{
						Level_PlayNextLevel();
					}
					break;
				}
			case 2:
				{
					if (m_elapsedTime >= 3.0f)
					{
						m_gui->changeImage("canh_8.png");
					}
					if (m_elapsedTime >= 6.0f)
					{
						m_gui->changeImage("canh_9.png");
					}
					if (m_elapsedTime >= 9.0f)
					{
						Level_PlayNextLevel();
					}
					break;
				}
			case 3:
				{
					if (m_elapsedTime >= 5.0f)
					{
						m_gui->changeImage("canh_11.png");
					}
					if (m_elapsedTime >= 10.0f)
					{
						SCENE->PopGameState("StateLoading");
						SCENE->SetGameState(new StateMenu());
					}
					break;
				}
		}
	m_elapsedTime += time_elapsed;
	return true;
}

void StateLoading::OnGlobalTouchUp(int id, float x, float y)
{
	if( m_elapsedTime < 3.0f)
	{
		m_elapsedTime = 3.0f;
		return;
	}
		if( m_elapsedTime < 6.0f)
	{
		m_elapsedTime = 6.0f;
		return;
	}
	if( m_elapsedTime < 9.0f)
	{
		m_elapsedTime = 9.0f;
		return;
	}
	if( m_elapsedTime < 12.0f)
	{
		m_elapsedTime = 12.0f;
		return;
	}
}

void StateLoading::Exit()
{
	//Clean up
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( NULL );
}
