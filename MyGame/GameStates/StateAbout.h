#pragma once

#include "CoreEngine/StateManager/State.h"

class StateAbout : public State
{
public:
	StateAbout() : State("StateAbout") {}
	~StateAbout() {}

	virtual void Enter() {}
	virtual bool Update(float time_elapsed) {return true;}
	virtual bool OnKeyUp(int keycode) {return true;}
};

