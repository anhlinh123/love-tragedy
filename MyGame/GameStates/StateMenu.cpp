#include "StdAfx.h"
#include "StateMenu.h"

#include "Sound/SoundManager.h"

#ifdef ANDROID
	extern "C" void nativeLogin();
#endif

void StateMenu::Enter()
{
	m_gui = new GUIMenu();
	SOUND->PlayMusic("Adversity.mp3");
#ifdef ANDROID
		nativeLogin();
#endif
}
bool StateMenu::Update(float time_elapsed)
{
	return false;
}

bool StateMenu::OnKeyUp(int keycode)
{
	return false;
}

void StateMenu::OnGlobalTouchUp(int id, float x, float y)
{
	m_gui->ResetGUI();
}

