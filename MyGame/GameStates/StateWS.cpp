#include "StdAfx.h"
#include "StateWS.h"
#include "CoreEngine/Controller/Scene.h"
#include "CoreEngine/Controller/GameInput.h"
#include "StateMenu.h"

#include "CEGUI\SchemeManager.h"
#include "CEGUI\ImageManager.h"

void StateWS::Enter()
{
	m_gui = new GUIWelcomeScreen();
	m_gui->changeImage("img_welcomescreen.png");
}

bool StateWS::Update(float time_elapsed)
{
	//SCENE->SetGameState(new StateLoading());
	return true;
}

bool StateWS::OnKeyUp(int keycode)
{
	switch (keycode)
	{
		case GK_BUTTON_X:
			{
					SCENE->SetGameState(new StateMenu());
					break;
			}
		default:
			break;
	}
	return true;
}

void StateWS::OnGlobalTouchUp(int id, float x, float y)
{
	SCENE->SetGameState(new StateMenu());
}

void StateWS::Exit()
{
}
