#ifndef _STATE_PLAY_H_
#define _STATE_PLAY_H_

#include "CoreEngine/StateManager/State.h"
#include "Level/Level.h"
#include "GUI/GUIPlay.h"

class StatePlay : public State
{
public:
	StatePlay(const char* levelName) : State("StatePlay"), m_levelName(levelName), m_gui(NULL), m_oldDragx(0.0f), m_touchId(-1) {}
	virtual ~StatePlay() {}

	virtual void Enter();
	virtual bool Update(float time_elapsed);
	virtual bool OnKeyUp(int keycode);
	virtual void OnGlobalTouchDrag(int id, float x, float y);
	virtual void OnGlobalTouchUp(int id, float x, float y);

public:
	Level m_level;
	GUIPlay* m_gui;
	std::string m_levelName;

	// drag values
	float m_oldDragx;
	int   m_touchId;
};

#endif