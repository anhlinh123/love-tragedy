#include "StdAfx.h"
#include "StateOptions.h"

#include "CoreEngine/Controller/Scene.h"
#include "GameStates/StateMenu.h"
#include "GameStates/StatePlay.h"
#include "Level/Level.h"


void StateOptions::Enter()
{
}

bool StateOptions::Update(float time_elapsed)
{
	return false;
}

bool StateOptions::OnKeyUp(int keycode)
{
	switch (keycode)
	{
	case 'A': // back to GUIPlay
		SCENE->PopGameState("StateOptions");
		break;
	case 'B': // back to main menu
		SCENE->PopGameState("StateOptions");
		SCENE->PopGameState("StatePlay");
		SCENE->SetGameState(new StateMenu());
		break;
	case 'C': // replay level
		SCENE->PopGameState("StateOptions");
		Level_ReplayLevel();
		break;
	default:
		break;
	}
	return true;
}