#include "stdafx.h"
#include "CoreEngine/Controller/Scene.h"
#include "CoreEngine/Controller/GameInput.h"
#include "StatePlay.h"
#include "Level/Level.h"
#include "Level/Character.h"
#include "Sound/SoundManager.h"

void StatePlay::Enter()
{
	m_level.LoadLevel(m_levelName.c_str());
	m_gui = new GUIPlay();
	SOUND->PlayMusic("Once We Were Shadows.mp3");
}

bool StatePlay::Update(float time_elapsed)
{
	if (!m_level.IsLevelEnding())
	{
		Character* character = m_level.GetActiveCharacter();

		// if player is not moving, check for new move
		if ( !m_level.IsCharacterMoving() )
		{
			SCENE->GetCamera()->ComputeBasis(character->m_face, character->m_up);

			if (GAME_INPUT->m_deltaPlayerLR > 0.0f)
				m_level.MoveCharacter(SCENE->GetCamera()->m_leftBasis);
			else if (GAME_INPUT->m_deltaPlayerLR < 0.0f)
				m_level.MoveCharacter(SCENE->GetCamera()->m_rightBasis);
			else if (GAME_INPUT->m_deltaPlayerFB > 0.0f)
				m_level.MoveCharacter(SCENE->GetCamera()->m_forwardBasis);
			else if (GAME_INPUT->m_deltaPlayerFB < 0.0f)
				m_level.MoveCharacter(SCENE->GetCamera()->m_backwardBasis);
		}
	}
	m_level.ProcessCommands(time_elapsed);
	return true;
}

bool StatePlay::OnKeyUp(int keycode)
{
	if (m_level.IsLevelEnding())
	{
		return true;
	}

	switch (keycode)
	{
	case GK_SWITCH_CAMERA_MODE:
		//{
		//	SCENE->GetCamera()->ChangeToNextMode();
		//	if (SCENE->GetCamera()->m_mode == Camera::CAMERA_LOCK_BEHIND)
		//	{
		//		SCENE->GetCamera()->SetBehindCharacter(	m_level.GetActiveCharacter()->m_pos + m_level.GetActiveCharacter()->m_up, 
		//									m_level.GetActiveCharacter()->m_face, m_level.GetActiveCharacter()->m_up, Vector2(8.0f, 4.0f));
		//	}
		//}
		break;
	case GK_BUTTON_X:
		if ( !m_level.IsLevelEnding() ) //Hard code so you can only switch at level 2
			m_level.SwitchCharacter();
		break;
	default:
		break;
	}
	return true;
}

void StatePlay::OnGlobalTouchDrag(int id, float x, float y)
{
	if (m_level.IsLevelEnding()
		|| (m_touchId != -1 && m_touchId != id))
	{
		return;
	}

	if (m_touchId == -1) // means it is not initialized yet
	{
		m_touchId = id;
		m_oldDragx = x;
		return;
	}

	float deltaDrag = (m_oldDragx - x) * 0.003f; // calibre value
	m_oldDragx = x;

	Character* character = m_level.GetActiveCharacter();
	// Camera look at character
	if (SCENE->GetCamera()->m_mode == Camera::CAMERA_LOOK_AT_CHARACTER)
		SCENE->GetCamera()->RotateAroundTarget(character->m_up * deltaDrag);
}

void StatePlay::OnGlobalTouchUp(int id, float x, float y)
{
	if (m_level.IsLevelEnding() || m_touchId != id)
	{
		return;
	}

	m_touchId = -1; // reset the old touch
	m_gui->ResetGUI();
}