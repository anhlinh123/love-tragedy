#pragma once

#include "CoreEngine/StateManager/State.h"
#include "GUI/GUIWelcomeScreen.h"

/** We need this state because we should put all
 *  heavy initializations here and try to save 
 *  the work for later use. Currently, loading 
 *  coreengine's table and GUI is way too heavy.
 *  The reasonable process might be:
 *  => Display just an image in WS + load GUI for StateLoading => switch to StateLoading
 *  => Display progress bar + load all heavy stuff => switch to main menu
 */
class StateLoading : public State
{
public:
	StateLoading() : State("StateLoading") {}
	~StateLoading() {delete m_gui;}

	virtual void Enter();
	virtual bool Update(float time_elapsed);
	//virtual bool OnKeyUp(int keycode);
	virtual void OnGlobalTouchUp(int id, float x, float y);
	virtual void Exit();

protected:
	GUIWelcomeScreen* m_gui;
	float m_elapsedTime;
};
