#pragma once

#include "CoreEngine/StateManager/State.h"

class StateOptions : public State
{
public:
	StateOptions() : State("StateOptions") {}
	~StateOptions() {}

	virtual void Enter();
	virtual bool Update(float time_elapsed);
	virtual bool OnKeyUp(int keycode);
};
