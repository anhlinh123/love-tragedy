#include "stdafx.h"
#include "CoreEngine/Object/Camera.h"
#include "CoreEngine/Controller/Scene.h"
#include "GameStates/StatePlay.h"
#include "GameStates/StateWS.h"

#include "CEGUI\System.h"
#include "CEGUI\RendererModules\OpenGL\GL3Renderer.h"
#include "CEGUI\DefaultResourceProvider.h"
#include "CEGUI\ImageManager.h"
#include "CEGUI\Font.h"
#include "CEGUI\Scheme.h"
#include "CEGUI\falagard\WidgetLookManager.h"
#include "CEGUI\SchemeManager.h"
#include "CEGUI\WindowManager.h"

#include "Sound\SoundManager.h"

void GameInit()
{
	// create camera
	Camera* camera = new Camera();
	camera->SetProjection(0.1f, FAR_PLANE, FOV, ASPECT_RATIO);
	camera->Initialize(VECTOR3_IDEN, VECTOR3_ZERO, Vector3(10.0f, 10.0f, 10.0f), Vector3(1.0f, 1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f));
	camera->SetActive();
	SCENE->SetCamera(camera);

	// push the first state
	SCENE->PushGameState(new StateWS());
	SOUND->Initialize();
}

void GUIInit()
{
	CEGUI::OpenGL3Renderer& myRenderer = CEGUI::OpenGL3Renderer::bootstrapSystem();

	// initialise the required dirs for the DefaultResourceProvider
	CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*> (CEGUI::System::getSingleton().getResourceProvider());
#ifdef WIN32
	rp->setResourceGroupDirectory("schemes", "../Data/GUI/schemes/");
	rp->setResourceGroupDirectory("imagesets", "../Data/GUI//imagesets/");
	rp->setResourceGroupDirectory("fonts", "../Data/GUI//fonts/");
	rp->setResourceGroupDirectory("layouts", "../Data/GUI//layouts/");
	rp->setResourceGroupDirectory("looknfeels", "../Data/GUI/looknfeel/");
#else
	rp->setResourceGroupDirectory("schemes", "sdcard/Data/GUI/schemes/");
	rp->setResourceGroupDirectory("imagesets", "sdcard/Data/GUI//imagesets/");
	rp->setResourceGroupDirectory("fonts", "sdcard/Data/GUI//fonts/");
	rp->setResourceGroupDirectory("layouts", "sdcard/Data/GUI//layouts/");
	rp->setResourceGroupDirectory("looknfeels", "sdcard/Data/GUI/looknfeel/");
#endif
	// set the default resource groups to be used
	CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
	CEGUI::Font::setDefaultResourceGroup("fonts");
	CEGUI::Scheme::setDefaultResourceGroup("schemes");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
	CEGUI::WindowManager::setDefaultResourceGroup("layouts");

	SCENE->SetGUIContext(&CEGUI::System::getSingleton().getDefaultGUIContext());
}