// CoreEngine.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Controller/Scene.h"
#include "Controller/GameInput.h"
#include "Object/Renderer.h"
#include "Object/Model.h"
#include "Object/Camera.h"
#include "Object/Shader.h"

#include "CEGUI/System.h"

#include "Utilities/utilities.h" // if you use STL, please include this line AFTER all other include
#include "Utilities/Config.h"
#include "Globals.h"
#include <time.h>

//Model* g_uvDisplacement;
//Model* g_envReflect;
//Model* g_model;
//Model* g_sky;
//Camera* g_camera;
//Model* g_sexygirl;
extern void GameInit();
extern void GUIInit();

int Init ( ESContext *esContext )
{
	//g_model = CreateModel("Sample.json");
	//g_sky = CreateModel("Sky.json");
	//g_envReflect = CreateModel("EnvReflect.json");
	//g_uvDisplacement = CreateModel("UvDisplacement.json");
	//g_sexygirl = CreateModel("sexygirl.json");
	//function from MyGame project
	
	GUIInit();
	GameInit();

	//g_camera = new Camera();
	//g_camera->SetProjection(0.1f, 10000.0f, 0.79f, 1.0f);
	//g_camera->Initialize(Vector3(0,5,10), Vector3(0,0,0), Vector3(10,10,10), Vector3(1,1,1));
	//g_camera->SetActive();
	glClearColor ( 0.15f, 0.15f, 0.15f, 1.0f );
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	return 0;
}

void Draw ( ESContext *esContext )
{
	DoDraw();
	CEGUI::System::getSingleton().renderAllGUIContexts();
	eglSwapBuffers ( esContext->eglDisplay, esContext->eglSurface );
}

void Update ( ESContext *esContext, float deltaTime )
{
	//g_sexygirl->Update(deltaTime);
	//g_sexygirl->Draw();
	//g_camera->Update(deltaTime);
	//g_sky->Draw();
	//g_envReflect->m_geometryChunk->SetCustomMatrix(0, g_envReflect->m_geometryChunk->m_matWorld);
	//g_envReflect->m_geometryChunk->SetCustomVector(0, g_camera->m_position);
	//g_envReflect->Draw();
	//g_model->Draw();
	//float tick = (float) clock();
	//g_uvDisplacement->m_geometryChunk->SetCustomVector(0, Vector3(tick/2000.0f, 0.0f, 0.0f));
	//g_uvDisplacement->Draw();
	GAME_INPUT->Update(deltaTime);
	SCENE->Update(deltaTime);
	SCENE->Draw();
}

void Key ( ESContext *esContext, unsigned char key, bool bIsPressed)
{
	GAME_INPUT->OnKey(key, bIsPressed);
}

void Touch ( ESContext *esContext, int type, int x, int y, int id)
{
	GAME_INPUT->OnTouch(type, x, y, id);
}

void CleanUp()
{
	ClearModelTable();
	ClearShaderTable();
	ClearTextureTable();
	ClearMeshTable();
	ClearRenderer();
	//delete g_camera;
	//g_camera = NULL;
	DESTROY_SCENE
	DESTROY_GAME_INPUT
	//delete g_sexygirl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	ESContext esContext;

	esInitContext ( &esContext );

	esCreateWindow ( &esContext, "CoreEngine", Globals::screenWidth, Globals::screenHeight, ES_WINDOW_RGB | ES_WINDOW_DEPTH);

	if ( Init ( &esContext ) != 0 )
		return 0;

	esRegisterDrawFunc ( &esContext, Draw );
	esRegisterUpdateFunc ( &esContext, Update );
	esRegisterKeyFunc ( &esContext, Key);
	esRegisterTouchFunc ( &esContext, Touch);

	esMainLoop ( &esContext );

	//releasing OpenGL resources
	CleanUp();

	//identifying memory leaks
	#ifdef ENABLE_MEMORY_CHECK
	MemoryDump();
	printf("Press any key...\n");
	_getch();
	#endif

	return 0;
}