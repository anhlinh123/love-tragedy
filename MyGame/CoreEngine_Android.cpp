#include "stdafx.h"
#include "Controller/Scene.h"
#include "Controller/GameInput.h"
#include "Object/Renderer.h"
#include "Object/Model.h"
#include "Object/Camera.h"
#include "Object/Shader.h"

#include "CEGUI/System.h"

#include "Utilities/utilities.h"
#include "Globals.h"
#include <time.h>

extern void GameInit();
extern void GUIInit();

int Init ()
{
	GUIInit();
	GameInit();

	glClearColor ( 1.0f, 1.0f, 1.0f, 1.0f );
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	return 0;
}

void Draw ()
{
	DoDraw();
	CEGUI::System::getSingleton().renderAllGUIContexts();
}

void Update ( float deltaTime )
{
	GAME_INPUT->Update(deltaTime);
	SCENE->Update(deltaTime);
	SCENE->Draw();
}

void Key ( unsigned char key, bool bIsPressed)
{
	GAME_INPUT->OnKey(key, bIsPressed);
}

void Touch ( int type, int x, int y, int id)
{
	GAME_INPUT->OnTouch(type, x, y, id);
}

void CleanUp()
{
	ClearModelTable();
	ClearShaderTable();
	ClearTextureTable();
	ClearMeshTable();
	ClearRenderer();
	DESTROY_SCENE
	DESTROY_GAME_INPUT
}