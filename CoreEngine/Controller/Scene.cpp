#include "stdafx.h"
#include "Scene.h"
#include "Utilities/utilities.h" // if you use STL, please include this line AFTER all other include
#include "Jsoncpp/json/json.h"


//
Scene * Scene::ms_pInstance = NULL;

Scene * SCENE 
{
	if ( ms_pInstance == NULL )
		ms_pInstance = new Scene();
	return ms_pInstance;
}

void Scene::DestroyInstance() 
{
	if ( ms_pInstance )
	{
		ms_pInstance->CleanUp();
		delete ms_pInstance;
		ms_pInstance = NULL;
	}
}

void Scene::Update(float deltaTime )
{
	m_camera->Update(deltaTime);

	if (m_stateStack.GetCurrentState())
		m_stateStack.GetCurrentState()->Update(deltaTime);

	// Update the objects, this has to be called after current state is updated
	// to delete all the pending-delete objects before call draw.
	for (ObjectVector::iterator it = m_objects.begin(); it != m_objects.end(); )
	{
		if ( (*it)->IsDeletePending() )
		{
			delete (*it);
			it = m_objects.erase(it);
		}
		else
		{
			(*it)->Update(deltaTime);
			it++;
		}
	}
}

void Scene::Draw()
{
	for (ObjectVector::iterator it = m_objects.begin(); it != m_objects.end(); it++)
	{
		if ( !(*it)->IsDeletePending() )
		{
			(*it)->Draw();
		}
	}
}

void Scene::AddObject(Object* obj)
{
	m_objects.push_back(obj);
}

void Scene::CleanUp()
{
	for (ObjectVector::iterator it = m_objects.begin(); it != m_objects.end(); it++)
	{
		delete (*it);
	}
	m_stateStack.Clear();
	if (m_camera)
		delete m_camera;
}

void Scene::OnKeyUp(int keycode)
{
	if (m_stateStack.GetCurrentState())
		m_stateStack.GetCurrentState()->OnKeyUp(keycode);
}

void Scene::Clear()
{
	for (ObjectVector::iterator it = m_objects.begin(); it != m_objects.end(); it++)
	{
		(*it)->SetVisible(false);
		(*it)->MarkAsDelete();
	}
}

void Scene::OnTouchUp(int id, int x, int y)
{
	if (m_guiTouchId == id)
	{
		m_GUIContext->injectMousePosition((float)x, (float)y);
		m_GUIContext->injectMouseButtonUp(CEGUI::LeftButton);
		m_guiTouchId = -1;
	}
	else
	{
		if (m_stateStack.GetCurrentState())
			m_stateStack.GetCurrentState()->OnGlobalTouchUp(id, (float)x, (float)y);
	}
}

void Scene::OnTouchDown(int id, int x, int y)
{
	if (m_guiTouchId != -1)
	{
		if (m_stateStack.GetCurrentState())
			m_stateStack.GetCurrentState()->OnGlobalTouchDown(id, (float)x, (float)y);
	}
	else
	{
		m_GUIContext->injectMousePosition((float)x, (float)y);
		if (!m_GUIContext->injectMouseButtonDown(CEGUI::LeftButton))
		{
			if (m_stateStack.GetCurrentState())
				m_stateStack.GetCurrentState()->OnGlobalTouchDown(id, (float)x, (float)y);
		}
		else if (m_guiTouchId == -1)
		{
			m_guiTouchId = id;
		}
	}
	
}

void Scene::OnTouchDrag(int id, int x, int y)
{
	if (m_guiTouchId == id)
	{
		m_GUIContext->injectMousePosition((float)x, (float)y);
	}
	else
	{
		if (m_stateStack.GetCurrentState())
			m_stateStack.GetCurrentState()->OnGlobalTouchDrag(id, (float)x, (float)y);
	}
}

void Scene::SetGUIContext(CEGUI::GUIContext* context)
{
	m_GUIContext = context;
}