#include "stdafx.h"
#include <math.h>
#include <mutex>
#include "GameInput.h"
#include "Scene.h"


//
GameInput * GameInput::ms_pInstance = NULL;
std::mutex g_mtx;

GameInput * GAME_INPUT 
{
	if ( ms_pInstance == NULL )
	{
		ms_pInstance = new GameInput();
		ms_pInstance->Reset();
	}
	return ms_pInstance;
}

void GameInput::DestroyInstance() 
{
	if ( ms_pInstance )
	{
		delete ms_pInstance;
		ms_pInstance = NULL;
	}
}

void GameInput::Reset()
{
	m_deltaCameraLR = m_deltaCameraFB = m_deltaCameraPitch = m_deltaCameraYaw = 0.0f;
	m_deltaPlayerLR = m_deltaPlayerFB = 0.0f;
	m_deltaCameraZoom = 0.0f;
}

void GameInput::Update(float deltaTime)
{
	g_mtx.lock();
	// touches input handling...
	TouchesList touchesTemp = m_touches;
	m_touches.clear();
	g_mtx.unlock();

	for (unsigned int i = 0; i < touchesTemp.size(); i++)
	{
		TouchEvent& touch = touchesTemp[i];
		switch(touch.touchtype)
		{
			case TOUCH_DOWN:
				HandleTouchDown(touch.id, touch.x, touch.y);
				break;
			case TOUCH_UP:
				HandleTouchUp(touch.id, touch.x, touch.y);
				break;
			case TOUCH_MOVE:
				HandleTouchDrag(touch.id, touch.x, touch.y);
				break;
			case TOUCH_PINCH:
				HandlePinch(touch.id, touch.x, touch.y, touch.distance, touch.prevdistance);
				break;
			default:
				//HandleGesture(touch.touchtype - TOUCH_MAX, touch.x, touch.y);
				break;
		}
	}

	g_mtx.lock();
	// key input handling...
	KeyList keysTemp = m_keys;
	m_keys.clear();
	g_mtx.unlock();

	for (unsigned int i = 0; i < keysTemp.size() ; i++)
	{
		KeyEvent& keyevent = keysTemp[i];
		switch(keyevent.keytype)
		{
		case KEY_DOWN:
			HandleKeyDown(keyevent.keycode, deltaTime);
			break;
		case KEY_UP:
			HandleKeyUp(keyevent.keycode, deltaTime);
			break;
		}
	}
}

void GameInput::OnKey(unsigned char key, bool bIsPressed)
{
	g_mtx.lock();
	m_keys.push_back( KeyEvent(bIsPressed? KEY_DOWN:KEY_UP, key) );
	g_mtx.unlock();
}

void GameInput::InjectKey(unsigned char key, bool bIsPressed)
{
	g_mtx.lock();
	m_keys.push_back( KeyEvent(bIsPressed? KEY_DOWN:KEY_UP, key) );
	g_mtx.unlock();
}


//
// references : http://android-developers.blogspot.com/2010/06/making-sense-of-multitouch.html
void GameInput::OnTouch(int action, int x, int y, int id)
{
	g_mtx.lock();
	m_touches.push_back( TouchEvent((TouchEventType) action, id, x, y));
	/*
	// touches...
	struct Touch
	{
	  int x;
	  int y;
	};

	static const int	CE_MAX_TOUCHES = 10;
	static Touch		touches[CE_MAX_TOUCHES];
	static int			touchCount;
	static float		oldpinch = 0;

	switch(action)
	{
	case 0: //Touch up
		for (int i = id; i < touchCount - 1 && i < CE_MAX_TOUCHES-1; ++i)
		{
			touches[i].x = touches[i+1].x;
			touches[i].y = touches[i+1].y;
		}
		--touchCount;
		if (touchCount == 0)
		{
			m_touches.push_back( TouchEvent(TOUCH_UP, id, x, y));
		}
		else if (touchCount == 1)
		{
			m_touches.push_back( TouchEvent(TOUCH_DOWN, id, touches[0].x, touches[0].y));
		}
		else if (touchCount == 2)
		{
			touches[id].x = x;
			touches[id].y = y;
			float diffx = (float)(touches[0].x - touches[1].x);
			float diffy = (float)(touches[0].y - touches[1].y);
			float dist = sqrt(diffx*diffx + diffy*diffy);
			oldpinch = dist;
		}
		else if (touchCount < 0)
		{
			touchCount = 0;
		}
		break;
	case 1: //Touch down
		touches[touchCount].x = x;
		touches[touchCount].y = y;
		++touchCount;

		if (touchCount == 1)
		{
			m_touches.push_back( TouchEvent(TOUCH_DOWN, id, x, y));
		}
		else if (touchCount == 2)
		{
			touches[id].x = x;
			touches[id].y = y;
			float diffx = (float)(touches[0].x - touches[1].x);
			float diffy = (float)(touches[0].y - touches[1].y);
			float dist = sqrt(diffx*diffx + diffy*diffy);
			oldpinch = dist;
		}
		break;
	case 2: //Touch move
		touches[id].x = x;
		touches[id].y = y;
		if (touchCount == 1)
		{
			m_touches.push_back( TouchEvent(TOUCH_MOVE, id, x, y));
		}
		else if (touchCount == 2)
		{
			touches[id].x = x;
			touches[id].y = y;
			float diffx = (float)(touches[0].x - touches[1].x);
			float diffy = (float)(touches[0].y - touches[1].y);
			float dist = sqrt(diffx*diffx + diffy*diffy);
			float midx = (float)(touches[0].x - (diffx * 0.5f));
			float midy = (float)(touches[0].y - (diffy * 0.5f));

			m_touches.push_back( TouchEvent(TOUCH_PINCH, id, midx, midy, dist, oldpinch));
			oldpinch = dist;
		}
		break;
	}
	*/
	g_mtx.unlock();
}

void GameInput::HandleTouchDown(const int id, const int x, const int y)
{
	SCENE->OnTouchDown(id, x, y);
}

void GameInput::HandleTouchUp(const int id, const int x, const int y)
{
	SCENE->OnTouchUp(id, x, y);
}

void GameInput::HandleTouchDrag(const int id, const int x, const int y)
{
	SCENE->OnTouchDrag(id, x, y);
}

void GameInput::HandlePinch(const int id, int x, int y, int distance, int olddistance)
{
}

void GameInput::HandleKeyDown(const unsigned char key, float deltaTime)
{
	// handle default control key
	switch (key) 
	{
			// Camera movement control
		case GK_CAMERA_MOVE_LEFT:
			m_deltaCameraLR = -deltaTime;
			break;
		case GK_CAMERA_MOVE_RIGHT: 
			m_deltaCameraLR = deltaTime;
			break;
		case GK_CAMERA_FORWARD: 
			m_deltaCameraFB = -deltaTime;
			break;
		case GK_CAMERA_BACKWARD: 
			m_deltaCameraFB = deltaTime;
			break;
			// Camera rotate control
		case GK_CAMERA_ROTATE_LEFT:
			m_deltaCameraYaw = -deltaTime;
			break;
		case GK_CAMERA_ROTATE_RIGHT:
			m_deltaCameraYaw = deltaTime;
			break;
		case GK_CAMERA_ROTATE_UP:
			m_deltaCameraPitch = -deltaTime;
			break;
		case GK_CAMERA_ROTATE_DOWN:
			m_deltaCameraPitch = deltaTime;
			break;
			// Camera zoom control
		case GK_CAMERA_ZOOM_IN: 
			m_deltaCameraZoom = -deltaTime;
			break;
		case GK_CAMERA_ZOOM_OUT: 
			m_deltaCameraZoom = deltaTime;
			break;

			// Player movement control
		case GK_PLAYER_UP:
			m_deltaPlayerFB = deltaTime;
			break;
		case GK_PLAYER_DOWN: 
			m_deltaPlayerFB = -deltaTime;
			break;
		case GK_PLAYER_LEFT: 
			m_deltaPlayerLR = deltaTime;
			break;
		case GK_PLAYER_RIGHT: 
			m_deltaPlayerLR = -deltaTime;
			break;

		default:
			break;
	}
}

void GameInput::HandleKeyUp(const unsigned char key, float deltaTime)
{
	// handle default control key
	switch (key) 
	{
			// Camera movement control
		case GK_CAMERA_MOVE_LEFT:
		case GK_CAMERA_MOVE_RIGHT: 
			m_deltaCameraLR = 0.0f;
			break;
		case GK_CAMERA_FORWARD: 
		case GK_CAMERA_BACKWARD: 
			m_deltaCameraFB = 0.0f;
			break;
			// Camera rotate control
		case GK_CAMERA_ROTATE_LEFT:
		case GK_CAMERA_ROTATE_RIGHT:
			m_deltaCameraYaw = 0.0f;
			break;
		case GK_CAMERA_ROTATE_UP:
		case GK_CAMERA_ROTATE_DOWN:
			m_deltaCameraPitch = 0.0f;
			break;
			// Camera zoom control
		case GK_CAMERA_ZOOM_IN: 
		case GK_CAMERA_ZOOM_OUT: 
			m_deltaCameraZoom = 0.0f;
			break;

			// Player movement control
		case GK_PLAYER_UP:
		case GK_PLAYER_DOWN: 
			m_deltaPlayerFB = 0.0f;
			break;
		case GK_PLAYER_LEFT: 
		case GK_PLAYER_RIGHT: 
			m_deltaPlayerLR = 0.0f;
			break;

		default:
			break;
	}

	// pass this key to SCENE
	SCENE->OnKeyUp((int)key);
}