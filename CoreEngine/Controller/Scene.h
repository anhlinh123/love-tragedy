#ifndef _SCENE_H_
#define _SCENE_H_

#include "Object/Object.h"
#include "Object/Camera.h"
#include "StateManager/StateStack.h"
#include "cegui/GUIContext.h"

class Scene
{
protected:
	Scene (): m_guiTouchId(-1) {}
	~Scene () {}
	/*implicit methods exist for the copy constructor and operator= and we want to forbid calling them.*/
	Scene (const Scene &){} 
	Scene& operator = (const Scene &){} 

public:
	static Scene * GetInstance ();
	static void DestroyInstance ();

	void Draw();
	void Update(float deltaTime );
	void CleanUp();
	void Clear();
	void AddObject(Object* obj);
	void SetGUIContext(CEGUI::GUIContext* context);
	void SetCamera(Camera* camera) { m_camera = camera; }
	Camera* GetCamera() { return m_camera; }

	void OnKeyUp(int keycode);
	void OnTouchDown(int id, int x, int y);
	void OnTouchUp(int id, int x, int y);
	void OnTouchDrag(int id, int x, int y);
	
	//
	void PushGameState(State* state) { m_stateStack.PushState(state); }
	void PopGameState(const char* expectedState) { m_stateStack.PopState(expectedState); }
	void SetGameState(State* state) { m_stateStack.SetState(state, ""); }
	State* GetCurrentState() { return m_stateStack.GetCurrentState(); }

protected:
	static Scene * ms_pInstance;
	
	// Objects in the scene
	ObjectVector m_objects;
	Camera* m_camera;
	StateStack m_stateStack;

	// GUI objects (Is this OK to add this here???)
	CEGUI::GUIContext* m_GUIContext;
	int m_guiTouchId;
};

#define SCENE Scene::GetInstance()
#define DESTROY_SCENE Scene::DestroyInstance();

#endif