#ifndef _GAME_INPUT_H_
#define _GAME_INPUT_H_

#include <vector>

// key defines...
enum GameKeyConfig {
	GK_PLAYER_NONE			     ,
	GK_PLAYER_UP			= 'W',
	GK_PLAYER_DOWN			= 'S',
	GK_PLAYER_LEFT			= 'A',
	GK_PLAYER_RIGHT			= 'D',
	GK_CAMERA_FORWARD		= 38,	//UP_KEY
	GK_CAMERA_BACKWARD		= 40,	//DOWN_KEY
	GK_CAMERA_MOVE_LEFT		= 37,	//LEFT_KEY
	GK_CAMERA_MOVE_RIGHT	= 39,	//RIGHT_KEY
	GK_CAMERA_ROTATE_UP		= 'U',
	GK_CAMERA_ROTATE_DOWN	= 'J',
	GK_CAMERA_ROTATE_LEFT	= 'H',
	GK_CAMERA_ROTATE_RIGHT	= 'K',
	GK_CAMERA_ZOOM_IN		= 'I',
	GK_CAMERA_ZOOM_OUT		= 'O',
	GK_SWITCH_CAMERA_MODE	= 9,	//TAB_KEY
	GK_BUTTON_X				= 13,	//ENTER_KEY
	GK_BUTTON_Y				= 32,	//SPACEBAR_KEY
	GK_BUTTON_A				= 15,
	GK_BUTTON_B				= 16
};

enum KeyEventType
{
	KEY_DOWN,
	KEY_UP,
	KEY_MAX
};

struct KeyEvent
{
	KeyEvent(KeyEventType type, unsigned char code) : keytype(type), keycode(code) {} 
	KeyEventType keytype;
	unsigned char keycode;
};

typedef std::vector<KeyEvent> KeyList;
	
// touch defines...
enum TouchEventType
{
	TOUCH_DOWN,
	TOUCH_UP,
	TOUCH_MOVE,
	TOUCH_PINCH,
	TOUCH_MAX
};

struct TouchEvent
{
	TouchEvent(TouchEventType type, int _id, float a, float b, float dist = 0.0f, float olddist = 0.0f)
		: touchtype(type), id(_id), x(a), y(b), distance(dist), prevdistance(olddist) {}

	TouchEventType touchtype;
	int id;
	float x;
	float y;
	float distance;
	float prevdistance;
};

typedef std::vector<TouchEvent> TouchesList;



class GameInput
{
protected:
	GameInput () {}
	~GameInput () {}
	/*implicit methods exist for the copy constructor and operator= and we want to forbid calling them.*/
	GameInput (const GameInput &){} 
	GameInput& operator = (const GameInput &){} 

public:
	static GameInput * GetInstance ();
	static void DestroyInstance ();

	void Update(float deltaTime);
	void Reset();

	// handle key input
	void OnKey(unsigned char key, bool bIsPressed);
	void InjectKey(unsigned char key, bool bIsPressed);

	void HandleKeyDown(const unsigned char key, float deltaTime);
	void HandleKeyUp(const unsigned char key, float deltaTime);

	KeyList m_keys;

	// default control key indicators
	float	m_deltaCameraLR, m_deltaCameraFB, m_deltaCameraPitch, m_deltaCameraYaw,
			m_deltaCameraZoom;

	float	m_deltaPlayerLR, m_deltaPlayerFB;

	// handle touches input
	void OnTouch(int action, int x, int y, int id);

	void HandleTouchDown(const int id, const int x, const int y);
	void HandleTouchUp(const int id, const int x, const int y);
	void HandleTouchDrag(const int id, const int x, const int y);
	void HandlePinch(const int id, int x, int y, int distance, int olddistance);

	TouchesList m_touches;

protected:
	static GameInput * ms_pInstance;
};

#define GAME_INPUT GameInput::GetInstance()
#define DESTROY_GAME_INPUT GameInput::DestroyInstance();

#endif