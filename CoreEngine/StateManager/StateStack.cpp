#include "stdafx.h"
#include "StateStack.h"


StateStack::StateStack() 
{
	//m_states.resize(MAXSTATES);
	//m_garbage.resize(MAXSTATES);

	m_pStateChangedCallbackCallBack = NULL;
}

StateStack::~StateStack()
{
}

void StateStack::SetInitialState(State* state)
{
	m_states.push_back(state);
}

void StateStack::Clear()
{
	while (m_states.size())
	{
		m_states.back()->Exit();
		//SCENE->CleanupRemovedObjects();
		delete m_states.back();
		m_states.pop_back();
	}

	while ( m_garbage.size() > 0 )
	{
		delete (m_garbage.back());
		m_garbage.pop_back();
	}
}

void StateStack::PushState(State* newstate)
{
	if (m_states.size())
	{
		m_states.back()->Pause();
		//SCENE->CleanupRemovedObjects();
	}

	m_states.push_back(newstate);
	newstate->Enter();

	if( m_pStateChangedCallbackCallBack )
	{
		m_pStateChangedCallbackCallBack(newstate->GetStateName(), StateActivationType_CREATED);
	}
}

void StateStack::PopState(const char* expectedStateName)
{
	if( !GetCurrentState()->IsState( expectedStateName ) )
	{
		//  Prevent bacchanalia
		//return;
	}

	if (m_states.size())
	{
		State* garbage = m_states.back();
		garbage->Exit();
		//SCENE->CleanupRemovedObjects();
		m_garbage.push_back( garbage );

		m_states.pop_back();

		if (m_states.size())
		{
			m_states.back()->Resume();
		}

		if( m_states.size() && m_pStateChangedCallbackCallBack ) // Fixed bug 7066758
		{
			m_pStateChangedCallbackCallBack(m_states.back()->GetStateName(), StateActivationType_RESUMED);
		}
	}
}

void StateStack::SetState(State* newstate, const char* expectedStateName)
{
	//if( !GetCurrentState()->IsState( expectedStateName ) )
	//{
		//  Prevent bacchanalia
	//       m_states.push_back(newstate);
	//       newstate->Enter();
		//      return;
	//}

	if (m_states.size())
	{
		State* garbage = m_states.back();
		garbage->Exit();
		//SCENE->CleanupRemovedObjects();
		m_garbage.push_back( garbage );

		m_states.pop_back();
	}

	m_states.push_back(newstate);
	newstate->Enter();

	if( m_pStateChangedCallbackCallBack )
	{
		m_pStateChangedCallbackCallBack(newstate->GetStateName(), StateActivationType_CREATED);
	}
}

State* StateStack::GetCurrentState()
{
	if (m_states.size() == 0)
		return NULL;

	return m_states.back();
}

State* StateStack::FindState(const char* stateName)
{
	for (unsigned int i = 0; i < m_states.size(); ++i)
	{
		if (m_states[i]->IsState(stateName))
		{
			return m_states[i];
		}
	}

	return NULL;
}

bool StateStack::ContainsState(State* state) const
{
	for (unsigned int i = 0; i < m_states.size(); ++i)
	{
		if (m_states[i] == state)
		{
			return true;
		}
	}

	return false;
}

void StateStack::CleanupGarbage()
{
	while ( m_garbage.size() > 0 )
	{
		delete (m_garbage.back());
		m_garbage.pop_back();
	}
}
