#ifndef _STATE_H_
#define _STATE_H_

#include <string>


class State
{
protected:
	std::string  m_name;
	
public:

	State(const char* name) : m_name(name) {}
	virtual ~State() {}

	virtual void Enter() {}
	virtual void Exit();
	virtual void Pause();
	virtual void Resume();
	virtual bool Update(float time_elapsed) = 0;

	bool IsState(const char* statename) const { return m_name == statename; }
	const char* GetStateName() const { return m_name.c_str(); }
	virtual void OnGlobalTouchDown(int id, float x, float y) {}
	virtual void OnGlobalTouchUp(int id, float x, float y) {}
	virtual void OnGlobalTouchDrag(int id, float x, float y) {}
	virtual bool OnGesture(unsigned int gestureid, float x, float y) { return false; }
	virtual bool OnGuiEvent(const char* eventtag) { return false; }

	virtual bool OnPinch(float x, float y, float distance, float prevdistance) { return false; }
	virtual bool OnTouchCancel(float x, float y) { return false; }
	virtual bool OnPinchCancel() {return false; }
	virtual bool OnKeyDown(int keycode) { return false; }

	virtual bool OnKeyUp(int keycode) { return false; }
	virtual void OnGyroscopeUpdate( float rotation_x, float rotation_y, float rotation_z ) {}
	
	virtual void Minimise();
	virtual void Maximise();
};

#endif
