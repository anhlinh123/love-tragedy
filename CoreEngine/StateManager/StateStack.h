#ifndef _STATESTACK_H_
#define _STATESTACK_H_

#include <vector>
#include "State.h"

#define MAXSTATES 8


class StateStack
{
	std::vector<State*> m_states;
	std::vector<State*> m_garbage;

public:
	StateStack();
	virtual ~StateStack();

	void Clear();
	void PushState(State* newstate);
	void PopState(const char* expectedStateName);
	void SetState(State* newstate, const char* expectedStateName);

	State* GetCurrentState();
	State* FindState(const char* stateName);
	bool ContainsState(State* state) const;

	//CallBacks 
	enum StateActivationType
	{
		StateActivationType_CREATED,
		StateActivationType_RESUMED,
	};
	typedef void (*StateChangeEvent)(const char* activeStateName, StateActivationType type);
	void SetStateChangedCallback(StateChangeEvent a_pCallBack) { m_pStateChangedCallbackCallBack = a_pCallBack;}


protected:
	void SetInitialState(State* state);
	void CleanupGarbage();

	StateChangeEvent     m_pStateChangedCallbackCallBack;
};

#endif

