#pragma once

class SoundManager
{
protected:
	SoundManager() {}
	~SoundManager() {}

	/*implicit methods exist for the copy constructor and operator= and we want to forbid calling them.*/
	SoundManager (const SoundManager &){} 
	SoundManager& operator = (const SoundManager &){} 

public:
	static SoundManager * GetInstance ();
	static void DestroyInstance ();

	void Initialize();
	void PlaySound(const char* name);
	void PlayMusic(const char* name);
	void Stop(const char* name);
	void Pause(const char* name);
	void Resume(const char* name);
	void StopAll();
	void StopMusic();
	void PauseAll();
	void ResumeAll();
	void SetVolume(float fVolume);
	float GetVolume();

protected:
	std::unordered_map<std::string, int> m_soundTable;

protected:
	static SoundManager * ms_pInstance;
};

#define SOUND SoundManager::GetInstance()
#define DESTROY_SOUND SoundManager::DestroyInstance();