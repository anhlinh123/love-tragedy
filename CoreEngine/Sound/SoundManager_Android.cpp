#include "StdAfx.h"
#include "SoundManager.h"
#include "jni_base.h"
#include "PathDefine.h"
#include "Jsoncpp/json/json.h"
#include <Android/log.h>

using namespace std;
SoundManager * SoundManager::ms_pInstance = NULL;

SoundManager* SOUND 
{
	if ( ms_pInstance == NULL )
		ms_pInstance = new SoundManager();
	return ms_pInstance;
}

void SoundManager::DestroyInstance() 
{
	if ( ms_pInstance )
	{
		delete ms_pInstance;
		ms_pInstance = NULL;
	}
}

void SoundManager::Initialize()
{
	char fullPath[500];
	GetFullPath(SOUND_PATH, "sound.json", fullPath);
	ifstream in(fullPath, ios::in | ios::binary);
	stringstream sstr;
	sstr << in.rdbuf();
	
	Json::Value root;
	Json::Reader reader;
	
	bool parsingSuccessful = reader.parse( sstr.str(), root );
	if ( !parsingSuccessful )
		return;

	Json::Value::Members memberList = root.getMemberNames();
	for (Json::Value::Members::iterator it = memberList.begin(); it != memberList.end(); it++)
	{
		char fullSoundPath[500];
		GetFullPath(SOUND_PATH, root[*it].asCString(), fullSoundPath);
		__android_log_print(ANDROID_LOG_INFO, "LINH1", "%s", fullSoundPath);
		m_soundTable[*it] = nativeAddSound(fullSoundPath);
		__android_log_print(ANDROID_LOG_INFO, "LINH2", "%s %d", (*it).c_str(), m_soundTable[*it]);
	}
}

void SoundManager::PlaySound(const char* name)
{
	//__android_log_print(ANDROID_LOG_INFO, "LINH1","YOLO");
	nativePlaySound(m_soundTable[name], 0);
}

void SoundManager::PlayMusic(const char* name)
{
	char fullPath[100];
	GetFullPath(SOUND_PATH, name, fullPath);
	nativePlayMusic(fullPath);
}

void SoundManager::StopMusic()
{
	nativeStopMusic();
}

void SoundManager::Stop(const char* name)
{
	nativeStopSound(m_soundTable[name]);
}

void SoundManager::Pause(const char* name)
{
	nativePauseSound(m_soundTable[name]);
}

void SoundManager::Resume(const char* name)
{
	nativeResumeSound(m_soundTable[name]);
}

void SoundManager::StopAll()
{
	//unsupported
}

void SoundManager::PauseAll()
{
	nativePauseAll();
}

void SoundManager::ResumeAll()
{
	nativeResumeAll();
}

void SoundManager::SetVolume(float fVolume)
{
	//unsupported
}

float SoundManager::GetVolume()
{
	//unsupported
	return 0;
}