#pragma once
#include "Utilities/utilities.h"
#include "Mesh.h"
#include "Texture.h"
#include "Uniform.h"

class Shader;
Shader* CreateShader(const char* name);
void ClearShaderTable();

///----------------------------------------------------------------------
///----------------------------------------------------------------------
class Shader
{
public:
	GLuint program, vertexShader, fragmentShader;
	GLint vertexAttrib[VERTEX_MAX];
	GLint textureUniform[TEXTURE_MAX];
	GLint matTransform;
	GLint matBones;
	GLint customMatrix[CUSTOM_MATRIX_MAX];
	GLint customVector[CUSTOM_VECTOR_MAX];

	int Load(const char * fileName);
	~Shader();
};