#include "stdafx.h"
#include "GeometryChunk.h"
#include <string.h>

GeometryChunk::GeometryChunk() 
	: m_mesh(NULL)
	, m_shader(NULL)
	, m_bIsBlendEnabled(false)
	, m_bIsCullingEnabled(false)
	, m_bIsDepthTestEnabled(false)
{
	m_matWorld.SetIdentity();
	m_matGlobal.SetIdentity();
	memset(m_texture, 0, sizeof(m_texture));
}

void GeometryChunk::SetCustomMatrix(int location, Matrix customMatrix)
{
	m_customMatrix[location] = customMatrix;
}

void GeometryChunk::SetCustomVector(int location, Vector3 customVector)
{
	m_customVector[location] = customVector;
}
