#pragma once

#include "Utilities/utilities.h"

class Texture;
///----------------------------------------------------------------------
///----------------------------------------------------------------------
enum TextureID
{
	TEXTURE_0 = 0,
	TEXTURE_1,
	TEXTURE_2,
	TEXTURE_3,
	TEXTURE_4,
	TEXTURE2D_MAX = TEXTURE_4,
	TEXTURE_5,
	TEXTURE_MAX
};

///----------------------------------------------------------------------
///----------------------------------------------------------------------
const char* GetTextureName(TextureID id);
Texture* CreateTexture(const char* name, const char* tileMode, bool isCubeTexture);
void ClearTextureTable();

///----------------------------------------------------------------------
/// Class Texture
///----------------------------------------------------------------------
class Texture {
public:
	bool m_bIsCubeTexture;
	GLuint m_TexId;

	Texture (bool isCubeTexture) : m_bIsCubeTexture(isCubeTexture) {}
	~Texture ();

	void Load(const char* fileName, GLenum tileMode, GLenum minFilter, GLenum magFilter);
	
private:
	char* ExtractBox(char *buffer, int buffer_width, int face, int box_width, int box_height, int bytePerPixel);
};