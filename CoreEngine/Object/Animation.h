/*
---------------------------------------------------------------------------
Open Asset Import Library (assimp)
---------------------------------------------------------------------------

Copyright (c) 2006-2012, assimp team

All rights reserved.

Redistribution and use of this software in source and binary forms, 
with or without modification, are permitted provided that the following 
conditions are met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

* Neither the name of the assimp team, nor the names of its
  contributors may be used to endorse or promote products
  derived from this software without specific prior
  written permission of the assimp team.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
---------------------------------------------------------------------------
*/

/** @file anim.h
 *  @brief Defines the data structures in which the imported animations
 *  are returned.
 */
#include <Utilities\Math.h>
// ---------------------------------------------------------------------------
/** A time-value pair specifying a certain 3D vector for the given time. */
struct VectorKey
{
	/** The time of this key */
	double mTime;     
	
	/** The value of this key */
	Vector3 mValue; 

#ifdef __cplusplus

	//! Default constructor
	VectorKey(){}

	typedef Vector3 elem_type;

	// Comparison operators. For use with std::find();
	bool operator == (const VectorKey& o) const {
		return o.mValue == this->mValue;
	}
	bool operator != (const VectorKey& o) const {
		return o.mValue != this->mValue;
	}

	// Relational operators. For use with std::sort();
	bool operator < (const VectorKey& o) const {
		return mTime < o.mTime;
	}
	bool operator > (const VectorKey& o) const {
		return mTime > o.mTime;
	}
#endif
};

// ---------------------------------------------------------------------------
/** A time-value pair specifying a rotation for the given time. 
 *  Rotations are expressed with quaternions. */
struct QuatKey
{
	/** The time of this key */
	double mTime;     

	/** The value of this key */
	Quaternion mValue; 

#ifdef __cplusplus
	QuatKey(){
	}

	/** Construction from a given time and key value */
	QuatKey(double time, const Quaternion& value)
		:	mTime	(time)
		,	mValue	(value)
	{}

	typedef Quaternion elem_type;

	// Comparison operators. For use with std::find();
	bool operator == (const QuatKey& o) const {
		return o.mValue == this->mValue;
	}
	bool operator != (const QuatKey& o) const {
		return o.mValue != this->mValue;
	}

	// Relational operators. For use with std::sort();
	bool operator < (const QuatKey& o) const {
		return mTime < o.mTime;
	}
	bool operator > (const QuatKey& o) const {
		return mTime > o.mTime;
	}
#endif
};

// ---------------------------------------------------------------------------
/** Describes the animation of a single node. The name specifies the 
 *  bone/node which is affected by this animation channel. The keyframes
 *  are given in three separate series of values, one each for position, 
 *  rotation and scaling. The transformation matrix computed from these
 *  values replaces the node's original transformation matrix at a
 *  specific time.
 *  This means all keys are absolute and not relative to the bone default pose.
 *  The order in which the transformations are applied is
 *  - as usual - scaling, rotation, translation.
 *
 *  @note All keys are returned in their correct, chronological order.
 *  Duplicate keys don't pass the validation step. Most likely there
 *  will be no negative time values, but they are not forbidden also ( so 
 *  implementations need to cope with them! ) */
struct NodeAnim
{
	/** The name of the node affected by this animation. The node 
	 *  must exist and it must be unique.*/
	std::string m_name;

	/** The number of position keys */
	unsigned int m_numPosKeys;

	/** The position keys of this animation channel. Positions are 
	 * specified as 3D vector. The array is mNumPositionKeys in size.
	 *
	 * If there are position keys, there will also be at least one
	 * scaling and one rotation key.*/
	VectorKey* m_posKeys;

	/** The number of rotation keys */
	unsigned int m_numRotKeys;

	/** The rotation keys of this animation channel. Rotations are 
	 *  given as quaternions,  which are 4D vectors. The array is 
	 *  mNumRotationKeys in size.
	 *
	 * If there are rotation keys, there will also be at least one
	 * scaling and one position key. */
	QuatKey* m_rotKeys;


	/** The number of scaling keys */
	unsigned int m_numScaKeys;

	/** The scaling keys of this animation channel. Scalings are 
	 *  specified as 3D vector. The array is mNumScalingKeys in size.
	 *
	 * If there are scaling keys, there will also be at least one
	 * position and one rotation key.*/
	VectorKey* m_scaKeys;

#ifdef __cplusplus
	NodeAnim()
	{
		m_numPosKeys = 0; m_posKeys = NULL; 
		m_numRotKeys = 0; m_rotKeys = NULL; 
		m_numScaKeys = 0; m_scaKeys  = NULL; 
	}

	~NodeAnim()
	{
		delete [] m_posKeys;
		delete [] m_rotKeys;
		delete [] m_scaKeys;
	}
#endif // __cplusplus
};

// ---------------------------------------------------------------------------
/** An animation consists of keyframe data for a number of nodes. For 
 *  each node affected by the animation a separate series of data is given.*/
struct Animation
{
	/** Duration of the animation in ticks.  */
	double m_duration;

	/** Ticks per second. 0 if not specified in the imported file */
	double m_ticksPerSecond;

	/** The number of bone animation channels. Each channel affects
	 *  a single node. */
	unsigned int m_num_channels;

	/** The node animation channels. Each channel affects a single node. 
	 *  The array is m_num_channels in size. */
	NodeAnim** m_channels;

#ifdef __cplusplus
	Animation()
		: m_duration(-1.)
		, m_ticksPerSecond()
		, m_num_channels()
		, m_channels()
	{
	}

	~Animation()
	{
		// DO NOT REMOVE THIS ADDITIONAL CHECK
		if (m_num_channels && m_channels)	{
			for( unsigned int a = 0; a < m_num_channels; a++) {
				delete m_channels[a];
			}

		delete [] m_channels;
		}
	}
#endif // __cplusplus
};