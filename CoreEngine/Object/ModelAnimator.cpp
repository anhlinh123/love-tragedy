/*
---------------------------------------------------------------------------
Open Asset Import Library (assimp)
---------------------------------------------------------------------------

Copyright (c) 2006-2012, assimp team

All rights reserved.

Redistribution and use of this software in source and binary forms, 
with or without modification, are permitted provided that the following 
conditions are met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

* Neither the name of the assimp team, nor the names of its
  contributors may be used to endorse or promote products
  derived from this software without specific prior
  written permission of the assimp team.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
---------------------------------------------------------------------------
*/

/** @file ModelAnimator.cpp
 *  @brief Implementation of the utility class ModelAnimator
 */

#include "stdafx.h"
#include "ModelAnimator.h"
#include "Mesh.h"
#include "Jsoncpp/json/json.h"
#include "PathDefine.h"
#include <math.h>

using namespace std;
// ------------------------------------------------------------------------------------------------
// Constructor for a given scene.
ModelAnimator::ModelAnimator(const char* fileName)
{
	char fullPath[100];
	GetFullPath(ANIMATION_PATH, fileName, fullPath);
	ifstream in(fullPath, ios::in | ios::binary);
	stringstream sstr;
	sstr << in.rdbuf();
	
	Json::Value root;
	Json::Reader reader;
	
	bool parsingSuccessful = reader.parse( sstr.str(), root );
	//assert(parsingSuccessful, "Unable to parse the file %s.", fullPath);

	const char* boneFile = root["Bone Hierarchy"].asCString();
	GetFullPath(ANIMATION_PATH, boneFile, fullPath);
	FILE* pFile = fopen(fullPath, "rb");		//assert( pFile, "Unable to open file %s", pFileBoneName);
	fseek(pFile, 0, SEEK_END);
	unsigned int size = ftell(pFile);
	rewind(pFile);
	unsigned char* buffer = new unsigned char[size];
	fread(buffer, sizeof(unsigned char), size, pFile);
	fclose(pFile);

	mRootNode = new Node();
	CreateNodeTree(buffer, 0, mRootNode, mIdentityMatrix);
	delete[] buffer;

	Json::Value animationList = root["Animation"];
	int animationCount = animationList.isNull() ? 0 : animationList.size();
	for (int i = 0; i < animationCount; i++)
	{
		Json::Value animation = animationList[i];
		AddAnimation(animation["File"].asCString(), animation["Name"].asCString());
	}

	m_currentAnimName = "";
	mLastTime = 0.0;
	m_isPrototype = true;
}

// ------------------------------------------------------------------------------------------------
// Copy constructor.
ModelAnimator::ModelAnimator(const ModelAnimator& otherAnimator)
{
	m_currentAnimName = "";
	mLastTime = 0.0;
	m_isPrototype = false;

	mRootNode = new Node();
	CopyNodeTree(mRootNode, otherAnimator.mRootNode);
	m_animations = otherAnimator.m_animations;
}

void ModelAnimator::CopyNodeTree(Node* pNode, Node* otherNode)
{
	pNode->m_name = otherNode->m_name;
	pNode->m_localTransform = otherNode->m_localTransform;
	pNode->m_globalTransform = otherNode->m_globalTransform;
	pNode->m_child = otherNode->m_child;
	mNodesByName[pNode->m_name] = pNode;
	// continue for all children
	for(unsigned int i = 0; i < pNode->m_child.size(); i++)
	{
		pNode->m_child[i] = new Node();
		CopyNodeTree( pNode->m_child[i], otherNode->m_child[i]); 
	}
}

// ------------------------------------------------------------------------------------------------
// Destructor
ModelAnimator::~ModelAnimator()
{
	delete mRootNode;
	if (m_isPrototype)
	{
		for (std::unordered_map<std::string, Animation*>::const_iterator iterator = m_animations.begin(); iterator != m_animations.end(); iterator++)
		{
			delete iterator->second;
		}
	}
}

// ------------------------------------------------------------------------------------------------
// Add an animation
void ModelAnimator::AddAnimation(const char* pFileName, const char* anim_name)
{
	char fullPath[100];
	GetFullPath(ANIMATION_PATH, pFileName, fullPath);
	FILE* pFile = fopen(fullPath, "rb");		
	assert(pFile);
	fseek(pFile, 0, SEEK_END);
	unsigned int size = ftell(pFile);
	rewind(pFile);
	unsigned char* buffer = new unsigned char[size];
	fread(buffer, sizeof(unsigned char), size, pFile);
	fclose(pFile);

	unsigned int cursor = 0;
	Animation* anim = new Animation();
	memcpy(&anim->m_duration, buffer+cursor, sizeof(double));				cursor += sizeof(double);
	memcpy(&anim->m_ticksPerSecond, buffer+cursor, sizeof(double));			cursor += sizeof(double);
	memcpy(&anim->m_num_channels, buffer+cursor, sizeof(unsigned int));		cursor += sizeof(unsigned int);

	anim->m_channels = new NodeAnim*[anim->m_num_channels];
	for (unsigned int i = 0; i < anim->m_num_channels; i++)
	{
		anim->m_channels[i] = new NodeAnim();
		anim->m_channels[i]->m_name = (const char*) buffer+cursor;
		cursor += anim->m_channels[i]->m_name.length()+1;

		memcpy(&anim->m_channels[i]->m_numPosKeys, buffer+cursor, sizeof(unsigned int));	cursor += sizeof(unsigned int);
		anim->m_channels[i]->m_posKeys = new VectorKey[anim->m_channels[i]->m_numPosKeys];
		for (unsigned int posIndex = 0; posIndex < anim->m_channels[i]->m_numPosKeys; posIndex++)
		{
			memcpy(&anim->m_channels[i]->m_posKeys[posIndex].mTime, buffer+cursor, sizeof(double));
			cursor += sizeof(double);
			memcpy(&anim->m_channels[i]->m_posKeys[posIndex].mValue, buffer+cursor, sizeof(Vector3));
			cursor += sizeof(Vector3);
		}

		memcpy(&anim->m_channels[i]->m_numRotKeys, buffer+cursor, sizeof(unsigned int));	cursor += sizeof(unsigned int);
		anim->m_channels[i]->m_rotKeys = new QuatKey[anim->m_channels[i]->m_numRotKeys];
		for (unsigned int rotIndex = 0; rotIndex < anim->m_channels[i]->m_numRotKeys; rotIndex++)
		{
			memcpy(&anim->m_channels[i]->m_rotKeys[rotIndex], buffer+cursor, sizeof(QuatKey));
			cursor += sizeof(QuatKey);
		}

		memcpy(&anim->m_channels[i]->m_numScaKeys, buffer+cursor, sizeof(unsigned int));	cursor += sizeof(unsigned int);
		anim->m_channels[i]->m_scaKeys = new VectorKey[anim->m_channels[i]->m_numScaKeys];
		for (unsigned int scaIndex = 0; scaIndex < anim->m_channels[i]->m_numScaKeys; scaIndex++)
		{
			memcpy(&anim->m_channels[i]->m_scaKeys[scaIndex].mTime, buffer+cursor, sizeof(double));
			cursor += sizeof(double);
			memcpy(&anim->m_channels[i]->m_scaKeys[scaIndex].mValue, buffer+cursor, sizeof(Vector3));
			cursor += sizeof(Vector3);
		}
	}
	m_animations[anim_name] = anim;

	delete[] buffer;
}

// ------------------------------------------------------------------------------------------------
// Sets the animation to use for playback. 
void ModelAnimator::SetAnimName( const char* anim_name)
{
	// no change
	if( m_currentAnimName == anim_name || m_animations.find(anim_name) == m_animations.end())
		return;
	m_currentAnimName = anim_name;
	mLastPositions.resize( m_animations[anim_name]->m_num_channels, boost::make_tuple( 0, 0, 0));
	UpdateNodeTree( mRootNode);
}

// ------------------------------------------------------------------------------------------------
// Calculates the node transformations for the scene. 
void ModelAnimator::Calculate( double pTime)
{
	// no animation
	if( m_currentAnimName == "")
		return;

	Animation* pAnim = m_animations[m_currentAnimName];
	// extract ticks per second. Assume default value if not given
	double ticksPerSecond = pAnim->m_ticksPerSecond != 0.0 ? pAnim->m_ticksPerSecond : 25.0;
	// every following time calculation happens in ticks
	pTime *= ticksPerSecond;

	// map into anim's duration
	double time = 0.0f;
	if( pAnim->m_duration > 0.0)
		time = fmod( pTime, pAnim->m_duration);

	std::vector<Matrix> transforms;
	transforms.resize( pAnim->m_num_channels);

	// calculate the transformations for each animation channel
	for( unsigned int a = 0; a < pAnim->m_num_channels; a++)
	{
		const NodeAnim* channel = pAnim->m_channels[a];

		// ******** Position *****
		Vector3 presentPosition( 0, 0, 0);
		if( channel->m_numPosKeys > 0)
		{
			// Look for present frame number. Search from last position if time is after the last time, else from beginning
			// Should be much quicker than always looking from start for the average use case.
			unsigned int frame = (time >= mLastTime) ? mLastPositions[a].get<0>() : 0;
			while( frame < channel->m_numPosKeys - 1)
			{
				if( time < channel->m_posKeys[frame+1].mTime)
					break;
				frame++;
			}
			
			// interpolate between this frame's value and next frame's value
			unsigned int nextFrame = (frame + 1) % channel->m_numPosKeys;
			const VectorKey& key = channel->m_posKeys[frame];
			const VectorKey& nextKey = channel->m_posKeys[nextFrame];
			double diffTime = nextKey.mTime - key.mTime;
			if( diffTime < 0.0)
				diffTime += pAnim->m_duration;
			if( diffTime > 0)
			{
				float factor = float( (time - key.mTime) / diffTime);
				presentPosition = key.mValue + (nextKey.mValue - key.mValue) * factor;
			} else
			{
				presentPosition = key.mValue;
			}

			mLastPositions[a].get<0>() = frame;
		}

		// ******** Rotation *********
		Quaternion presentRotation( 1, 0, 0, 0);
		if( channel->m_numRotKeys > 0)
		{
			unsigned int frame = (time >= mLastTime) ? mLastPositions[a].get<1>() : 0;
			while( frame < channel->m_numRotKeys - 1)
			{
				if( time < channel->m_rotKeys[frame+1].mTime)
					break;
				frame++;
			}
			
			// interpolate between this frame's value and next frame's value
			unsigned int nextFrame = (frame + 1) % channel->m_numRotKeys;
			const QuatKey& key = channel->m_rotKeys[frame];
			const QuatKey& nextKey = channel->m_rotKeys[nextFrame];
			double diffTime = nextKey.mTime - key.mTime;
			if( diffTime < 0.0)
				diffTime += pAnim->m_duration;
			if( diffTime > 0)
			{
				//presentRotation = nextKey.mValue;
				float factor = float( (time - key.mTime) / diffTime);
				Quaternion::Interpolate( presentRotation, key.mValue, nextKey.mValue, factor);
			} else
			{
				presentRotation = key.mValue;
			}

			mLastPositions[a].get<1>() = frame;
		}

		// ******** Scaling **********
		Vector3 presentScaling( 1, 1, 1);
		if( channel->m_numScaKeys > 0)
		{
			unsigned int frame = (time >= mLastTime) ? mLastPositions[a].get<2>() : 0;
			while( frame < channel->m_numScaKeys - 1)
			{
				if( time < channel->m_scaKeys[frame+1].mTime)
					break;
				frame++;
			}
			
			// TODO: (thom) interpolation maybe? This time maybe even logarithmic, not linear
			presentScaling = channel->m_scaKeys[frame].mValue;
			mLastPositions[a].get<2>() = frame;
		}

		// build a transformation matrix from it
		Matrix& mat = transforms[a];
		mat = Matrix( presentRotation.GetMatrix());
		mat.a1 *= presentScaling.x; mat.b1 *= presentScaling.x; mat.c1 *= presentScaling.x;
		mat.a2 *= presentScaling.y; mat.b2 *= presentScaling.y; mat.c2 *= presentScaling.y;
		mat.a3 *= presentScaling.z; mat.b3 *= presentScaling.z; mat.c3 *= presentScaling.z;
		mat.a4 = presentPosition.x; mat.b4 = presentPosition.y; mat.c4 = presentPosition.z;
		mat = mat.Transpose();
	}

	mLastTime = time;

	// and update all node transformations with the results
	UpdateTransforms( mRootNode, transforms, Matrix());
}

// ------------------------------------------------------------------------------------------------
// Calculates the bone matrices for the given mesh. 
const std::vector<Matrix>& ModelAnimator::GetBoneMatrices( Mesh* pMesh)
{
	// resize array and initialise it with identity matrices
	mTransforms.clear();
	mTransforms.resize( pMesh->m_numBones ? pMesh->m_numBones : 1, mIdentityMatrix);

	// calculate the mesh's inverse global transform
	Matrix globalInverseMeshTransform;
	if (mNodesByName.find(pMesh->m_nodeName) != mNodesByName.end())
	{
		globalInverseMeshTransform = mNodesByName[pMesh->m_nodeName]->m_globalTransform;
		globalInverseMeshTransform.Inverse();
	}
	else
	{
		//assert(true, "A mesh without bones!!!");
	}

	// Bone matrices transform from mesh coordinates in bind pose to mesh coordinates in skinned pose
	// Therefore the formula is offsetMatrix * currentGlobalTransform * inverseCurrentMeshTransform
	for( int a = 0; a < pMesh->m_numBones; ++a)
	{
		const Matrix& currentGlobalTransform = mNodesByName[pMesh->m_bones[a].m_name]->m_globalTransform;
		mTransforms[a] = pMesh->m_bones[a].m_offset * currentGlobalTransform * globalInverseMeshTransform;
	}

	// and return the result
	return mTransforms;
}

// ------------------------------------------------------------------------------------------------
// Get the global matrix for the given mesh.
Matrix ModelAnimator::GetGlobalMatrix( Mesh* pMesh)
{
	return mNodesByName[pMesh->m_nodeName]->m_globalTransform;
}

// ------------------------------------------------------------------------------------------------
// Recursively creates an internal node structure.
void ModelAnimator::CreateNodeTree( unsigned char* buffer, unsigned int offset, Node* pNode, Matrix parentMatrix)
{
	pNode->m_name = (const char*) buffer + offset ;
	mNodesByName[pNode->m_name] = pNode;
	offset += pNode->m_name.length() + 1;

	memcpy(&pNode->m_localTransform, buffer+offset, sizeof(Matrix));
	pNode->m_globalTransform = pNode->m_localTransform * parentMatrix;
	offset += sizeof(Matrix);
	
	unsigned int numChild = 0;
	memcpy(&numChild, buffer+offset, sizeof(unsigned int));
	pNode->m_child.resize(numChild, NULL);
	offset += sizeof(unsigned int);

	if (numChild > 0)
	{
		unsigned int* childOffset = new unsigned int[numChild];
		memcpy(childOffset, buffer+offset, sizeof(unsigned int) * numChild);
		for (unsigned int i = 0; i < numChild; i++)
		{
			pNode->m_child[i] = new Node();
			CreateNodeTree(buffer, childOffset[i], pNode->m_child[i], pNode->m_globalTransform);
		}
		delete[] childOffset;
	}
}

// ------------------------------------------------------------------------------------------------
// Recursively update an internal node structure matching the current animation.
void ModelAnimator::UpdateNodeTree( Node* pNode)
{
	// find the index of the animation track affecting this node, if any
	if( m_animations.find(m_currentAnimName) != m_animations.end())
	{
		pNode->mChannelIndex = -1;
		const Animation* currentAnim = m_animations[m_currentAnimName];
		for( unsigned int a = 0; a < currentAnim->m_num_channels; a++)
		{
			if( currentAnim->m_channels[a]->m_name == pNode->m_name)
			{
				pNode->mChannelIndex = a;
				break;
			}
		}
	}

	// continue for all children
	for( std::vector<Node*>::iterator it = pNode->m_child.begin(); it != pNode->m_child.end(); ++it)
	{
		UpdateNodeTree( *it);
	}
}

// ------------------------------------------------------------------------------------------------
// Recursively updates the internal node transformations from the given matrix array
void ModelAnimator::UpdateTransforms( Node* pNode, const std::vector<Matrix>& pTransforms, Matrix parentMatrix)
{
	// update node local transform
	if( pNode->mChannelIndex != -1)
	{
		//ai_assert( pNode->mChannelIndex < pTransforms.size());
		pNode->m_localTransform = pTransforms[pNode->mChannelIndex];
	}

	pNode->m_globalTransform = pNode->m_localTransform * parentMatrix;

	// continue for all children
	for( std::vector<Node*>::iterator it = pNode->m_child.begin(); it != pNode->m_child.end(); ++it)
		UpdateTransforms( *it, pTransforms, pNode->m_globalTransform);
}