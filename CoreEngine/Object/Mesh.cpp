#include "stdafx.h"
#include "Utilities/utilities.h"
#include "Mesh.h"
#include "PathDefine.h"
#include <stdio.h>
#include <unordered_map>

std::unordered_map<std::string, std::vector<Mesh*>> g_meshTable;
///----------------------------------------------------------------------
///----------------------------------------------------------------------
const char* Vertex_AttributeName[VERTEX_MAX] =
{
	"aPosition",
	"aNormal",
	"aBinormal",
	"aTangent",
	"aTexCoord0",
	"aTexCoord1",
	"aColor",
	"aSkinMatrixIndices",
	"aSkinMatrixWeights"
};

///----------------------------------------------------------------------
///----------------------------------------------------------------------
const char* GetVertexAttributeName(Vertex_Attribute attrib)
{
	return Vertex_AttributeName[attrib];
}

int GetVertexAttributeSize(Vertex_Attribute attrib)
{
	switch (attrib)
	{
	case VERTEX_POSITION:   return 3;
	case VERTEX_NORMAL:     return 3;
	case VERTEX_BINORMAL:   return 3;
	case VERTEX_TANGENT:    return 3;
	case VERTEX_TEXCOORD_0: return 2;
	case VERTEX_TEXCOORD_1: return 2;
	case VERTEX_COLOR:      return 3;
	//GPU_SKINNING
	case VERTEX_SKIN_INDICES: return 4;
	case VERTEX_SKIN_WEIGHTS: return 4;
	default: return 0;
	}
}

int GetVertexAttributeOffset(Vertex_Attribute attrib)
{
	switch (attrib)
	{
	case VERTEX_POSITION:   return 0;
	case VERTEX_NORMAL:     return 3 * sizeof(float);
	case VERTEX_BINORMAL:   return 6 * sizeof(float);
	case VERTEX_TANGENT:    return 9 * sizeof(float);
	case VERTEX_TEXCOORD_0: return 12 * sizeof(float);
	case VERTEX_TEXCOORD_1: return 14 * sizeof(float);
	case VERTEX_COLOR:      return 16 * sizeof(float);
	//GPU_SKINNING
	case VERTEX_SKIN_INDICES: return 19 * sizeof(float);
	case VERTEX_SKIN_WEIGHTS: return 19 * sizeof(float) + 4 * sizeof(unsigned char);
	default: return 0;
	}
}

GLenum GetVertexAttributeType(Vertex_Attribute attrib)
{
	switch (attrib)
	{
	case VERTEX_POSITION:   return GL_FLOAT;
	case VERTEX_NORMAL:     return GL_FLOAT;
	case VERTEX_BINORMAL:   return GL_FLOAT;
	case VERTEX_TANGENT:    return GL_FLOAT;
	case VERTEX_TEXCOORD_0: return GL_FLOAT;
	case VERTEX_TEXCOORD_1: return GL_FLOAT;
	case VERTEX_COLOR:      return GL_FLOAT;
	//GPU_SKINNING
	case VERTEX_SKIN_INDICES: return GL_UNSIGNED_BYTE;
	case VERTEX_SKIN_WEIGHTS: return GL_UNSIGNED_BYTE;
	default: return 0;
	}
}


std::vector<Mesh*> CreateMesh(const char* fileName)
{
	std::unordered_map<std::string, std::vector<Mesh*>>::const_iterator iterator = g_meshTable.find(fileName);
	if (iterator != g_meshTable.end())
	{
		return iterator->second;
	}
	else
	{
		std::string name = fileName;
		std::string extension = name.substr(name.find_last_of(".")+1);

		// Backward compatibility
		if (extension == "nfg")
		{
			std::vector<Mesh*> meshList;
			meshList.resize(1, NULL);
			meshList[0] = new Mesh();
			meshList[0]->Load(fileName);
			g_meshTable[fileName] = meshList;
			return meshList;
		}

		char fullPath[100];
		GetFullPath(MESH_PATH, fileName, fullPath);	
		FILE* pFile = fopen(fullPath, "rb");
		fseek(pFile, 0, SEEK_END);
		unsigned int size = ftell(pFile);
		rewind(pFile);
		unsigned char* buffer = new unsigned char[size];
		fread(buffer, sizeof(unsigned char), size, pFile);
		fclose(pFile);

		unsigned int cursor = 0;
		int numMesh = 0;
		memcpy(&numMesh, buffer+cursor, sizeof(int));
		cursor += sizeof(int);

		std::vector<Mesh*> meshList;
		meshList.resize(numMesh, NULL);
		for (int i = 0; i < numMesh; i++)
		{
			meshList[i] = new Mesh();

			meshList[i]->m_nodeName = (const char*) buffer+cursor;
			cursor += meshList[i]->m_nodeName.length()+1;

			int numVertices = 0;
			memcpy(&numVertices, buffer+cursor, sizeof(int));
			cursor += sizeof(int);

			Vertex* p_aVertices = new Vertex[numVertices];
			memcpy(p_aVertices, buffer+cursor, sizeof(Vertex) * numVertices);
			cursor += sizeof(Vertex) * numVertices;
			
			int numIndices = 0;
			memcpy(&numIndices, buffer+cursor, sizeof(int));
			meshList[i]->m_iIndex_count = numIndices;
			cursor += sizeof(int);
			//Fix bug no graphic on some device
			//This one should be replaced with memcpy, and the data should be
			//changed as well
			/*int* p_aIndices = new int[numIndices];
			memcpy(p_aIndices, buffer+cursor, sizeof(int) * numIndices);
			cursor += sizeof(int) * numIndices;*/
			unsigned short* p_aIndices = new unsigned short[numIndices];
			for (int a = 0; a < numIndices; a++, cursor += sizeof(int))
			{
				p_aIndices[a] = (unsigned short)*(buffer + cursor);
			}

			glGenBuffers(2, meshList[i]->m_boIds);
			glBindBuffer(GL_ARRAY_BUFFER, meshList[i]->m_boIds[0]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * numVertices, p_aVertices, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshList[i]->m_boIds[1]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * numIndices, p_aIndices, GL_STATIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

			delete[] p_aVertices;
			delete[] p_aIndices;

			int numBones = 0;
			memcpy(&numBones, buffer+cursor, sizeof(int));
			cursor += sizeof(int);
			meshList[i]->m_numBones = numBones;
			meshList[i]->m_bones = new Bone[numBones];
			for (int j = 0; j < numBones; j++)
			{
				meshList[i]->m_bones[j].m_name = (const char*) buffer+cursor;
				cursor += meshList[i]->m_bones[j].m_name.length()+1;
				memcpy(&meshList[i]->m_bones[j].m_offset, buffer+cursor, sizeof(Matrix));
				cursor += sizeof(Matrix);
			}
		}
		delete[] buffer;
		g_meshTable[fileName] = meshList;
		return meshList;
	}
}

void ClearMeshTable()
{
	for (std::unordered_map<std::string, std::vector<Mesh*>>::const_iterator iterator = g_meshTable.begin(); iterator != g_meshTable.end(); iterator++)
	{
		std::vector<Mesh*> meshList = iterator->second;
		for (std::vector<Mesh*>::const_iterator iterMesh = meshList.begin(); iterMesh != meshList.end(); iterMesh++)
		{
			delete *iterMesh;
		}
	}
	g_meshTable.clear();
}

///----------------------------------------------------------------------
/// Class implementation
///----------------------------------------------------------------------
int Mesh::Load(const char* fileName)
{
	char fullPath[100];
	GetFullPath(MESH_PATH, fileName, fullPath);
	FILE *nfg = fopen(fullPath, "r");
	if(!nfg) 
	{
		printf("Cannot read nfg file");
		return -1;
	};
	
	int vertex_count;
	fscanf(nfg, "%*s %d\n", &vertex_count);
	Vertex* p_aVertices = new Vertex[vertex_count];
	for (int i = 0; i < vertex_count; i++) 
	{
		fscanf(nfg, "%*d. pos:[%f, %f, %f]; norm:[%f, %f, %f]; binorm:[%f, %f, %f]; tgt:[%f, %f, %f]; uv:[%f, %f];\n", 
			&p_aVertices[i].m_pos.x,      &p_aVertices[i].m_pos.y,      &p_aVertices[i].m_pos.z,
			&p_aVertices[i].m_normal.x,   &p_aVertices[i].m_normal.y,   &p_aVertices[i].m_normal.z,
			&p_aVertices[i].m_binormal.x, &p_aVertices[i].m_binormal.y, &p_aVertices[i].m_binormal.z,
			&p_aVertices[i].m_tangent.x,  &p_aVertices[i].m_tangent.y,  &p_aVertices[i].m_tangent.z,
			&p_aVertices[i].m_uv.x,       &p_aVertices[i].m_uv.y);
		p_aVertices[i].m_uvBlend = Vector2(0.0f, 0.0f);
	}

	fscanf(nfg, "%*s %d", &m_iIndex_count);
	GLushort* p_aIndices = new GLushort[m_iIndex_count];
	int triangle = m_iIndex_count/3;
	for (int i = 0; i < triangle; i++) 
	{
		fscanf(nfg, "%*u. %hu, %hu, %hu\n", &p_aIndices[i*3], &p_aIndices[i*3 + 1], &p_aIndices[i*3 + 2]);
	}
	
	glGenBuffers(2, m_boIds);
	glBindBuffer(GL_ARRAY_BUFFER, m_boIds[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertex_count, p_aVertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_boIds[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * m_iIndex_count, p_aIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] p_aVertices;
	delete[] p_aIndices;
	p_aVertices = NULL;
	p_aIndices = NULL;

	fclose(nfg);
	return 0;
}

///----------------------------------------------------------------------
/// Destrucstor
///----------------------------------------------------------------------
Mesh::~Mesh() 
{
	glDeleteBuffers(2, m_boIds);
	if (m_aVertices)
	{
		delete[] m_aVertices;
		delete[] m_aIndices;
		m_aVertices = NULL;
		m_aIndices = NULL;
	}
	delete[] m_bones;
}