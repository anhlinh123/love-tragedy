#include "stdafx.h"
#include "Shader.h"
#include "PathDefine.h"
#include <unordered_map>

std::unordered_map<std::string, Shader*> g_shaderTable;
Shader* CreateShader(const char* name)
{
	std::unordered_map<std::string, Shader*>::const_iterator iterator = g_shaderTable.find(name);
	if (iterator != g_shaderTable.end())
	{
		return iterator->second;
	}
	else
	{
		Shader* newShader = new Shader();
		newShader->Load(name);
		g_shaderTable[name] = newShader;
		return newShader;
	}
}

void ClearShaderTable()
{
	for (std::unordered_map<std::string, Shader*>::const_iterator iterator = g_shaderTable.begin(); iterator != g_shaderTable.end(); iterator++)
	{
		delete iterator->second;
	}
	g_shaderTable.clear();
}

///----------------------------------------------------------------------
/// Class implementation
///----------------------------------------------------------------------
int Shader::Load(const char* fileName)
{
	char fullPath[100];

	GetFullPath(SHADER_PATH, fileName, fullPath);
	strcat(fullPath, ".vs");
	vertexShader = esLoadShader(GL_VERTEX_SHADER, fullPath);

	if ( vertexShader == 0 )
		return -1;

	GetFullPath(SHADER_PATH, fileName, fullPath);
	strcat(fullPath, ".fs");
	fragmentShader = esLoadShader(GL_FRAGMENT_SHADER, fullPath);

	if ( fragmentShader == 0 )
	{
		glDeleteShader( vertexShader );
		return -2;
	}

	program = esLoadProgram(vertexShader, fragmentShader);

	//finding location of uniforms / attributes
	for (int i = 0; i < VERTEX_MAX; i++)
	{
		vertexAttrib[i] = glGetAttribLocation(program, GetVertexAttributeName( (Vertex_Attribute)i ));
	}

	for (int i = 0; i < TEXTURE_MAX; i++)
	{
		textureUniform[i] = glGetUniformLocation(program, GetTextureName( (TextureID)i ));
	}

	matTransform = glGetUniformLocation(program, "uMatTransform");
	matBones = glGetUniformLocation(program, "uMatBones[0]");

	for (int i = 0; i < CUSTOM_MATRIX_MAX; i++)
	{
		customMatrix[i] = glGetUniformLocation(program, GetCustomMatrixName(i));
	}

	for (int i = 0; i < CUSTOM_VECTOR_MAX; i++)
	{
		customVector[i] = glGetUniformLocation(program, GetCustomVectorName(i));
	}

	return 0;
}

Shader::~Shader()
{
	glDeleteProgram(program);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}