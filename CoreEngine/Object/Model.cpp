#include "StdAfx.h"
#include "Renderer.h"
#include "Model.h"
#include "PathDefine.h"
#include "ModelAnimator.h"
#include "Jsoncpp/json/json.h"

using namespace std;
std::unordered_map<std::string, Model*> g_modelTable;
Model* CreateModel(const char* name)
{
	Model* newModel = new Model();
	std::unordered_map<std::string, Model*>::const_iterator iterator = g_modelTable.find(name);
	if (iterator != g_modelTable.end())
	{
		newModel->Load(*(iterator->second));
	}
	else
	{
		newModel->Load(name);
	}
	return newModel;
}

void ClearModelTable()
{
	for (std::unordered_map<std::string, Model*>::const_iterator iterator = g_modelTable.begin(); iterator != g_modelTable.end(); iterator++)
	{
		delete iterator->second;
	}
	g_modelTable.clear();
}

///----------------------------------------------------------------------
/// Class implementation
///----------------------------------------------------------------------
Model::Model(void) : m_animator(NULL), m_timeInSec(0.0)
{
}


Model::~Model(void)
{
	for (int i = 0; i < m_numChunks; i++)
	{
		delete m_geometryChunks[i];
	}
	if (m_geometryChunks)
		delete m_geometryChunks;
	if (m_animator)
		delete m_animator;
}

void Model::Load(const char* fileName)
{
	char fullPath[100];
	GetFullPath(MODEL_PATH, fileName, fullPath);
	ifstream in(fullPath, ios::in | ios::binary);
	stringstream sstr;
	sstr << in.rdbuf();
	
	Json::Value root;
	Json::Reader reader;
	
	bool parsingSuccessful = reader.parse( sstr.str(), root );
	if ( !parsingSuccessful )
		return;
	
	Texture* textureList[TEXTURE_MAX];
	memset(textureList, 0, sizeof(textureList));

	Shader* shader = CreateShader(root["Shader"].asCString());
	std::vector<Mesh*> meshList = CreateMesh(root["Mesh"].asCString());
	
	Json::Value textures = root["Textures"];
	int textureSize = textures.isNull() ? 0 : textures.size();
	for (int i = 0; i < textureSize && i < TEXTURE2D_MAX; i++)
	{
		Json::Value currentTexture = textures[i];
		textureList[i] = CreateTexture(currentTexture["File"].asCString(), currentTexture["TileMode"].asCString(), false);
	}
	
	Json::Value cubeTex = root["CubeTextures"];
	int cubeTexSize = cubeTex.isNull() ? TEXTURE2D_MAX : cubeTex.size() + TEXTURE2D_MAX;
	for (int i = TEXTURE2D_MAX; i < cubeTexSize && i < TEXTURE_MAX; i++)
	{
		Json::Value currentTexture = cubeTex[i-TEXTURE2D_MAX];
		textureList[i] = CreateTexture(currentTexture["File"].asCString(), currentTexture["TileMode"].asCString(), true);
	}

	bool bIsDepthTestEnabled = root["DepthTest"].asBool();
	bool bIsCullingEnabled = root["Culling"].asBool();
	bool bIsBlendEnabled = root["Blend"].asBool();

	Model* prototype = new Model();
	prototype->m_numChunks = meshList.size();
	prototype->m_geometryChunks = new GeometryChunk*[prototype->m_numChunks];
	for (int i = 0; i < prototype->m_numChunks; i++)
	{
		prototype->m_geometryChunks[i]                        = new GeometryChunk();
		prototype->m_geometryChunks[i]->m_shader              = shader;
		prototype->m_geometryChunks[i]->m_mesh                = meshList[i];
		prototype->m_geometryChunks[i]->m_bIsBlendEnabled     = bIsBlendEnabled;
		prototype->m_geometryChunks[i]->m_bIsCullingEnabled   = bIsCullingEnabled;
		prototype->m_geometryChunks[i]->m_bIsDepthTestEnabled = bIsDepthTestEnabled;
		for (int j = 0; j < TEXTURE_MAX; j++)
		{
			prototype->m_geometryChunks[i]->m_texture[j] = textureList[j];
		}
	}
	Json::Value animation = root["Animation"];
	if (!animation.isNull())
		prototype->m_animator = new ModelAnimator(animation.asCString());
	
	g_modelTable[fileName] = prototype;
	Load(*prototype);
}

void Model::Load(const Model& prototype)
{
	m_numChunks = prototype.m_numChunks;
	m_geometryChunks = new GeometryChunk*[m_numChunks];
	for (int i = 0; i < m_numChunks; i++)
	{
		GeometryChunk* prototypeChunk = prototype.m_geometryChunks[i];

		m_geometryChunks[i]                        = new GeometryChunk();
		m_geometryChunks[i]->m_mesh                = prototypeChunk->m_mesh;
		m_geometryChunks[i]->m_shader              = prototypeChunk->m_shader;
		m_geometryChunks[i]->m_bIsBlendEnabled     = prototypeChunk->m_bIsBlendEnabled;
		m_geometryChunks[i]->m_bIsCullingEnabled   = prototypeChunk->m_bIsCullingEnabled;
		m_geometryChunks[i]->m_bIsDepthTestEnabled = prototypeChunk->m_bIsDepthTestEnabled;
		for (int j = 0; j < TEXTURE_MAX; j++)
		{
			m_geometryChunks[i]->m_texture[j]             = prototypeChunk->m_texture[j];
		}
	}
	if (prototype.m_animator)
	{
		m_animator = new ModelAnimator(*prototype.m_animator);
	}
}

void Model::PlayAnimation(const char* name)
{
	if (m_animator)
		m_animator->SetAnimName(name);
}

void Model::Draw()
{
	for (int i = 0; i < m_numChunks; i++)
	{
		AddGeometryChunk(m_geometryChunks[i]);
	}
}

void Model::Update(float dTime)
{
	if (m_animator)
	{
		m_animator->Calculate(m_timeInSec);
		for (int i = 0; i < m_numChunks; i++)
		{
			m_geometryChunks[i]->m_matBones = m_animator->GetBoneMatrices(m_geometryChunks[i]->m_mesh);
			m_geometryChunks[i]->m_matGlobal = m_animator->GetGlobalMatrix(m_geometryChunks[i]->m_mesh);
		}
	}
	m_timeInSec += (double)dTime;
}

void Model::SetMatWorld(const Matrix& matWorld)
{
	for (int i = 0; i < m_numChunks; i++)
	{
		m_geometryChunks[i]->m_matWorld = matWorld;
	}
}