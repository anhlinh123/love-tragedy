#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "Renderable.h"
#include "Utilities\math.h"

/** Base class for a game object
 *  This class should never know anything about
 *  what's inside its renderable.
 *  Don't access a geometryChunk directly.
 *  What if in a bad day, I rewrite everything
 *  for a renderable??? Actually, it happened
 *  when I tried to merge all animation stuffs
 *  into the code.
 */
class Object
{
public:
	Object() {}
	Object(Renderable* renderable) : m_renderable(renderable), m_isVisible(true), m_markAsDelete(false) {}
	~Object() { delete m_renderable; }
	
	void Update (float deltaTime);
	void Draw ();
	void PlayAnimation(const char* name);

	void SetGeometry (const Vector3& pos, const Vector3& rot, const Vector3& scale);
	void ChangeGeometry (const Vector3& move, const Vector3& rot, const Vector3& scale);
	void Move (const Vector3& move);
	void Rotate (Vector3& rot, Vector3& posOffset);
	void SetVisible(bool bVisible) { m_isVisible = bVisible; }

	void MarkAsDelete() { m_markAsDelete = true; }
	bool IsDeletePending() { return m_markAsDelete; }

	// parents & children
	void RelateObject(Object* obj);
	void UnrelateObject(Object* obj);

public:
	Renderable* m_renderable;
	bool m_isVisible;
	bool m_markAsDelete;
	std::vector<Object*> m_relateObjects;

private:
	Vector3  m_scale;
	Vector3  m_rotation;
	Vector3  m_translation;
	Matrix   m_matWorld;
};

typedef std::vector<Object*> ObjectVector;

#endif