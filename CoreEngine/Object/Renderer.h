#pragma once

class GeometryChunk;
class Camera;

void SetCamera(Camera* pCamera);
void AddGeometryChunk(GeometryChunk* pChunk);
void DoDraw();
void ClearRenderer();

/** Global Effects: At this point, we don't care about design patterns anymore
 * To do: Do better than this
 */
void SetFog(float fogStart);
void SetLight(float x, float y, float z);