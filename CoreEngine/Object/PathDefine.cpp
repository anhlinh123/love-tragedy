#include <stdafx.h>
#include <string.h>

void GetFullPath(const char* path, const char* fileName, char* fullPath)
{
	memset(fullPath, 0, 100);
	strcpy(fullPath, path);
	strcat(fullPath, fileName);
}