#pragma once
#include <vector>
#include "Vertex.h"

class Mesh;
///----------------------------------------------------------------------
/// Declare Vertex
///----------------------------------------------------------------------
enum Vertex_Attribute
{
	VERTEX_POSITION      = 0,
	VERTEX_NORMAL        = 1,
	VERTEX_BINORMAL      = 2,
	VERTEX_TANGENT       = 3,
	VERTEX_TEXCOORD_0    = 4,
	VERTEX_TEXCOORD_1    = 5,
	VERTEX_COLOR         = 6,
	//GPU_SKINNING
	VERTEX_SKIN_INDICES  = 7,
	VERTEX_SKIN_WEIGHTS  = 8,
	
	// Do not modify the order of this enum unless you are willing to break existing model format
	// You can append without breaking existing data
	
	VERTEX_MAX
};

///----------------------------------------------------------------------
///----------------------------------------------------------------------
const char* GetVertexAttributeName(Vertex_Attribute attrib);
int GetVertexAttributeSize(Vertex_Attribute attrib);
int GetVertexAttributeOffset(Vertex_Attribute attrib);
GLenum GetVertexAttributeType(Vertex_Attribute attrib);

///----------------------------------------------------------------------
///----------------------------------------------------------------------
std::vector<Mesh*> CreateMesh(const char* name);
void ClearMeshTable();

///----------------------------------------------------------------------
///----------------------------------------------------------------------
struct Bone
{
	std::string m_name;
	Matrix m_offset;
};

///----------------------------------------------------------------------
/// Class Mesh
///----------------------------------------------------------------------
class Mesh
{
public:
	Mesh() : m_aVertices(NULL), m_aIndices(NULL), m_bones(NULL) {}
	~Mesh();

	int Load(const char* fileName);

	//Heightmap information
	int     m_iDimension;
	Vertex *m_aVertices;
	GLuint *m_aIndices;

	//Buffers
	GLuint m_boIds[2];
	int m_iIndex_count;

	//Animation
	std::string m_nodeName;
	int m_numBones;
	Bone* m_bones;
};