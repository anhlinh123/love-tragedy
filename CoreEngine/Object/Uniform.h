#pragma once

#define CUSTOM_VECTOR_MAX     8
#define CUSTOM_MATRIX_MAX     4

const char* GetCustomVectorName(int customVector);
const char* GetCustomMatrixName(int customMatrix);