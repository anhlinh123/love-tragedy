#include "stdafx.h"
#include "Uniform.h"

const char* CustomVectorName[CUSTOM_VECTOR_MAX] =
{
	"CustomVector0",
	"CustomVector1",
	"CustomVector2",
	"CustomVector3",
	"CustomVector4",
	"CustomVector5",
	"CustomVector6",
	"CustomVector7"
};

const char* CustomMatrixName[CUSTOM_MATRIX_MAX] =
{
	"CustomMatrix0",
	"CustomMatrix1",
	"CustomMatrix2",
	"CustomMatrix3"
};

const char* GetCustomVectorName(int customVector)
{
	return CustomVectorName[customVector];
}

const char* GetCustomMatrixName(int customMatrix)
{
	return CustomMatrixName[customMatrix];
}