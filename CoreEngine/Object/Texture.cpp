#include "stdafx.h"
#include "Texture.h"
#include "PathDefine.h"
#include <unordered_map>

static std::unordered_map<std::string, Texture*> g_textureTable;

///----------------------------------------------------------------------
///----------------------------------------------------------------------
const char* TextureName[TEXTURE_MAX] =
{
	"DiffuseTexture",
	"CustomTexture0",
	"CustomTexture1",
	"CustomTexture2",
	"CubeTexture",
	"CustomCubeTexture0",
};

///----------------------------------------------------------------------
///----------------------------------------------------------------------
const char* GetTextureName(TextureID id)
{
	return TextureName[id];
}

GLenum GetTileMode(const char* tileMode)
{
	if (strcmp(tileMode, "GL_REPEAT") == 0) return GL_REPEAT;
	else if (strcmp(tileMode, "GL_CLAMP_TO_EDGE") == 0) return GL_CLAMP_TO_EDGE;
	else if (strcmp(tileMode, "GL_MIRRORED_REPEAT") == 0) return GL_MIRRORED_REPEAT;

	return GL_CLAMP_TO_EDGE;
}

Texture* CreateTexture(const char* name, const char* tileMode, bool isCubeTexture)
{
	std::unordered_map<std::string, Texture*>::const_iterator iterator = g_textureTable.find(name);
	if (iterator != g_textureTable.end())
	{
		return iterator->second;
	}
	else
	{
		Texture* newTexture = new Texture(isCubeTexture);
		newTexture->Load(name, GetTileMode(tileMode), GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
		g_textureTable[name] = newTexture;
		return newTexture;
	}
}

void ClearTextureTable()
{
	for (std::unordered_map<std::string, Texture*>::const_iterator iterator = g_textureTable.begin(); iterator != g_textureTable.end(); iterator++)
	{
		delete iterator->second;
	}
	g_textureTable.clear();
}

///----------------------------------------------------------------------
/// Class texture
///----------------------------------------------------------------------
void Texture::Load(const char* fileName, GLenum tileMode, GLenum minFilter, GLenum magFilter)
{
	glGenTextures(1, &m_TexId);
	char *buffer;
	int width, height, bpp;

	char fullPath[100];
	GetFullPath(TEXTURE_PATH, fileName, fullPath);
	buffer = LoadTGA(fullPath, &width, &height, &bpp);

	//Check whether this is a 2d texture or a cube texture
	if (!m_bIsCubeTexture) 
	{
		glBindTexture(GL_TEXTURE_2D, m_TexId);

		if(bpp == 24) 
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		}
		else 
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, tileMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tileMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

		if (minFilter > GL_LINEAR)
		{
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	else 
	{
		int box_width = width/4;
		int box_height = height/3;
		int bytePerPixel = bpp/8;

		glBindTexture(GL_TEXTURE_CUBE_MAP, m_TexId);
		for (int i = 0; i < 6; i++)
		{
			char *colorBuffer = ExtractBox(buffer, width, i, box_width, box_height, bytePerPixel);
			if(bpp == 24) 
			{
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, GL_RGB, 
					box_width, box_height, 0, GL_RGB, GL_UNSIGNED_BYTE, colorBuffer);
			}
			else {
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, GL_RGBA, 
					box_width, box_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, colorBuffer);
			}
			delete[] colorBuffer;
		}
		
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, tileMode);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, tileMode);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, magFilter);

		if (minFilter > GL_LINEAR)
		{
			glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		}

		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}

	delete[] buffer;
}

///-------------------------------------------------------------
/// Extract a face of a cube box, in case this texture is a cube
///-------------------------------------------------------------
char* Texture::ExtractBox (char *buffer, int buffer_width, int face, int box_width, int box_height, int bytePerPixel) {
	int posX, posY;
	
	switch (face) 
	{
		case 0: 
			posX = box_width * 2;
			posY = box_height;
			break;
		case 1:
			posX = 0;
			posY = box_height;
			break;
		case 2:
			posX = box_width;
			posY = 0;
			break;
		case 3:
			posX = box_width;
			posY = box_height * 2;
			break;
		case 4:
			posX = box_width;
			posY = box_height;
			break;
		case 5:
			posX = box_width * 3;
			posY = box_height;
			break;
	}

	char *colorBuffer = new char[box_width * box_height * bytePerPixel];

	for (int i = 0; i < box_width * box_height * bytePerPixel; i++) 
	{
		int offset = 
			(i / (box_width * bytePerPixel) + posY) * 
			buffer_width * bytePerPixel	+ 
			i % (box_width * bytePerPixel) +
			posX * bytePerPixel;

		colorBuffer[i] = buffer[offset];
	}

	return colorBuffer;
}

Texture::~Texture()
{
	glDeleteTextures(1, &m_TexId);
}