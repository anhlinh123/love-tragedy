#include <stdafx.h>
#include <list>
#include "Renderer.h"
#include "Texture.h"
#include "Vertex.h"
#include "Uniform.h"
#include "GeometryChunk.h"
#include "Camera.h"

///----------------------------------------------------------------------
/// Do not ever access these variables !!!
///----------------------------------------------------------------------
std::list<GeometryChunk*> g_chunkList;
Camera* g_currentCamera;
float g_fogStart = 13.0f;
Vector3 g_fogColor = Vector3(1.0f, 1.0f, 1.0f);
Vector3 g_lightDirection = Vector3(0.5f, -1.0f, 0.5f);

void SetFog(float fogStart)
{
	g_fogStart = fogStart;
}

void SetLight(float x, float y, float z)
{
	g_lightDirection = Vector3(x, y, z);
}

void SetCamera(Camera* pCamera)
{
	g_currentCamera = pCamera;
}

void AddGeometryChunk(GeometryChunk* pChunk)
{
	g_chunkList.push_back(pChunk);
}

void DoDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (std::list<GeometryChunk*>::const_iterator iterator = g_chunkList.begin(); iterator != g_chunkList.end(); iterator++)
	{
		//Set states
		if ( (*iterator)->m_bIsCullingEnabled ) glEnable(GL_CULL_FACE);
		else glDisable(GL_CULL_FACE);
		if ( (*iterator)->m_bIsDepthTestEnabled ) glEnable(GL_DEPTH_TEST);
		else glDisable(GL_DEPTH_TEST);
		if ( (*iterator)->m_bIsBlendEnabled ) glEnable(GL_BLEND);
		else glDisable(GL_BLEND);

		//Set shader
		glUseProgram( (*iterator)->m_shader->program );
		//Set textures
		for (int i = 0; i < TEXTURE_MAX; i++)
		{
			Texture* pTexture = (*iterator)->m_texture[i];
			GLint location = (*iterator)->m_shader->textureUniform[i];
			if (pTexture && pTexture->m_TexId != 0 && location != -1)
			{
				glActiveTexture(GL_TEXTURE0 + i);
				if (pTexture->m_bIsCubeTexture)
				{
					glBindTexture(GL_TEXTURE_CUBE_MAP, (*iterator)->m_texture[i]->m_TexId);
				}
				else
				{	
					glBindTexture(GL_TEXTURE_2D, (*iterator)->m_texture[i]->m_TexId);
				}
				glUniform1i( location, i);
			}
		}
		//Set uniforms
		Matrix transform = (*iterator)->m_matGlobal * (*iterator)->m_matWorld * g_currentCamera->m_view * g_currentCamera->m_projection;
		glUniformMatrix4fv( (*iterator)->m_shader->matTransform, 1, GL_FALSE, (GLfloat*) transform.m );
		if ((*iterator)->m_shader->matBones != -1)
		{
			std::vector<Matrix>& bone = (*iterator)->m_matBones;
			glUniformMatrix4fv( (*iterator)->m_shader->matBones, bone.size(), GL_FALSE, (float*) &bone[0] );
		}
		//Fog effect
		(*iterator)->m_customMatrix[0] = (*iterator)->m_matGlobal * (*iterator)->m_matWorld * g_currentCamera->m_view;
		(*iterator)->m_customVector[0] = Vector3(g_fogStart, g_fogStart + 0.0001f, 0.0f);
		(*iterator)->m_customVector[1] = g_fogColor;
		//Light effect
		(*iterator)->m_customMatrix[1] = (*iterator)->m_matGlobal * (*iterator)->m_matWorld;
		(*iterator)->m_customVector[2] = g_currentCamera->m_position;
		(*iterator)->m_customVector[3] = g_lightDirection;
		//Custom uniforms
		for (int i = 0; i < CUSTOM_MATRIX_MAX; i++)
		{
			GLint location = (*iterator)->m_shader->customMatrix[i];
			if ( location != -1 )
			{
				glUniformMatrix4fv( location, 1, GL_FALSE, (GLfloat*) (*iterator)->m_customMatrix[i].m); 
			}
		}
		for (int i = 0; i < CUSTOM_VECTOR_MAX; i++)
		{
			GLint location = (*iterator)->m_shader->customVector[i];
			Vector3 customVector = (*iterator)->m_customVector[i];
			if ( location != -1 )
			{
				glUniform3f( location, customVector.x, customVector.y, customVector.z);
			}
		}
		/** -----------------------------------
		 * To do: Do something better than this
		 */ 
		//Set attributes
		glBindBuffer(GL_ARRAY_BUFFER, (*iterator)->m_mesh->m_boIds[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*iterator)->m_mesh->m_boIds[1]);
		for (int i = 0; i < VERTEX_MAX-1; i++)
		{
			GLint id = (*iterator)->m_shader->vertexAttrib[i];
			if (id != -1)
			{
				glEnableVertexAttribArray(id);
				glVertexAttribPointer(id, GetVertexAttributeSize( (Vertex_Attribute)i ), GetVertexAttributeType( (Vertex_Attribute)i ), GL_FALSE, sizeof(Vertex), (const void*) GetVertexAttributeOffset( (Vertex_Attribute)i ) );
			}
		}
		GLint id = (*iterator)->m_shader->vertexAttrib[VERTEX_SKIN_WEIGHTS];
		if (id != -1)
		{
			glEnableVertexAttribArray(id);
			glVertexAttribPointer(id, GetVertexAttributeSize( VERTEX_SKIN_WEIGHTS), GetVertexAttributeType( VERTEX_SKIN_WEIGHTS), GL_TRUE, sizeof(Vertex), (const void*) GetVertexAttributeOffset( VERTEX_SKIN_WEIGHTS));
		}

		//Draw
		glDrawElements(GL_TRIANGLES, (*iterator)->m_mesh->m_iIndex_count, GL_UNSIGNED_SHORT, 0);

		//Clean up
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	g_chunkList.clear();
}

void ClearRenderer()
{
	g_chunkList.clear();
}