#include "StdAfx.h"
#include "Controller/GameInput.h"
#include "Camera.h"
#include "Renderer.h"
#include <Math.h>

void Camera::SetProjection (float nearPlane, float farPlane, float verticalFOV, float aspectRatio) 
{
	m_projection.SetPerspective(verticalFOV, aspectRatio, nearPlane, farPlane);
	m_fNearPlane = nearPlane;
	m_fFarPlane  = farPlane;
}

void Camera::OpenView(float farPlane)
{
	m_projection.SetPerspective(FOV, ASPECT_RATIO, m_fNearPlane, farPlane);
	m_fFarPlane  = farPlane;
}

void Camera::Initialize (const Vector3& pos, const Vector3& target, const Vector3& moveSpeed, const Vector3& angleSpeed, const Vector3& up) 
{
	m_position = pos;
	m_target   = target;
	m_up       = up;

	ComputeMatrixes();

	m_move_speed = moveSpeed;
	m_angle_speed = angleSpeed;
	m_mode = CAMERA_FREE;
	m_zoomLevel = 1.0f;
	m_originalLength = (m_position - m_target).Length();
}
	
void Camera::ComputeMatrixes () 
{
	m_zaxis = (m_position - m_target).Normalize();
	m_xaxis = m_up.Cross(m_zaxis).Normalize();
	m_yaxis = m_zaxis.Cross(m_xaxis).Normalize();

	m_horizontal = m_xaxis;
	m_straight   = (m_xaxis.Cross(m_up)).Normalize();

	//View matrix
	m_view.m[0][0] =                m_xaxis.x; m_view.m[0][1] =                m_yaxis.x; m_view.m[0][2] =                m_zaxis.x; m_view.m[0][3] = 0.0f;
	m_view.m[1][0] =                m_xaxis.y; m_view.m[1][1] =                m_yaxis.y; m_view.m[1][2] =                m_zaxis.y; m_view.m[1][3] = 0.0f;
	m_view.m[2][0] =                m_xaxis.z; m_view.m[2][1] =                m_yaxis.z; m_view.m[2][2] =                m_zaxis.z; m_view.m[2][3] = 0.0f;
	m_view.m[3][0] = -m_position.Dot(m_xaxis); m_view.m[3][1] = -m_position.Dot(m_yaxis); m_view.m[3][2] = -m_position.Dot(m_zaxis); m_view.m[3][3] = 1.0f;
	
	//World matrix
	m_world.m[0][0] =    m_xaxis.x; m_world.m[0][1] =    m_xaxis.y; m_world.m[0][2] =    m_xaxis.z; m_world.m[0][3] = 0.0f;
	m_world.m[1][0] =    m_yaxis.x; m_world.m[1][1] =    m_yaxis.y; m_world.m[1][2] =    m_yaxis.z; m_world.m[1][3] = 0.0f;
	m_world.m[2][0] =    m_zaxis.x; m_world.m[2][1] =    m_zaxis.y; m_world.m[2][2] =    m_zaxis.z; m_world.m[2][3] = 0.0f;
	m_world.m[3][0] = m_position.x; m_world.m[3][1] = m_position.y; m_world.m[3][2] = m_position.z; m_world.m[3][3] = 1.0f;
}

void Camera::MoveStraight (float z) 
{
	Vector3 normal;
	Vector3 nextPos = m_position + m_straight * z;
	
	if (normal.Dot(m_straight) < 0.8)
	{
		m_normalTerrain = normal;
		m_position = nextPos;
		m_target += m_straight * z;
		ComputeMatrixes();
	}
}

void Camera::MoveHorizontal (float x) 
{
	Vector3 normal;
	Vector3 nextPos = m_position + m_horizontal * x;
	
	if (normal.Dot(m_horizontal) < 0.8)
	{
		m_normalTerrain = normal;
		m_position = nextPos;
		m_target += m_horizontal * x;
		ComputeMatrixes();
	}
}

///--------------------------------
/// Rotate the camera around x axis
///--------------------------------
void Camera::RotateVertical (float pitch) 
{
	Vector4 localTarget(0, 0, -(m_position - m_target).Length(), 1);

	Matrix m;

	localTarget = localTarget * m.SetRotationX(pitch);
	localTarget = localTarget * m_world;

	Vector3 target = Vector3(localTarget.x, localTarget.y, localTarget.z);
	m_target = target;

	ComputeMatrixes();
}

void Camera::RotateHorizontal (float yaw) 
{
	Vector4 target(m_target.x, m_target.y, m_target.z, 1);
	
	Matrix m;

	target = target * m.SetTranslation(-m_position);
	target = target * m.SetRotationY(yaw);
	target = target * m.SetTranslation(m_position);

	m_target.x = target.x;
	m_target.y = target.y;
	m_target.z = target.z;

	ComputeMatrixes();
}

void Camera::Update (float deltaTime) 
{
	if (m_mode == CAMERA_FREE)
	{
		// Handle a movement along x axis event
		MoveHorizontal(GAME_INPUT->m_deltaCameraLR);
	
		// Handle a movement along z axis event
		MoveStraight(m_move_speed.z * GAME_INPUT->m_deltaCameraFB);

		// Handle a rotation around y axis event
		RotateHorizontal(m_angle_speed.y * GAME_INPUT->m_deltaCameraYaw);

		// Handle a rotation around x axis event
		RotateVertical(m_angle_speed.x * GAME_INPUT->m_deltaCameraPitch);
	}
}

void Camera::SetActive()
{
	SetCamera(this);
}

void Camera::MovePosAndTarget(const Vector3& direction)
{
	m_position += direction;
	m_target   += direction;

	ComputeMatrixes();
}

// rotate based on point as origin
void Camera::RotatePosAndTarget(Vector3& rotate, Vector3& point)
{
	Vector4 position(m_position.x, m_position.y, m_position.z, 1);
	Vector4 target(m_target.x, m_target.y, m_target.z, 1);
	Vector4 up(m_up.x, m_up.y, m_up.z, 1);

	Matrix m;

	position = position * m.SetTranslation(-point);
	position = position * m.SetRotationZ(rotate.z);
	position = position * m.SetRotationX(rotate.x);
	position = position * m.SetRotationY(rotate.y);
	position = position * m.SetTranslation(point);
	m_position.x = position.x;
	m_position.y = position.y;
	m_position.z = position.z;

	target = target * m.SetTranslation(-point);
	target = target * m.SetRotationZ(rotate.z);
	target = target * m.SetRotationX(rotate.x);
	target = target * m.SetRotationY(rotate.y);
	target = target * m.SetTranslation(point);
	m_target.x = target.x;
	m_target.y = target.y;
	m_target.z = target.z;

	up = up * m.SetRotationZ(rotate.z);
	up = up * m.SetRotationX(rotate.x);
	up = up * m.SetRotationY(rotate.y);
	m_up.x = up.x;
	m_up.y = up.y;
	m_up.z = up.z;

	ComputeMatrixes();
}

void Camera::RotateAroundTarget(const Vector3& rotate)
{
	Vector4 position(m_position.x, m_position.y, m_position.z, 1);
	
	Matrix m;

	position = position * m.SetTranslation(-m_target);
	position = position * m.SetRotationZ(rotate.z);
	position = position * m.SetRotationX(rotate.x);
	position = position * m.SetRotationY(rotate.y);
	position = position * m.SetTranslation(m_target);

	m_position.x = position.x;
	m_position.y = position.y;
	m_position.z = position.z;

	ComputeMatrixes();
}

/*
	According to current direction of camera, the forward/backward/left/right will be computed
	Ex: 
		when camera direction is (0, 0, -1) then forward will be (0, 0, -1)
		when camera direction is (0.1, 0, -0.95) then forward will be (0, 0, -1) too, because this is nearest to camera direction
*/
void Camera::ComputeBasis(Vector3& face, Vector3& up)
{
	Vector3 direction = (m_target - m_position).Normalize();
	Vector3 faceOrtho = face.Cross(up);

	float angleX = direction.Dot(face);
	float angleZ = direction.Dot(faceOrtho);

	// larger cos means smaller angle
	if (fabs(angleX) > fabs(angleZ))
	{
		if (angleX > 0)
		{
			m_forwardBasis	= face;
			m_backwardBasis = face * (-1.0f);
			m_leftBasis		= faceOrtho * (-1.0f);
			m_rightBasis	= faceOrtho;
		}
		else
		{
			m_forwardBasis	= face * (-1.0f);
			m_backwardBasis = face;
			m_leftBasis		= faceOrtho;
			m_rightBasis	= faceOrtho * (-1.0f);
		}
	}
	else
	{
		if (angleZ > 0)
		{
			m_forwardBasis	= faceOrtho;
			m_backwardBasis = faceOrtho * (-1.0f);
			m_leftBasis		= face;
			m_rightBasis	= face * (-1.0f);
		}
		else
		{
			m_forwardBasis	= faceOrtho * (-1.0f);
			m_backwardBasis = faceOrtho;
			m_leftBasis		= face * (-1.0f);
			m_rightBasis	= face;
		}
	}
}

// offset.x is distance behind, offset.y is distance up
void Camera::SetBehindCharacter (const Vector3& charPos, const Vector3& charFace, const Vector3& charUp, const Vector2& offset, bool keepZoom)
{
	m_up = charUp;
	m_target   = charPos;
	m_position = charPos + charFace * (-1.0f) * offset.x + m_up * offset.y;
	m_originalLength = (m_position - m_target).Length();

	if (keepZoom)
	{
		float z = m_zoomLevel;
		m_zoomLevel = 0.0f;
		ChangeZoomLevel(z);
	}
	else
		m_zoomLevel = 1.0f;

	ComputeMatrixes();
	m_mode = CAMERA_LOCK_BEHIND;
}

void Camera::SetLookAtCharacter (const Vector3& charPos, const Vector3& camPos, const Vector3& camUp, bool keepZoom)
{
	m_target   = charPos;
	m_position = camPos;
	m_up = camUp;
	m_originalLength = (m_position - m_target).Length();

	if (keepZoom)
	{
		float z = m_zoomLevel;
		m_zoomLevel = 0.0f;
		ChangeZoomLevel(z);
	}
	else
		m_zoomLevel = 1.0f;

	ComputeMatrixes();
	m_mode = CAMERA_LOOK_AT_CHARACTER;
}

void Camera::ChangeToNextMode()
{
	m_mode = (CameraMode)((m_mode + 1) % CAMERA_MODE_MAX_VALUE);
}

void Camera::ChangeZoomLevel(float delta)
{
	m_zoomLevel += delta;
	Vector3 targetToPos = m_position - m_target;
	m_position = m_target + targetToPos.Normalize() * m_originalLength * m_zoomLevel;
	
	ComputeMatrixes();
}