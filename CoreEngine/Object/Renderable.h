#pragma once

#include "GeometryChunk.h"

class Renderable
{
public:
	Renderable() : m_geometryChunks(NULL) {}
	virtual ~Renderable() {}

	virtual void Load(const char* fileName) = 0;
	virtual void SetMatWorld(const Matrix& matWorld) = 0;
	virtual void Update(float deltaTime) = 0;
	virtual void Draw() = 0;
	virtual void PlayAnimation(const char* name) = 0;

protected:
	int m_numChunks;
	GeometryChunk** m_geometryChunks;
};