#include <unordered_map>
#include "tuple.hpp"
#include "Animation.h"

class Mesh;
class Matrix;
/** Skeleton hierarchy
 */
struct Node
{
	std::string m_name;
	std::vector<Node*> m_child;

	//! most recently calculated local transform
	Matrix m_localTransform; 

	//! same, but in world space
	Matrix m_globalTransform;

	//!  index in the current animation's channel array. -1 if not animated.
	int mChannelIndex;        

	//! Default construction
	Node() {
		mChannelIndex = -1;
	}

	//! Destruct all children recursively
	~Node() {
		for( std::vector<Node*>::iterator it = m_child.begin(); it != m_child.end(); ++it)
			delete *it; 
	}
};

// ---------------------------------------------------------------------------------
/** Calculates the animated node transformations for a given scene and timestamp. 
 *
 *  Create an instance for a aiScene you want to animate and set the current animation 
 *  to play. You can then have the instance calculate the current pose for all nodes 
 *  by calling Calculate() for a given timestamp. A full set of bone matrices can be 
 *  retrieved by GetBoneMatrices() for a given mesh. 
 */
class ModelAnimator
{
public:

	// ----------------------------------------------------------------------------
	/** Constructor for a given scene.
	 *
	 * The object keeps a reference to the scene during its lifetime, but 
	 * ownership stays at the caller. 
	 * @param pScene The scene to animate.
	 * @param pAnimIndex [optional] Index of the animation to play. Assumed to
	 *  be 0 if not given.
	 */
	ModelAnimator(const char* fileName);

	// ----------------------------------------------------------------------------
	/** Copy constructor. (Use for cloning)
	 */
	ModelAnimator(const ModelAnimator& otherAnimator);
	void CopyNodeTree(Node* pNode, Node* otherNode);

	/** Destructor */
	~ModelAnimator();

	// ----------------------------------------------------------------------------
	void AddAnimation( const char* pfileName, const char* anim_name);

	// ----------------------------------------------------------------------------
	/** Sets the animation to use for playback. This also recreates the internal 
	 * mapping structures, which might take a few cycles.
	 * @param pAnimIndex Index of the animation in the scene's animation array 
	 */
	void SetAnimName( const char* anim_name);

	// ----------------------------------------------------------------------------
	/** Calculates the node transformations for the scene. Call this to get 
	 * uptodate results before calling one of the getters.
	 * @param pTime Current time. Can be an arbitrary range.
	 */
	void Calculate( double pTime);

	// ----------------------------------------------------------------------------
	/** Calculates the bone matrices for the given mesh.
	 *
	 * Each bone matrix transforms from mesh space in bind pose to mesh space in 
	 * skinned pose, it does not contain the mesh's world matrix. Thus the usual
	 * matrix chain for using in the vertex shader is
	 * @code
	 * boneMatrix * worldMatrix * viewMatrix * projMatrix
	 * @endcode
	 * @param pMesh The given mesh.
	 * @return A reference to a vector of bone matrices. Stays stable till the
	 *   next call to GetBoneMatrices();
	 */
	const std::vector<Matrix>& GetBoneMatrices( Mesh* pMesh);

	// ----------------------------------------------------------------------------
	/** Get the global matrix for the given mesh.
	 */
	Matrix GetGlobalMatrix( Mesh* pMesh);

	//// ----------------------------------------------------------------------------
	///** @brief Get the current animation index
	// */
	//std::string GetCurrentAnimName() const {
	//	return m_currentAnimName;
	//}

	//// ----------------------------------------------------------------------------
	///** @brief Get the current animation or NULL
	// */
	//Animation* GetCurrentAnim() const {
	//	return  m_currentAnimName != "" ? m_animations[m_currentAnimName] : NULL;
	//}

protected:

	/** Recursively creates an internal node structure matching the 
	 *  current scene and animation. (A more efficient way) 
	 */
	void CreateNodeTree( unsigned char* buffer, unsigned int offset, Node* pNode, Matrix parentMatrix);

	/** Recursively creates an internal node structure matching the 
	 *  current scene and animation. 
	 */
	void UpdateNodeTree( Node* pParent);

	/** Recursively updates the internal node transformations from the
	 *  given matrix array 
	 */
	void UpdateTransforms( Node* pNode, const std::vector<Matrix>& pTransforms, Matrix parentMatrix);

protected:
	/** Current animation name */
	std::string m_currentAnimName;

	/** Root node of the internal scene structure */
	Node* mRootNode;

	/** Name to node map to quickly find nodes for given bones by their name */
	std::unordered_map<std::string, const Node*> mNodesByName;

	/** Array to return transformations results inside. */
	std::vector<Matrix> mTransforms;

	/** Identity matrix to return a reference to in case of error */
	Matrix mIdentityMatrix;

	/** Animation set for the model */
	std::unordered_map<std::string, Animation*> m_animations;

	/** At which frame the last evaluation happened for each channel. 
	 * Useful to quickly find the corresponding frame for slightly increased time stamps
	 */
	double mLastTime;
	std::vector<boost::tuple<unsigned int, unsigned int, unsigned int> > mLastPositions;

	/** Flags
	 */
	bool m_isPrototype;
};