#pragma once

#ifdef WIN32
#define DATA_PATH			 "../Data/"
#define SHADER_PATH          "../Data/Shaders/"
#define MESH_PATH            "../Data/Meshes/"
#define TEXTURE_PATH         "../Data/Textures/"
#define MODEL_PATH           "../Data/Models/"
#define ANIMATION_PATH       "../Data/Animations/"
#define SOUND_PATH			 "../Data/Sounds/"
#define CINE_PATH			 "../Data/Cinematics/"
#else
#define DATA_PATH			 "/sdcard/Data/"
#define SHADER_PATH          "/sdcard/Data/Shaders/"
#define MESH_PATH            "/sdcard/Data/Meshes/"
#define TEXTURE_PATH         "/sdcard/Data/Textures/"
#define MODEL_PATH           "/sdcard/Data/Models/"
#define ANIMATION_PATH       "/sdcard/Data/Animations/"
#define SOUND_PATH			 "/sdcard/Data/Sounds/"
#define CINE_PATH			 "/sdcard/Data/Cinematics/"
#endif

void GetFullPath(const char* path, const char* fileName, char* fullPath);