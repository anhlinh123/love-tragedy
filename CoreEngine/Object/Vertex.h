#pragma once
#include "Utilities/Math.h"

struct Vertex 
{
	Vector3 m_pos;
	Vector3 m_normal;
	Vector3 m_binormal;
	Vector3 m_tangent;
	Vector2 m_uv;
	Vector2 m_uvBlend;
	Vector3 m_color;
	unsigned char m_boneId[4];
	unsigned char m_weight[4];
};