#include "stdafx.h"
#include "Object.h"
#include "Utilities/utilities.h" // if you use STL, please include this line AFTER all other include


void Object::SetGeometry (const Vector3& pos, const Vector3& rot, const Vector3& scale)
{
	Matrix tmp;
	tmp.SetIdentity();
	m_matWorld.SetIdentity();
	
	tmp.SetScale(scale);
	m_matWorld = m_matWorld * tmp;
	m_scale = scale;

	tmp.SetRotationZ(rot.z);
	m_matWorld = m_matWorld * tmp;
	tmp.SetRotationX(rot.x);
	m_matWorld = m_matWorld * tmp;
	tmp.SetRotationY(rot.y);
	m_matWorld = m_matWorld * tmp;
	m_rotation = rot;

	tmp.SetTranslation(pos);
	m_matWorld = m_matWorld * tmp;
	m_translation = pos;
}

void Object::ChangeGeometry (const Vector3& move, const Vector3& rot, const Vector3& scale)
{
	m_scale.x *= scale.x;
	m_scale.y *= scale.y;
	m_scale.z *= scale.z;
	m_rotation += rot;
	m_translation += move;

	Matrix tmp;
	tmp.SetIdentity();
	
	tmp.SetScale(scale);
	m_matWorld = m_matWorld * tmp;

	tmp.SetRotationZ(rot.z);
	m_matWorld = m_matWorld * tmp;
	tmp.SetRotationX(rot.x);
	m_matWorld = m_matWorld * tmp;
	tmp.SetRotationY(rot.y);
	m_matWorld = m_matWorld * tmp;

	tmp.SetTranslation(move);
	m_matWorld = m_matWorld * tmp;
}
 
void Object::Move (const Vector3& move)
{
	ChangeGeometry(move, Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f));

	// move the related objects also
	// TODO: this will crash if the object is deleted elsewhere
	for (ObjectVector::iterator it = m_relateObjects.begin(); it != m_relateObjects.end(); it++)
	{
		(*it)->Move(move);
	}
}

void Object::Rotate (Vector3& rot, Vector3& posOffset)
{
	Vector3 translation = m_translation;
	ChangeGeometry(-translation, Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f));
	ChangeGeometry(Vector3(0.0f, 0.0f, 0.0f), rot, Vector3(1.0f, 1.0f, 1.0f));

	// also need to update the new translation vector because rotate with posOffset make it false
	Vector4 newPosOffset = Vector4(		posOffset.x,
										posOffset.y,
										posOffset.z,
										1.0f);
	Matrix m;
	newPosOffset = newPosOffset * m.SetRotationZ(rot.z);
	newPosOffset = newPosOffset * m.SetRotationX(rot.x);
	newPosOffset = newPosOffset * m.SetRotationY(rot.y);
	translation -= posOffset;
	translation += Vector3(newPosOffset.x, newPosOffset.y, newPosOffset.z);
	ChangeGeometry(translation, Vector3(0.0f, 0.0f, 0.0f), Vector3(1.0f, 1.0f, 1.0f));
	posOffset = Vector3(newPosOffset.x, newPosOffset.y, newPosOffset.z);

	// rotate the related objects also
	// TODO: this will crash if the object is deleted elsewhere
	for (ObjectVector::iterator it = m_relateObjects.begin(); it != m_relateObjects.end(); it++)
	{
		(*it)->Rotate(rot, posOffset);
	}
}

void Object::Update (float deltaTime)
{
	if (m_isVisible)
	{
		m_renderable->Update(deltaTime);
	}
}

void Object::Draw ()
{
	if (m_isVisible)
	{
		m_renderable->SetMatWorld(m_matWorld);
		m_renderable->Draw();
	}
}

void Object::PlayAnimation(const char* name) 
{ 
	if (m_isVisible)
	{
		m_renderable->PlayAnimation(name);
	}
}

void Object::RelateObject(Object* obj)
{
	// add object to the list, if it not presented yet
	for (ObjectVector::iterator it = m_relateObjects.begin(); it != m_relateObjects.end(); it++)
	{
		if (obj == (*it))
			return;
	}
	m_relateObjects.push_back(obj);
}

void Object::UnrelateObject(Object* obj)
{
	ObjectVector::iterator it = m_relateObjects.begin();
	while ( it != m_relateObjects.end() )
	{
		if (obj == (*it))
			break;
		it++;
	}
	m_relateObjects.erase(it);
}