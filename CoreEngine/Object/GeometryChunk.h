#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "Uniform.h"

class GeometryChunk
{
public:
	GeometryChunk();
	~GeometryChunk() {};

	void SetCustomMatrix(int location, Matrix customMatrix);
	void SetCustomVector(int location, Vector3 customVector);

	Mesh*    m_mesh;
	Texture* m_texture[TEXTURE_MAX];
	Shader*  m_shader;

	std::vector<Matrix> m_matBones;
	Matrix   m_matWorld;
	Matrix   m_matGlobal;
	Matrix   m_customMatrix[CUSTOM_MATRIX_MAX];
	Vector3  m_customVector[CUSTOM_VECTOR_MAX];

	//Some OpenGL States
	bool     m_bIsDepthTestEnabled;
	bool     m_bIsCullingEnabled;
	bool     m_bIsBlendEnabled;
};
