#pragma once

#include "Utilities/Utilities.h"

#define		FAR_PLANE		150.0f//12.5f
#define		FOV				0.79f
#define		ASPECT_RATIO	1.6f

///----------------------------------------------------------------------
/// This camera is only for debugging.
///----------------------------------------------------------------------
class Camera 
{
public:
	enum CameraMode
	{
		CAMERA_FREE,
		CAMERA_LOOK_AT_CHARACTER,
		CAMERA_LOCK_BEHIND,
		CAMERA_MODE_MAX_VALUE
	};

	CameraMode m_mode;

	float m_fNearPlane;
	float m_fFarPlane;

	Matrix m_projection;
	Matrix m_view;
	
	Vector3 m_position;
	Vector3 m_up;

	float m_zoomLevel;
	float m_originalLength;

	Camera () {}
	~Camera () {}

	void SetProjection (float nearPlane, float farPlane, float verticalFOV, float aspectRatio);
	void Initialize (const Vector3& pos, const Vector3& target, const Vector3& moveSpeed, const Vector3& angleSpeed, const Vector3& up);
	void Update (float deltaTime);
	void RotateVertical (float pitch);
	void RotateHorizontal (float yaw);
	void SetActive();
	void ChangeToNextMode();

	void SetBehindCharacter (const Vector3& charPos, const Vector3& charFace, const Vector3& charUp, const Vector2& offset, bool keepZoom = false);
	void SetLookAtCharacter (const Vector3& charPos, const Vector3& camPos, const Vector3& camUp, bool keepZoom = false);
	void MovePosAndTarget(const Vector3& direction);
	void RotatePosAndTarget(Vector3& rotate, Vector3& point);
	void RotateAroundTarget(const Vector3& rotate);
	void ChangeZoomLevel(float delta);

	void OpenView(float farPlane);

	// compute basises based on direction of camera
	void ComputeBasis(Vector3& face, Vector3& up);
	
	// convenience value
	Vector3 m_forwardBasis;
	Vector3 m_backwardBasis;
	Vector3 m_leftBasis;
	Vector3 m_rightBasis;

private:
	Matrix m_world;

	Vector3 m_target;
	Vector3 m_normalTerrain;
	Vector3 m_xaxis, m_yaxis, m_zaxis, m_straight, m_horizontal;
	Vector3 m_move_speed;
	Vector3 m_angle_speed;


	void ComputeMatrixes ();
	void MoveStraight (float z);
	void MoveHorizontal (float x);
};