#pragma once

#include "renderable.h"

class Model;
class ModelAnimator;
Model* CreateModel(const char* name);
void ClearModelTable();

class Model : public Renderable
{
public:
	Model(void);
	~Model(void);

	void Load(const char* fileName);
	void Load(const Model& prototype);
	void Update(float dTime);
	void SetMatWorld(const Matrix& matWorld);
	void Draw();
	void PlayAnimation(const char* name);

private:
	ModelAnimator* m_animator;
	double m_timeInSec;
};

